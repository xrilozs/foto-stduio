<?php

namespace backend\models;

use common\models\Album;
use common\models\CetakFoto;
use common\models\OrderDetailPaketFoto;
use common\models\PaketFrameDetail;
use common\models\PaketKategori;
use common\models\TemaFoto;

class PaketFoto extends \common\models\PaketFoto
{
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paket_id' => 'Paket ID',
            'paket_kategori_id' => 'Kategori Paket',
            'paket_nama' => 'Nama Paket',
            //'paket_gambar' => 'Gambar Paket',
            'paket_harga' => 'Harga Paket',
            'paket_ket' => 'Keterangan',
            'paket_jumlah_tema' => 'Jumlah Tema',
            'paket_pose' => 'Jumlah Pose',
            'paket_album_id' => 'Nama Album',
            'paket_cetak_id' => 'Cetak Ukuran Foto',
            'paket_album_qty' => 'Qty Album',
            'paket_cetak_qty' => 'Qty Cetak',
            'paket_jumlah_foto_utama' => 'Cetak Foto Utama',
            'paket_jumlah_cetak' => 'Cetak Foto Biasa'
        ];
    }

    public function getTemaFoto()
    {
        return $this->hasOne(TemaFoto::className(), ['tem_paket_id' => 'paket_id']);
    }
    public function getOrderDetailPaketFoto()
    {
        return $this->hasOne(OrderDetailPaketFoto::className(), ['odpf_paket_id' => 'paket_id']);
    }
    public function getAlbum()
    {
        return $this->hasOne(Album::className(), ['album_id' => 'paket_album_id']);
    }
    public function getPaketKategori()
    {
        return $this->hasOne(PaketKategori::className(), ['kategori_id' => 'paket_kategori_id']);
    }
    public function getCetakFoto()
    {
        return $this->hasOne(CetakFoto::className(), ['cetak_id' => 'paket_cetak_id']);
    }
    public function getPaketFrameDetail()
    {
        return $this->hasOne(PaketFrameDetail::className(), ['fd_paket_id' => 'paket_id']);
    }
    
}
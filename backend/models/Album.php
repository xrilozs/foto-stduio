<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "album".
 *
 * @property int $album_id
 * @property string $album_nama
 * @property string $album_warna
 */
class Album extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'album';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['album_nama', 'album_warna'], 'required'],
            [['album_nama'], 'string', 'max' => 25],
            [['album_warna'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'album_id' => 'Album ID',
            'album_nama' => 'Album Nama',
            'album_warna' => 'Album Warna',
        ];
    }
}

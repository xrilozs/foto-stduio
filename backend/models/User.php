<?php

namespace backend\models;

use common\models\Karyawan;
use Yii;
/**
* User model 
* This is the model class for table "user".
*
* @property int $id
* @property string $username
* @property string $password
* @property string $authKey
* @property string $accessToken
* @property int $status
* @property string $role
* @property int $id_karyawan
* @property string|null $time_create
* @property string|null $time_update
*
* @property Karyawan $karyawan
*/


class User extends \yii\db\ActiveRecord
{
    
    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['username', 'password', 'authKey', 'accessToken', 'status', 'role', 'id_karyawan'], 'required'],
            [['status', 'id_karyawan'], 'integer'],
            [['time_create', 'time_update'], 'safe'],
            [['username', 'password'], 'string', 'max' => 30],
            [['authKey'], 'string', 'max' => 50],
            [['accessToken'], 'string', 'max' => 100],
            [['role'], 'string', 'max' => 60],
            [['id_karyawan'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['id_karyawan' => 'kary_id']],
        ];
    }

    public function attributeLabels()
   {
        return [
           'id' => 'ID',
           'username' => 'Username',
           'password' => 'Password',
           'authKey' => 'Auth Key',
           'accessToken' => 'Access Token',
           'status' => 'Status',
           'role' => 'Role',
           'id_karyawan' => 'Id Karyawan',
           'time_create' => 'Time Create',
           'time_update' => 'Time Update',
        ];
    }
}
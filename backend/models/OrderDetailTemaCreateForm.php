<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * @property int $cust_id
 */
class OrderDetailTemaCreateForm extends Model
{
    public $cust_id;
    public $cust_nama;
    public $detcus_tanggal;
    public $paket_nama;
    public $jumlah_tema;
    public $paket_pose;
    public $tem_id;
    public $ket;
    public $studio;
    public $fotografer_id;
    public $asisten_id;
    public $cs_id;

    public function rules()
    {
        return [
            [['cust_id'], 'required'],
            [['tem_id'], 'required'],
            [['ket'], 'required'],
            [['studio'], 'required'],
            [['fotografer_id'], 'required'],
            [['asisten_id'], 'required'],
            [['cs_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cust_id' => 'No invoice',
            'tem_id' => 'Nama Tema',
            'ket' => 'Keterangan',
            'studio' => 'No. Studio',
            'fotografer_id' => 'Nama Fotografer',
            'asisten_id' => 'Nama Asisten',
            'cs_id' => 'Nama CS',
        ];
    }    
}

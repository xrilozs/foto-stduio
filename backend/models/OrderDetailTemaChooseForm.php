<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * @property int $cust_id
 */
class OrderDetailTemaChooseForm extends Model
{
    public $cust_id;

    public function rules()
    {
        return [
            [['cust_id'], 'required'],
            ['cust_id', 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cust_id' => 'No invoice',
        ];
    }    
}

<?php

namespace backend\controllers;

use common\models\PaketUpgrade;
use common\models\PaketUpgradeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PaketUpgradeController implements the CRUD actions for PaketUpgrade model.
 */
class PaketUpgradeController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all PaketUpgrade models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PaketUpgradeSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaketUpgrade model.
     * @param int $upg_id Upg ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($upg_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($upg_id),
        ]);
    }

    /**
     * Creates a new PaketUpgrade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new PaketUpgrade();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->upg_gambar = UploadedFile::getInstance($model, 'upg_gambar');
                $image_nama = $model->upg_nama.rand(1, 4000).'.'.$model->upg_gambar->extension;
                $image_path = 'upg_gambar'.$image_nama;
                $model->upg_gambar->saveAs($image_path);
                $model->upg_gambar= $image_path;
                
                if($model->save())
               {    
                   //Yii::$app->getSession()->setFlash('success','Data saved!');
                   return $this->redirect(['view','upg_id'=>$model->upg_id]);
               }
               else
               {
                   Yii::$app->getSession()->setFlash('error','Data not saved!');
                   return $this->render('create', [
                         'model' => $model,
                   ]);
               }                        
                //return $this->redirect(['view', 'upg_id' => $lastInsertID]);
            }
        } else {
            $model->loadDefaultValues();
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PaketUpgrade model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $upg_id Upg ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($upg_id)
    {
        $model = $this->findModel($upg_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'upg_id' => $model->upg_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PaketUpgrade model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $upg_id Upg ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($upg_id)
    {
        $this->findModel($upg_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaketUpgrade model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $upg_id Upg ID
     * @return PaketUpgrade the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($upg_id)
    {
        if (($model = PaketUpgrade::findOne(['upg_id' => $upg_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

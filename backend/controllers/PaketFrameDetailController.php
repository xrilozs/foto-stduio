<?php

namespace backend\controllers;

use common\models\PaketFrameDetail;
use common\models\PaketFrameDetailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaketFrameDetailController implements the CRUD actions for PaketFrameDetail model.
 */
class PaketFrameDetailController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all PaketFrameDetail models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PaketFrameDetailSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaketFrameDetail model.
     * @param int $fd_id Fd ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($fd_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($fd_id),
        ]);
    }

    /**
     * Creates a new PaketFrameDetail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new PaketFrameDetail();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'fd_id' => $model->fd_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PaketFrameDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $fd_id Fd ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($fd_id)
    {
        $model = $this->findModel($fd_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'fd_id' => $model->fd_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PaketFrameDetail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $fd_id Fd ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($fd_id)
    {
        $this->findModel($fd_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaketFrameDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $fd_id Fd ID
     * @return PaketFrameDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($fd_id)
    {
        if (($model = PaketFrameDetail::findOne(['fd_id' => $fd_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace backend\controllers;

use common\models\CetakFoto;
use common\models\CetakFotoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CetakFotoController implements the CRUD actions for CetakFoto model.
 */
class CetakFotoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all CetakFoto models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CetakFotoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CetakFoto model.
     * @param int $cetak_id Cetak ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cetak_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($cetak_id),
        ]);
    }

    /**
     * Creates a new CetakFoto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new CetakFoto();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'cetak_id' => $model->cetak_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CetakFoto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cetak_id Cetak ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cetak_id)
    {
        $model = $this->findModel($cetak_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cetak_id' => $model->cetak_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CetakFoto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cetak_id Cetak ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cetak_id)
    {
        $this->findModel($cetak_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CetakFoto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cetak_id Cetak ID
     * @return CetakFoto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cetak_id)
    {
        if (($model = CetakFoto::findOne(['cetak_id' => $cetak_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace backend\controllers;

use Yii;
use common\models\Foto;
use common\models\HasilFoto;
use common\models\HasilFotoItem;
use common\models\Customer;
use common\models\DetailCustomer;
use common\models\FotoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile ;


/**
 * FotoController implements the CRUD actions for Foto model.
 */
class FotoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Foto models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new FotoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Foto model.
     * @param int $foto_detcus_id Foto Detcus ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($foto_detcus_id)
    {
        $model = Foto::find()->where(['foto_detcus_id' => $foto_detcus_id])->all();
        $detcus = DetailCustomer::findOne($foto_detcus_id);
        $customer = Customer::find()->where(['cust_id' => $detcus->detcus_cust_id])->one();
        return $this->render('view', [
            'model' => $model,
            'customer' => $customer
        ]);
    }

    /**
     * Creates a new Foto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Foto;
            $request_payload = Yii::$app->request;
            if (Yii::$app->request->ispost)
            $model->foto_image = UploadedFile::getInstances($model, 'foto_nama_file');
            if ($model->foto_image) {
                $foto_request = $request_payload->post('Foto');
                $detcus = DetailCustomer::find()->where(['detcus_cust_id' => $foto_request['foto_detcus_id']])->one();
                $foto_detcus_id = null;
                foreach ($model->foto_image as $value) {
                    $model = new Foto;
                    $BasePath = Yii::$app->basePath.'/web/uploads/';
                    $filename = $value->baseName.'.'.$value->extension;
                    $model->foto_image = $filename;
                    $model->foto_nama_file = $filename;
                    $model->foto_detcus_id = $detcus->detcus_id;
                    $model->foto_ket = "";
                    if ($model->save()){
                        $value->saveAs($BasePath.$filename);
                    }
                    if(is_null($foto_detcus_id)){
                        $foto_detcus_id = $model->foto_detcus_id;
                    }
                }
                return $this->redirect(['view','foto_detcus_id'=>$foto_detcus_id]);
            }
            else {
                $model->loadDefaultValues();
            }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Foto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $foto_id Foto ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($foto_id)
    {
        $model = $this->findModel($foto_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'foto_id' => $model->foto_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Foto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $foto_id Foto ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($foto_id)
    {
        $this->findModel($foto_id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteByHasilFoto($hasil_foto_id)
    {
        $hasil_foto = HasilFoto::findOne($hasil_foto_id);
        $hasil_foto_item = HasilFotoItem::find()->where(['hasil_foto_id' => $hasil_foto_id])->all();
        $foto = Foto::find()->where(["foto_detcus_id" => $hasil_foto->hf_detcus_id])->all();

        $list_hasil_foto = array_map(function ($item){
            return $item->foto_id;
        }, $hasil_foto_item);

        for($i = 0; $i < sizeof($foto); $i++){
            $item = $foto[$i];
            if( !in_array($item->foto_id, $list_hasil_foto) ){
                $foto_i = Foto::findOne($item->foto_id);
                $foto_i->delete();
                if(file_exists(Yii::getAlias('@backend/web/uploads').'/'.$item->foto_image)){
                    unlink(Yii::getAlias('@backend/web/uploads').'/'.$item->foto_image);
                }
            }
        }
        
        return $this->asJson(array("status"=>"success"));
    }

    /**
     * Finds the Foto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $foto_id Foto ID
     * @return Foto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($foto_id)
    {
        if (($model = Foto::findOne(['foto_id' => $foto_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSteper()
    {
        return $this->render('steper');
    }
}

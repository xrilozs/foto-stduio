<?php

namespace backend\controllers;

use common\models\Pemesanan;
use common\models\PemesananSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PemesananController implements the CRUD actions for Pemesanan model.
 */
class PemesananController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Pemesanan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PemesananSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pemesanan model.
     * @param int $pes_id Pes ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($pes_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($pes_id),
        ]);
    }

    /**
     * Creates a new Pemesanan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Pemesanan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {

                //ubah format tanggal sebelum disave ke database
                $model->pes_tgl_foto = \Yii::$app->formatter->asDate($model->pes_tgl_foto, 'yyyy-MM-dd');
                $model->pes_tgl_pilih_foto = \Yii::$app->formatter->asDate($model->pes_tgl_pilih_foto, 'yyyy-MM-dd');
                $model->pes_tgl_review = \Yii::$app->formatter->asDate($model->pes_tgl_review, 'yyyy-MM-dd');
                $model->pes_tgl_deadline = \Yii::$app->formatter->asDate($model->pes_tgl_deadline, 'yyyy-MM-dd');
                if($model->save())
                   {    
                       //Yii::$app->getSession()->setFlash('success','Data saved!');
                       return $this->redirect(['view','pes_id'=>$model->pes_id]);
                   }
                   else
                   {
                       Yii::$app->getSession()->setFlash('error','Data not saved!');
                       return $this->render('create', [
                             'model' => $model,
                       ]);
                   }      
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pemesanan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $pes_id Pes ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($pes_id)
    {
        $model = $this->findModel($pes_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'pes_id' => $model->pes_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pemesanan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $pes_id Pes ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($pes_id)
    {
        $this->findModel($pes_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pemesanan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $pes_id Pes ID
     * @return Pemesanan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($pes_id)
    {
        if (($model = Pemesanan::findOne(['pes_id' => $pes_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

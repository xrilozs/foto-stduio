<?php

namespace backend\controllers;

use common\models\OrderDetailPaketUpgrade;
use common\models\OrderDetailPaketUpgradeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderDetailPaketUpgradeController implements the CRUD actions for OrderDetailPaketUpgrade model.
 */
class OrderDetailPaketUpgradeController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all OrderDetailPaketUpgrade models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new OrderDetailPaketUpgradeSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderDetailPaketUpgrade model.
     * @param int $odpu_id Odpu ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($odpu_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($odpu_id),
        ]);
    }

    /**
     * Creates a new OrderDetailPaketUpgrade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new OrderDetailPaketUpgrade();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'odpu_id' => $model->odpu_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrderDetailPaketUpgrade model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $odpu_id Odpu ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($odpu_id)
    {
        $model = $this->findModel($odpu_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'odpu_id' => $model->odpu_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrderDetailPaketUpgrade model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $odpu_id Odpu ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($odpu_id)
    {
        $this->findModel($odpu_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrderDetailPaketUpgrade model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $odpu_id Odpu ID
     * @return OrderDetailPaketUpgrade the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($odpu_id)
    {
        if (($model = OrderDetailPaketUpgrade::findOne(['odpu_id' => $odpu_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace backend\controllers;

use common\models\OrderDetailPaketFoto;
use common\models\OrderDetailPaketFotoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderDetailPaketFotoController implements the CRUD actions for OrderDetailPaketFoto model.
 */
class OrderDetailPaketFotoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all OrderDetailPaketFoto models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new OrderDetailPaketFotoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderDetailPaketFoto model.
     * @param int $odpf_id Odpf ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($odpf_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($odpf_id),
        ]);
    }

    /**
     * Creates a new OrderDetailPaketFoto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new OrderDetailPaketFoto();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'odpf_id' => $model->odpf_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrderDetailPaketFoto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $odpf_id Odpf ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($odpf_id)
    {
        $model = $this->findModel($odpf_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'odpf_id' => $model->odpf_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrderDetailPaketFoto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $odpf_id Odpf ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($odpf_id)
    {
        $this->findModel($odpf_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrderDetailPaketFoto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $odpf_id Odpf ID
     * @return OrderDetailPaketFoto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($odpf_id)
    {
        if (($model = OrderDetailPaketFoto::findOne(['odpf_id' => $odpf_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetOrderDetailPaketFoto($paketId)
    {
        $paketfoto = OrderDetailPaketFoto::findOne($paketId);
        echo Json::encode($paketfoto);
    }
}

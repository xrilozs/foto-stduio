<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use common\models\DetailCustomer;
use common\models\OrderDetailPaketFoto;
use common\models\OrderDetailPaketUpgrade;
use common\models\OrderDetailTema;
use common\models\DetailServiceSpk;
use common\models\TemaFoto;
use common\models\TemaFotoSearch;
use common\models\DetailCustomerSearch;
use common\models\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;
use yii\helpers\Json;

/**
 * DetailCustomerController implements the CRUD actions for DetailCustomer model.
 */
class DetailCustomerController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all DetailCustomer models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DetailCustomerSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        if(Yii::$app->request->post('hasEditable'))
        {
            $detcusId = Yii::$app->request->post('editableKey');
            $detcus = DetailCustomer::findOne($detcusId);

            $out = Json::encode(['output' => '', 'message' => '']);
            $post = [];
            $posted = current($_POST['DetailCustomer']);
            $post['DetailCustomer'] = $posted;

            if ($detcus->load($post))
            {
                $detcus -> save();
            }
            echo $out;
            return;

        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DetailCustomer model.
     * @param int $detcus_id Detcus ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($detcus_id)
    {
        $rowView = Yii::$app->db->createCommand('SELECT * FROM detail_customer 
        JOIN customer ON customer.cust_id = detail_customer.detcus_cust_id 
        JOIN order_detail_paket_foto ON order_detail_paket_foto.odpf_detcus_id = detail_customer.detcus_id 
        JOIN paket_foto ON paket_foto.paket_id = order_detail_paket_foto.odpf_paket_id
        -- JOIN order_detail_paket_upgrade ON order_detail_paket_upgrade.odpu_detcus_id = detail_customer.detcus_id
        -- JOIN paket_upgrade ON paket_upgrade.upg_id = order_detail_paket_upgrade.odpu_upg_id
        WHERE detcus_id ="'.$detcus_id.'";')->queryOne(); 
        return $this->render('view', [
            'model' => $this->findModel($detcus_id),
            'rowView' => $rowView,
        ]);
        
    }

    /**
     * Creates a new DetailCustomer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new DetailCustomer();
        $modelsOrderDetailPaketFoto = [new OrderDetailPaketFoto];
        $modelsOrderDetailPaketUpgrade = [new OrderDetailPaketUpgrade];
       
        if ($this->request->isPost) {
        
            if ($model->load($this->request->post()) && $model->save()) {

                //ubah format tanggal sebelum disave ke database
               $model->detcus_tanggal = \Yii::$app->formatter->asDate($model->detcus_tanggal, 'yyyy-MM-dd');

               //form tabularinput
               $modelsOrderDetailPaketFoto = Model::createMultiple(OrderDetailPaketFoto::classname());
               Model::loadMultiple($modelsOrderDetailPaketFoto, Yii::$app->request->post());
               $modelsOrderDetailPaketUpgrade = Model::createMultiple(OrderDetailPaketUpgrade::classname());
               Model::loadMultiple($modelsOrderDetailPaketUpgrade, Yii::$app->request->post());

                // validate all models
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelsOrderDetailPaketFoto) && $valid;
                $valid = Model::validateMultiple($modelsOrderDetailPaketUpgrade) && $valid;
            
                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $model->save(false)) {
                            foreach ($modelsOrderDetailPaketFoto as $modelOrderDetailPaketFoto) 
                            {
                                $modelOrderDetailPaketFoto->odpf_detcus_id = $model->detcus_id;
                                if (! ($flag = $modelOrderDetailPaketFoto->save(false)))  {
                                        $transaction->rollBack();
                                        break;
                                    }
                            }
                            foreach ($modelsOrderDetailPaketUpgrade as $modelOrderDetailPaketUpgrade) 
                            {
                                $modelOrderDetailPaketUpgrade->odpu_detcus_id = $model->detcus_id;
                                if (! ($flag = $modelOrderDetailPaketUpgrade->save(false)))  {
                                $transaction->rollBack();
                                break;
                                }
                            }
                            if ($flag) {
                                $transaction->commit();
                                return $this->redirect(['view', 'detcus_id' => $model->detcus_id]);
                            }
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }   
             }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'modelsOrderDetailPaketFoto' => (empty($modelsOrderDetailPaketFoto)) ? [new OrderDetailPaketFoto] : $modelsOrderDetailPaketFoto,
            'modelsOrderDetailPaketUpgrade' => (empty($modelsOrderDetailPaketUpgrade)) ? [new OrderDetailPaketFoto] : $modelsOrderDetailPaketUpgrade,
        ]);
        
    }

    /**
     * Updates an existing DetailCustomer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $detcus_id Detcus ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($detcus_id)
    {
        $model = $this->findModel($detcus_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'detcus_id' => $model->detcus_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DetailCustomer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $detcus_id Detcus ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($detcus_id)
    {
        $this->findModel($detcus_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DetailCustomer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $detcus_id Detcus ID
     * @return DetailCustomer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($detcus_id)
    {
        if (($model = DetailCustomer::findOne(['detcus_id' => $detcus_id])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPototema()
    {
        $modelODTema = TemaFoto::find()->all();
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('pototema', [
                'medias' => $modelODTema,
            ]);
        }else {
           return $this->render('pototema', [
               'medias' => $modelODTema
           ]);
        } 
    }

    public function actionCetakspk($detcus_id) {
        $rowTema = Yii::$app->db->createCommand('SELECT * FROM detail_customer 
        JOIN customer ON customer.cust_id = detail_customer.detcus_cust_id 
        JOIN order_detail_paket_foto ON order_detail_paket_foto.odpf_detcus_id = detail_customer.detcus_id 
        JOIN paket_foto ON paket_foto.paket_id = order_detail_paket_foto.odpf_paket_id 
        JOIN detail_service_spk ON detail_service_spk.serv_detcus_id = detail_customer.detcus_id 
        JOIN paket_kategori ON paket_kategori.kategori_id = paket_foto.paket_kategori_id 
        JOIN tema_foto ON tema_foto.tem_kategori_id = paket_kategori.kategori_id 
        JOIN order_detail_tema ON odtema_detcus_id = detail_customer.detcus_id 
        JOIN karyawan ON kary_id = detail_service_spk.serv_fotografer_id 
        WHERE detcus_id ="'.$detcus_id.'";')->queryOne();

        // get your HTML raw content without any layouts or scripts
        $model = DetailCustomer::findOne($detcus_id);
        $content = $this->renderPartial('cetakspk', ['model' => $model, 'rowTema' => $rowTema,]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '
            body{
                font-family:"garuda", "sans-serif";
                font-size:16px;
              }
            .keterangan {
                font-size : 14px;
                line-height: 14px;
            p{
                font-size: 10px;
                line-height: 12px;
                }
            #wrapper{
                width: 210.5mm;
                height: 150mm;
                margin: 0px;
            }
            #header{
                height: 25mm;
            }
            #header p{
                margin-bottom: 0px;
            }
            .row1{
                height: 50%
                margin: 0px;
            }
            .row2{
                height: 50%
                margin: 0px;
                background-color: yellow;
            }
        ', 
        ]);
        return $pdf->render('cetakspk'); 

    }

    public function actionPreview($detcus_id)
    {
        return $this->renderAjax('preview', [
            'model' => $this->findModel($detcus_id)
        ]);
    }

}
<?php

namespace backend\controllers;

use common\models\JenisFrame;
use common\models\JenisFrameSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * JenisFrameController implements the CRUD actions for JenisFrame model.
 */
class JenisFrameController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all JenisFrame models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new JenisFrameSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JenisFrame model.
     * @param int $frame_id Frame ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($frame_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($frame_id),
        ]);
    }

    /**
     * Creates a new JenisFrame model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new JenisFrame();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->frame_gambar = UploadedFile::getInstance($model, 'frame_gambar');
                $image_nama = $model->frame_nama_produk.rand(1, 4000).'.'.$model->frame_gambar->extension;
                $image_path = 'uploads/frame_foto/'.$image_nama;
                $model->frame_gambar->saveAs($image_path);
                $model->frame_gambar= $image_path;
                
                if($model->save())
               {    
                   //Yii::$app->getSession()->setFlash('success','Data saved!');
                   return $this->redirect(['view','frame_id'=>$model->frame_id]);
               }
               else
               {
                   Yii::$app->getSession()->setFlash('error','Data not saved!');
                   return $this->render('create', [
                         'model' => $model,
                   ]);
               }                        
                //return $this->redirect(['view', 'frame_id' => $lastInsertID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing JenisFrame model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $frame_id Frame ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($frame_id)
    {
        $model = $this->findModel($frame_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'frame_id' => $model->frame_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing JenisFrame model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $frame_id Frame ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($frame_id)
    {
        $this->findModel($frame_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JenisFrame model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $frame_id Frame ID
     * @return JenisFrame the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($frame_id)
    {
        if (($model = JenisFrame::findOne(['frame_id' => $frame_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

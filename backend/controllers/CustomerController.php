<?php

namespace backend\controllers;

use common\models\Customer;
use common\models\CustomerSearch;
use common\models\DetailCustomer;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\filters\AccessControl;
use Yii;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    //'only' => ['index'], <= ini kenapa di off, karena semua action butuh login
                    'rules' => [
                        [
                            //'actions' => ['index'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],

                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Customer models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Customer model.
     * @param int $cust_id Cust ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cust_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($cust_id),
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Customer();
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $cust_id Cust ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cust_id)
    {
        $model = $this->findModel($cust_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cust_id' => $model->cust_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $cust_id Cust ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cust_id)
    {
        $this->findModel($cust_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $cust_id Cust ID
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cust_id)
    {
        if (($model = Customer::findOne(['cust_id' => $cust_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetCustomer($cust_id)
    {
        $customer = Customer::findOne($cust_id);
        $detailCustomer = $customer->detailCustomer;
        $orderDetailPaketFoto = $detailCustomer->orderDetailPaketFoto;
        $paketFoto = $orderDetailPaketFoto->paketFoto ?? null;
        $orderDetailPaketUpgrade = $detailCustomer->orderDetailPaketUpgrade;
        $paketUpgrade = $orderDetailPaketUpgrade->paketUpgrade ?? null;
        $temaFoto = $paketFoto->temaFoto;

        $data['customer'] = $customer;
        $data['detailCustomer'] = $detailCustomer;
        $data['orderDetailPaketFoto'] = $orderDetailPaketFoto;
        $data['orderDetailPaketUpgrade'] = $orderDetailPaketUpgrade;
        $data['paketFoto'] = $paketFoto;
        $data['paketUpgrade'] = $paketUpgrade;
        $data['temaFoto'] = $temaFoto;

        return $this->asJson($data);

        // Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        // return [ 
        //     'cust_id' => $cust_id,
        //     'customer' => $customer,
        //     'detailCustomer' => $detailCustomer,
        //     'orderDetailPaketFoto' => $orderDetailPaketFoto,
        //     // ini bisa undefined, orderDetailPaketFoto
        //     'paketFoto' => $paketFoto
        // ];
    }

    public function actionGetCustomerDetail($cust_id)
    {
        $customer = Customer::findOne($cust_id);
        $detailCustomer = $customer->detailCustomer;
        
        $data['customer'] = $customer;
        $data['detailCustomer'] = $detailCustomer;
        return $this->asJson($data);
    }

    // public function actionGetCustomerPilihFoto($cust_id)
    // {
    //     $customer = Customer::findOne($cust_id);
    //     $detailCustomer = $customer->detailCustomer;
    //     $foto = $detailCustomer->$foto;
        
    //     $data['customer'] = $customer;
    //     $data['detailCustomer'] = $detailCustomer;
    //     $data['foto'] = $foto;
    //     return $this->asJson($data);
    // }

    public function actionGetCustomerPilihFoto($cust_id)
    {
        $customer = Customer::findOne($cust_id);
        $detailCustomer = $customer->detailCustomer;
        // $foto = null;
        $foto = $detailCustomer;
        
        $data['customer'] = $customer;
        $data['detailCustomer'] = $detailCustomer;
        $data['foto'] = $foto;
        return $this->asJson($data);
    }
}

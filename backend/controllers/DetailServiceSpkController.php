<?php

namespace backend\controllers;

use common\models\DetailServiceSpk;
use common\models\DetailServiceSpkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DetailServiceSpkController implements the CRUD actions for DetailServiceSpk model.
 */
class DetailServiceSpkController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all DetailServiceSpk models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DetailServiceSpkSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DetailServiceSpk model.
     * @param int $serv_id Serv ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($serv_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($serv_id),
        ]);
    }

    /**
     * Creates a new DetailServiceSpk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new DetailServiceSpk();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'serv_id' => $model->serv_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DetailServiceSpk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $serv_id Serv ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($serv_id)
    {
        $model = $this->findModel($serv_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'serv_id' => $model->serv_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DetailServiceSpk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $serv_id Serv ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($serv_id)
    {
        $this->findModel($serv_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DetailServiceSpk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $serv_id Serv ID
     * @return DetailServiceSpk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($serv_id)
    {
        if (($model = DetailServiceSpk::findOne(['serv_id' => $serv_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace backend\controllers;

use common\models\UkuranCetak;
use common\models\UkuranCetakSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UkuranCetakController implements the CRUD actions for UkuranCetak model.
 */
class UkuranCetakController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all UkuranCetak models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UkuranCetakSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UkuranCetak model.
     * @param int $ukuran_id Ukuran ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($ukuran_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($ukuran_id),
        ]);
    }

    /**
     * Creates a new UkuranCetak model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new UkuranCetak();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'ukuran_id' => $model->ukuran_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UkuranCetak model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $ukuran_id Ukuran ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($ukuran_id)
    {
        $model = $this->findModel($ukuran_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ukuran_id' => $model->ukuran_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UkuranCetak model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $ukuran_id Ukuran ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($ukuran_id)
    {
        $this->findModel($ukuran_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UkuranCetak model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $ukuran_id Ukuran ID
     * @return UkuranCetak the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ukuran_id)
    {
        if (($model = UkuranCetak::findOne(['ukuran_id' => $ukuran_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace backend\controllers;

use backend\models\PaketFoto;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\Model;
use common\models\PaketFotoSearch;
use common\models\PaketFrameDetail;
use Exception;

/**
 * PaketFotoController implements the CRUD actions for PaketFoto model.
 */
class PaketFotoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()  
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all PaketFoto models.
     *
     * @return string
     */


    public function actionIndex()
    {
        $searchModel = new PaketFotoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaketFoto model.
     * @param int $paket_id Paket ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($paket_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($paket_id),
        ]);
    }

    /**
     * Creates a new PaketFoto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new PaketFoto();
        $modelsPaketFrameDetail = [new PaketFrameDetail];

        if ($this->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $modelsPaketFrameDetail = Model::createMultiple(PaketFrameDetail::classname());
                foreach ($modelsPaketFrameDetail as $modelPaketFrameDetail) {
                    $modelPaketFrameDetail->fd_paket_id = $model->paket_id;
                }
                Model::loadMultiple($modelsPaketFrameDetail, Yii::$app->request->post());

                // validate all models
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelsPaketFrameDetail) && $valid;

                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $model->save()) {
                            foreach ($modelsPaketFrameDetail as $modelPaketFrameDetail) {
                                $modelPaketFrameDetail->fd_paket_id = $model->paket_id;
                                if (! ($flag = $modelPaketFrameDetail->save())) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['view', 'paket_id' => $model->paket_id]);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'modelsPaketFrameDetail' => (empty($modelsPaketFrameDetail)) ? [new PaketFrameDetail] : $modelsPaketFrameDetail
        ]);
        
    }

    // public function beforeAction($paket_id) {
    //     if($paket_id == 'actionCreate') {
    //         Yii::$app->request->enableCsrfValidation = false;
    //     }
    //     return parent::beforeAction($paket_id);
    // }

    /**
     * Updates an existing PaketFoto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $paket_id Paket ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($paket_id)
    {
        $model = $this->findModel($paket_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'paket_id' => $model->paket_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PaketFoto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $paket_id Paket ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($paket_id)
    {
        $this->findModel($paket_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaketFoto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $paket_id Paket ID
     * @return PaketFoto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($paket_id)
    {
        if (($model = PaketFoto::findOne(['paket_id' => $paket_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetPaketFoto($invoiceId)
    {
        $paketfoto = PaketFoto::findOne($invoiceId);
        echo Json::encode($paketfoto);
    }


}

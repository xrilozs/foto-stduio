<?php

namespace backend\controllers;

use common\models\Album;
use common\models\AlbumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AlbumController implements the CRUD actions for Album model.
 */
class AlbumController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Album models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AlbumSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Album model.
     * @param int $album_id Album ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($album_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($album_id),
        ]);
    }

    /**
     * Creates a new Album model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Album();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->album_gambar = UploadedFile::getInstance($model, 'album_gambar');
                $image_nama = $model->album_nama.rand(1, 4000).'.'.$model->album_gambar->extension;
                $image_path = 'uploads/album_foto/'.$image_nama;
                $model->album_gambar->saveAs($image_path);
                $model->album_gambar= $image_path;
                
                if($model->save())
               {    
                   //Yii::$app->getSession()->setFlash('success','Data saved!');
                   return $this->redirect(['view','album_id'=>$model->album_id]);
               }
               else
               {
                   Yii::$app->getSession()->setFlash('error','Data not saved!');
                   return $this->render('create', [
                         'model' => $model,
                   ]);
               }                        
                //return $this->redirect(['view', 'album_id' => $lastInsertID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Album model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $album_id Album ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($album_id)
    {
        $model = $this->findModel($album_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'album_id' => $model->album_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Album model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $album_id Album ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($album_id)
    {
        $this->findModel($album_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Album model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $album_id Album ID
     * @return Album the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($album_id)
    {
        if (($model = Album::findOne(['album_id' => $album_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

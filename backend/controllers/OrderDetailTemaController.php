<?php

namespace backend\controllers;

use Yii;
use backend\models\OrderDetailTemaChooseForm;
use backend\models\OrderDetailTemaCreateForm;
use common\models\OrderDetailTema;
use common\models\Customer;
use common\models\DetailCustomer;
use common\models\DetailServiceSpk;
use common\models\OrderDetailTemaSearch;
use common\models\TemaFoto;
use yii\db\ActiveQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * OrderDetailTemaController implements the CRUD actions for OrderDetailTema model.
 */
class OrderDetailTemaController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all OrderDetailTema models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new OrderDetailTemaSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderDetailTema model.
     * @param int $odtema_id Odtema ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($odtema_detcus_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($odtema_detcus_id),
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionChoose()
    {
        $customers = Customer::find()
            ->joinWith(['detailCustomer' => function (ActiveQuery $query) {
                return $query->where(['detcus_status' => 'Lunas']);
            }])
            ->all();
        $model = new OrderDetailTemaChooseForm();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->validate()) {
                return $this->redirect(['create', 'cust_id' => $model->cust_id]);
            }
        }

        return $this->render('choose', [
            'customers' => $customers,
            'model' => $model,
        ]);
    }

    /**
     * Creates a new OrderDetailTema model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($cust_id)
    {
        $customer = Customer::findOne($cust_id);
        if (is_null($customer)) {
            throw new NotFoundHttpException();
        }

        $detailCustomer = $customer->detailCustomer;

        $orderDetailPaketFoto = $detailCustomer->orderDetailPaketFoto;

        $paketFoto = $orderDetailPaketFoto->paketFoto;

        $paketKategori = $paketFoto->paketKategori;

        $temaFotos = TemaFoto::find()->where(['tem_kategori_id' => $paketKategori->kategori_id])->all();


        $customers = Customer::find()->all();
        $model = new OrderDetailTemaCreateForm();
        $model->cust_id = $customer->cust_id;
        $model->cust_nama = $customer->cust_nama;
        $model->detcus_tanggal = $detailCustomer->detcus_tanggal;
        $model->paket_nama = $paketFoto->paket_nama;
        $model->jumlah_tema = $paketFoto->paket_jumlah_tema;
        $model->paket_pose = $paketFoto->paket_pose;

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->validate()) {
                foreach ($model->tem_id as $temId) {
                    $orderDetailTema = new OrderDetailTema();
                    $orderDetailTema->odtema_detcus_id = $detailCustomer->detcus_id;
                    $orderDetailTema->odtema_tem_id = intval($temId);
                    $orderDetailTema->odtema_ket = $model->ket;
                    $orderDetailTema->save();
                }

                $detailServiceSpk = new DetailServiceSpk();
                $detailServiceSpk->serv_detcus_id = $detailCustomer->detcus_id;
                $detailServiceSpk->serv_studio = $model->studio;
                $detailServiceSpk->serv_fotografer_id = $model->fotografer_id;
                $detailServiceSpk->serv_asisten_id = $model->asisten_id;
                $detailServiceSpk->serv_cs_id = $model->cs_id;
                $detailServiceSpk->save();
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'customers' => $customers,
            'model' => $model,
            'temaFotos' => $temaFotos,
            //'customerbelumlunas' => $custbelumnunas, // ini nanti maksudnya nanti dipakai di form
        ]);
    }

    /**
     * Updates an existing OrderDetailTema model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $odtema_id Odtema ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */

    /**
     * Updates an existing OrderDetailTema model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $odtema_id Odtema ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($odtema_id)
    {
        $model = $this->findModel($odtema_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'odtema_id' => $model->odtema_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrderDetailTema model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $odtema_id Odtema ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($odtema_id)
    {
        $this->findModel($odtema_id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPreview($odtema_detcus_id)
    {
        return $this->render('preview', [
            'model' => $this->findModel($odtema_detcus_id)
        ]);
    }

    /**
     * Finds the OrderDetailTema model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $odtema_id Odtema ID
     * @return OrderDetailTema the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($odtema_id)
    {
        if (($model = OrderDetailTema::findOne(['odtema_id' => $odtema_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
 }

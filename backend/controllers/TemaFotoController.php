<?php

namespace backend\controllers;

use common\models\TemaFoto;
use common\models\PaketFotoSearch;
use common\models\TemaFotoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TemaFotoController implements the CRUD actions for TemaFoto model.
 */
class TemaFotoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all TemaFoto models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TemaFotoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TemaFoto model.
     * @param int $tem_id Tem ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($tem_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($tem_id),
        ]);
    }

    /**
     * Creates a new TemaFoto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new TemaFoto();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->tem_gambar = UploadedFile::getInstance($model, 'tem_gambar');
                $image_nama = $model->tem_nama.rand(1, 4000).'.'.$model->tem_gambar->extension;
                $image_path = 'uploads/tema_gambar/'.$image_nama;
                $model->tem_gambar->saveAs($image_path);
                $model->tem_gambar= $image_path;
                
                if($model->save())
               {    
                   //Yii::$app->getSession()->setFlash('success','Data saved!');
                   return $this->redirect(['view','tem_id'=>$model->tem_id]);
               }
               else
               {
                   Yii::$app->getSession()->setFlash('error','Data not saved!');
                   return $this->render('create', [
                         'model' => $model,
                   ]);
               }                        
                //return $this->redirect(['view', 'tem_id' => $lastInsertID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TemaFoto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $tem_id Tem ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($tem_id)
    {
        $model = $this->findModel($tem_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'tem_id' => $model->tem_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TemaFoto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $tem_id Tem ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($tem_id)
    {
        $this->findModel($tem_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TemaFoto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $tem_id Tem ID
     * @return TemaFoto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($tem_id)
    {
        if (($model = TemaFoto::findOne(['tem_id' => $tem_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

function setPackageEvent() {
    $('.odpf_paket_id').on('change', function (e) {
        $('#' + e.currentTarget.id.replace('odpf_paket_id', 'odpf_harga')).val(0);
         setSubtotal();
         setSisaPelunasan();
         setGrandTotal();
    });
    $('.odpf_paket_id').on('select2:select', function (e) {
        $('#' + e.currentTarget.id.replace('odpf_paket_id', 'odpf_harga')).val(e.params.data.paket_harga);
         setSubtotal();
         setGrandTotal();
    });
    $('.odpf_qty').on('keyup', function () {
        setSubtotal();
        setSisaPelunasan();
        setGrandTotal();
    });
    $('.detcus_pay').on('keyup', function () {
        setSisaPelunasan();
        setGrandTotal();
    })
    $('.odpu_upg_id').on('change', function (e) {
        $('#' + e.currentTarget.id.replace('odpu_upg_id', 'odpu_harga')).val(0);
        setSubtotal();
        setGrandTotal();
    });
    $('.odpu_upg_id').on('select2:select', function (e) {
        $('#' + e.currentTarget.id.replace('odpu_upg_id', 'odpu_harga')).val(e.params.data.upg_harga);
        setSubtotal();
        setGrandTotal();
    });
     $('.odpu_qty').on('keyup', function () {
        setSubtotal();
        setGrandTotal();
    });
}

function setSisaPelunasan() {
    let sisaPelunasan = $('#subtotal').val() - parseInt($('.detcus_pay').val() || 0);
    $('#sisa_pelunasan').val(sisaPelunasan);
}

function setSubtotal() {
    let subtotal = 0;
    $('.odpf_qty').each(function (index) {
        subtotal += $('.odpf_qty:eq(' + index + ')').val() * $('.odpf_harga:eq(' + index + ')').val();
    });
    $('#subtotal').val(subtotal);
    let subtotalupgrade = 0;  
    $('.odpu_qty').each(function (index) {
        subtotalupgrade += $('.odpu_qty:eq(' + index + ')').val() * $('.odpu_harga:eq(' + index + ')').val();
    });
    $('#subtotalupgrade').val(subtotalupgrade);
}

function setGrandTotal(){
    //let grandtotal = 0;
    var subtotal = parseInt($('#subtotal').val());
    var detcus_pay = parseInt($('.detcus_pay').val() || 0);
    var subtotalupgrade = parseInt($('#subtotalupgrade').val());
    grandtotal = subtotal - detcus_pay + subtotalupgrade;
    $('#grandtotal').val(grandtotal);
}

setPackageEvent();

$('.dynamicform_wrapper, .dynamicform_wrapper_upgrade').on('afterInsert', function(e, item) {
    setPackageEvent();
});

// $('.dynamicform_wrapper_upgrade').on('afterInsert', function(e, item) {
//     setPackageEvent();
// });
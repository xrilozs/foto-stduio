function setJuispk() {
    $("#project").autocomplete({
        minLength: 0,
        source: projects,
        focus: function( event, ui ) {
        $( "#project" ).val( ui.item.label );
        return false;
        },
        select: function( event, ui ) {
        // $( "#project" ).val( ui.item.label ;
        // $( "#project-id" ).val( ui.item.detcus.detcus_id);
        $( "#tgldaftar").html( ui.item.detcus.detcus_tanggal);
        $( "#namacustomer").html( ui.item.cust.cust_nama);
        $( "#paketfoto").html( ui.item.pf.paket_nama);
        $( "#jumlahtema").html( ui.item.pf.paket_jumlah_tema);
        $( "#jumlahpose").html( ui.item.pf.paket_pose);

        //$( "#project-icon" ).attr( "src", "images/" + ui.item.icon );

        return false;
        }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
        .append( "<div>" + item.detcus.detcus_tanggal + "<br>" + item.cust.cust_nama + "</div> "+ "<br>" + item.pf.paket_nama + "</div>" + "<br>" + item.pf.paket_jumlah_tema + "</div>" + "<br>" + item.pf.paket_pose + "</div>" )
        .appendTo( ul );
    };

    $("#project").die();
}

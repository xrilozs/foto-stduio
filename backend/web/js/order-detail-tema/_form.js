function temIdCheckedOrUnchecked()
{
    const jumlahTema = Number($('[name="OrderDetailTemaCreateForm[jumlah_tema]"]').val());

    const totalTemIdChecked = $('.tem_id_input_checkbox:checked').length;

    if (jumlahTema === totalTemIdChecked) {
        $('.tem_id_input_checkbox').each(function (i) {
            if (! $(this).is(':checked')) {
                $(this).prop('disabled', true);
                $('.tem_id_div').eq(i).addClass('disabled');
            }
        });
    } else {
        $('.tem_id_input_checkbox').each(function (i) {
            $(this).prop('disabled', false);
            $('.tem_id_div').eq(i).removeClass('disabled');
        });
    }
}

$(document).on('click', '.tem_id_input_checkbox', function () {
    temIdCheckedOrUnchecked();
});

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderDetailPaketUpgradeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Detail Paket Upgrades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-detail-paket-upgrade-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Order Detail Paket Upgrade', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'odpu_id',
            'odpu_detcus_id',
            'odpu_upg_id',
            'odpu_qty',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, OrderDetailPaketUpgrade $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'odpu_id' => $model->odpu_id]);
                 }
            ],
        ],
    ]); ?>


</div>

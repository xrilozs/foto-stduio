<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailPaketUpgrade */

$this->title = 'Create Order Detail Paket Upgrade';
$this->params['breadcrumbs'][] = ['label' => 'Order Detail Paket Upgrades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-detail-paket-upgrade-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

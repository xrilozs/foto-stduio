<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailPaketUpgrade */

$this->title = $model->odpu_id;
$this->params['breadcrumbs'][] = ['label' => 'Order Detail Paket Upgrades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-detail-paket-upgrade-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'odpu_id' => $model->odpu_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'odpu_id' => $model->odpu_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'odpu_id',
            'odpu_detcus_id',
            'odpu_upg_id',
            'odpu_qty',
        ],
    ]) ?>

</div>

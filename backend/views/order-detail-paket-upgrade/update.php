<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailPaketUpgrade */

$this->title = 'Update Order Detail Paket Upgrade: ' . $model->odpu_id;
$this->params['breadcrumbs'][] = ['label' => 'Order Detail Paket Upgrades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->odpu_id, 'url' => ['view', 'odpu_id' => $model->odpu_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-detail-paket-upgrade-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

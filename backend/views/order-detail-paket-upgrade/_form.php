<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailPaketUpgrade */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-detail-paket-upgrade-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'odpu_detcus_id')->textInput() ?>

    <?= $form->field($model, 'odpu_upg_id')->textInput() ?>

    <?= $form->field($model, 'odpu_qty')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

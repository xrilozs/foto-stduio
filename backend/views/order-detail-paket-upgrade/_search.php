<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailPaketUpgradeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-detail-paket-upgrade-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'odpu_id') ?>

    <?= $form->field($model, 'odpu_detcus_id') ?>

    <?= $form->field($model, 'odpu_upg_id') ?>

    <?= $form->field($model, 'odpu_qty') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

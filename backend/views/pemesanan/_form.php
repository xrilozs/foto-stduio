<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use common\models\Customer;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Pemesanan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Input Jadwal Pemesanan</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="customer-form">
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form-horizontal', 
                    'type' => ActiveForm::TYPE_HORIZONTAL,
                    'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
                ]); ?>

                <?= $form->field($model, 'pes_cust_id')->widget(TypeaheadBasic::classname(), [
                            'data' => ArrayHelper::map(Customer::find()->all(),'cust_id', 'cust_no_invoice'),
                            'options' => ['placeholder' => 'Filter as you type ...'],
                            'pluginOptions' => ['highlight'=>true],
                        ]); ?>

                <?= $form->field($model, 'pes_tgl_foto')->widget(DatePicker::classname(), [
                        'name' => 'cust_tgl_foto',
                        'type' => DatePicker::TYPE_INPUT,
                        'value' => 'pes_tgl_foto',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                        ]
                    ]); ?>

                <?= $form->field($model, 'pes_jam_foto')->textInput() ?>

                <?= $form->field($model, 'pes_tgl_pilih_foto')->widget(DatePicker::classname(), [
                        'name' => 'cust_tgl_foto',
                        'type' => DatePicker::TYPE_INPUT,
                        'value' => 'pes_tgl_pilih_foto',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                        ]
                    ]); ?>

                <?= $form->field($model, 'pes_tgl_review')->widget(DatePicker::classname(), [
                        'name' => 'cust_tgl_foto',
                        'type' => DatePicker::TYPE_INPUT,
                        'value' => 'pes_tgl_review',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                        ]
                    ]); ?>

                <?= $form->field($model, 'pes_tgl_deadline')->widget(DatePicker::classname(), [
                        'name' => 'cust_tgl_foto',
                        'type' => DatePicker::TYPE_INPUT,
                        'value' => 'pes_tgl_deadline',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                        ]
                    ]); ?>

                <?= $form->field($model, 'pes_status_pembayaran')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
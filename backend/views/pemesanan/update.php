<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pemesanan */

$this->title = 'Update Pemesanan: ' . $model->pes_id;
$this->params['breadcrumbs'][] = ['label' => 'Pemesanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pes_id, 'url' => ['view', 'pes_id' => $model->pes_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pemesanan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PemesananSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Schedule Order List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pemesanan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pemesanan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'pes_id',
            'pes_cust_id',
            'pes_tgl_foto',
            'pes_jam_foto',
            'pes_tgl_pilih_foto',
            'pes_tgl_review',
            'pes_tgl_deadline',
            'pes_status_pembayaran',
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, Pemesanan $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'pes_id' => $model->pes_id]);
            //      }
            // ],
        ],
    ]); ?>


</div>

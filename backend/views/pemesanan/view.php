<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Pemesanan */

$this->title = $model->pes_id;
$this->params['breadcrumbs'][] = ['label' => 'Pemesanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pemesanan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'pes_id' => $model->pes_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'pes_id' => $model->pes_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'pes_id',
            'pes_cust_id',
            'pes_tgl_foto',
            'pes_jam_foto',
            'pes_tgl_pilih_foto',
            'pes_tgl_review',
            'pes_tgl_deadline',
            'pes_status_pembayaran',
        ],
    ]) ?>

</div>

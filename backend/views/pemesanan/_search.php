<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PemesananSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pemesanan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pes_id') ?>

    <?= $form->field($model, 'pes_cust_id') ?>

    <?= $form->field($model, 'pes_tgl_foto') ?>

    <?= $form->field($model, 'pes_jam_foto') ?>

    <?= $form->field($model, 'pes_tgl_pilih_foto') ?>

    <?php // echo $form->field($model, 'pes_tgl_review') ?>

    <?php // echo $form->field($model, 'pes_tgl_deadline') ?>

    <?php // echo $form->field($model, 'pes_status_pembayaran') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

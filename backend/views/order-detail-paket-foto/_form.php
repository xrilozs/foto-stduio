<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailPaketFoto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-detail-paket-foto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'odpf_detcus_id')->textInput() ?>

    <?= $form->field($model, 'odpf_paket_id')->textInput() ?>

    <?= $form->field($model, 'odpf_qty')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

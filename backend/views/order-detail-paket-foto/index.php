<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderDetailPaketFotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Detail Paket Fotos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-detail-paket-foto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Order Detail Paket Foto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'odpf_id',
            'odpf_detcus_id',
            'odpf_paket_id',
            'odpf_qty',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, OrderDetailPaketFoto $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'odpf_id' => $model->odpf_id]);
                 }
            ],
        ],
    ]); ?>


</div>

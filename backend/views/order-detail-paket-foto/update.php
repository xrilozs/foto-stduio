<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailPaketFoto */

$this->title = 'Update Order Detail Paket Foto: ' . $model->odpf_id;
$this->params['breadcrumbs'][] = ['label' => 'Order Detail Paket Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->odpf_id, 'url' => ['view', 'odpf_id' => $model->odpf_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-detail-paket-foto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailPaketFotoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-detail-paket-foto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'odpf_id') ?>

    <?= $form->field($model, 'odpf_detcus_id') ?>

    <?= $form->field($model, 'odpf_paket_id') ?>

    <?= $form->field($model, 'odpf_qty') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use common\models\PaketFoto;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\form\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\JenisFrame */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Input Paket Foto</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="customer-form">
                <?php $form = ActiveForm::begin([
                    'options'=>['enctype'=>'multipart/form-data']
                ]); ?>

                <div class="row">
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'frame_nama_produk')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'frame_ukuran')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'frame_warna')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="form-group col-md-6">
                    <?= $form->field($model, 'frame_harga')->textInput() ?>
                    </div>
                </div>

                <?= $form->field($model, 'frame_gambar')->fileInput() ?>

                <?= $form->field($model, 'frame_ket')->textArea(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\JenisFrame */

$this->title = "Nama Frame ".$model->frame_nama_produk;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Frame', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="jenis-frame-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Ubah', ['update', 'frame_id' => $model->frame_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'frame_id' => $model->frame_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'frame_nama_produk',
            'frame_ukuran',
            'frame_warna',
            [
                'attribute'=>'frame_gambar',
                'value'=>$model->frame_gambar,
                'format' => ['image',['width'=>'80','height'=>'100']],                 
            ],
            'frame_harga',
            'frame_ket',
        ],
    ]) ?>

</div>

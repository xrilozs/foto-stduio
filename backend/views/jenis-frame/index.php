<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JenisFrameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Produk Frame';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-frame-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambah Produk Frame', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'frame_nama_produk',
            [
                'attribute'=>'frame_gambar',
                'value'=>'frame_gambar',
                'format' => ['image',['width'=>'80','height'=>'100']],                 
            ],
            'frame_ukuran',
            'frame_warna',
            [
                'attribute' => 'frame_harga',
                'format' => 'currency',
            ],
            'frame_ket',
        ],
    ]); ?>


</div>

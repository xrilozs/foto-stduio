<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Customer';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambah Customer', ['create'], ['class' => 'btn btn-success btn-lg']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cust_id',
            'cust_no_invoice',
            'cust_nama',
            'cust_telp',
            'cust_email:email',
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, Customer $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'cust_id' => $model->cust_id]);
            //      }
            // ],
        ],
    ]); ?>


</div>

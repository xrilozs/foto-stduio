<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Input Data Customer</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
			<div class="customer-form">
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form-horizontal', 
                    'type' => ActiveForm::TYPE_HORIZONTAL,
                    'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
                ]); ?>

			    <?= $form->field($model, 'cust_no_invoice')->textInput(['maxlength' => true]) ?>

			    <?= $form->field($model, 'cust_nama')->textInput(['maxlength' => true]) ?>

			    <?= $form->field($model, 'cust_telp')->textInput() ?>

			    <?= $form->field($model, 'cust_email')->textInput(['maxlength' => true]) ?>

			    <div class="form-group">
			        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
			    </div>

			    <?php ActiveForm::end(); ?>

			</div>
			</div>
        </div>
    </div>
</div>
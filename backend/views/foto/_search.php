<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FotoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="foto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'foto_id') ?>

    <?= $form->field($model, 'foto_detcus_id') ?>

    <?= $form->field($model, 'foto_nama_file') ?>

    <?= $form->field($model, 'foto_image') ?>

    <?= $form->field($model, 'foto_ket') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use buttflattery\formwizard\FormWizard;
use yii\widgets\ActiveForm; 
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="row">
    <?php 
    echo FormWizard::widget([
        'theme' => FormWizard::THEME_DOTS,
        'steps' => [
            [
                'model' => $model,
                'title' => 'Pilih Foto',
                'description' => 'Add your shoots',
                'formInfoText' => 'Fill all fields'
            ],
            [
                'model' => $model,
                'title' => 'Rekap',
                'description' => 'Add your shoots',
                'formInfoText' => 'Fill all fields'
            ],
            [
                'model' => $model,
                'title' => 'Finalisasi',
                'description' => 'Add your shoots',
                'formInfoText' => 'Fill all fields'
            ],
        ]
    ]);
    ?>
</div>
<div class="x_content">

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Media Gallery <small> gallery design </small></h2>
                <div class="row">
                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask">
                        <p>Your Text</p>
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p>Snow and Ice Incoming for the South</p>
                    </div>
                </div>
                </div>
                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask">
                        <p>Your Text</p>
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p>Snow and Ice Incoming for the South</p>
                    </div>
                </div>
                </div>
                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask">
                        <p>Your Text</p>
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p>Snow and Ice Incoming for the South</p>
                    </div>
                </div>
                </div>
                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask">
                        <p>Your Text</p>
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p>Snow and Ice Incoming for the South</p>
                    </div>
                </div>
                </div>
                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask">
                        <p>Your Text</p>
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p>Snow and Ice Incoming for the South</p>
                    </div>
                </div>
                </div>


                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask no-caption">
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p><strong>Image Name</strong>
                    </p>
                    <p>Snow and Ice Incoming</p>
                    </div>
                </div>
                </div>
                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask no-caption">
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p><strong>Image Name</strong>
                    </p>
                    <p>Snow and Ice Incoming</p>
                    </div>
                </div>
                </div>
                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask no-caption">
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p><strong>Image Name</strong>
                    </p>
                    <p>Snow and Ice Incoming</p>
                    </div>
                </div>
                </div>
                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask no-caption">
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p><strong>Image Name</strong>
                    </p>
                    <p>Snow and Ice Incoming</p>
                    </div>
                </div>
                </div>
                <div class="col-md-55">
                <div class="thumbnail">
                    <div class="image view view-first">
                    <img style="width: 100%; display: block;" src="images/media.jpg" alt="image" />
                    <div class="mask no-caption">
                        <div class="tools tools-bottom">
                        <a href="#"><i class="fa fa-link"></i></a>
                        <a href="#"><i class="fa fa-pencil"></i></a>
                        <a href="#"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    </div>
                    <div class="caption">
                    <p><strong>Image Name</strong>
                    </p>
                    <p>Snow and Ice Incoming</p>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

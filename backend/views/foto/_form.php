<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\Customer;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Foto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Upload Foto Client</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="customer-form">
              <?php $form = ActiveForm::begin([
                  'options'=>['enctype'=>'multipart/form-data']
              ]); ?>

              <?php 
                  $customer = ArrayHelper::map(Customer::find()->all(), 'cust_id', function($model, $default){
                      return $model["cust_no_invoice"];
                  });
              ?>

              <?= $form->field($model, 'foto_detcus_id')->widget(Select2::classname(), [
                  'data' => $customer,
                  'language' => 'en',
                  'options' => [
                      'value' => (!$model->isNewRecord ? $selected : ''), 
                      'placeholder' => 'Input No Invoice',
                      'id' => 'noinvoice'
                  ],
                  'pluginOptions' => [
                      'allowClear' => true,
                  ]
                ]); 
              ?>

              <div class="form-group">
                  <label for="inputNamaCustomer">Nama Customer</label>
                  <input type="text" class="form-control" id="namacustomer" placeholder="Nama Customer" readonly>
              </div>

              <?= $form->field($model, 'foto_nama_file[]')->fileInput(['multiple' => true, 'accept' => 'web*']) ?>

              <div class="form-group">
                  <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
              </div>

              <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<<JS
 $('#noinvoice').change(function(){
     var cust_id = $(this).val();
     $.get('index.php?r=customer/get-customer-pilih-foto', { cust_id : cust_id }, function(data) {
         console.log("DATA:", data)
         $('#namacustomer').attr('value', data.customer.cust_nama);
     })
 });
JS;
$this->registerJs($script);
 
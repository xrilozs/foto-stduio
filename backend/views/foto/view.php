<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Foto */

$this->title = $customer->cust_nama."(".$customer->cust_no_invoice.")";
$this->params['breadcrumbs'][] = ['label' => 'Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="foto-view" style="margin:20px;">
    <h3>Foto <?php echo Html::encode($this->title) ?> Success Upload!</h3>
    <div class="row" style="margin-top:20px;">
        <?php foreach ($model as $mod) { ?>
            <div class="col-md-3">
                <img src="<?php echo Yii::getAlias('@web/uploads').'/'.$mod->foto_image;?>" class="card-mg-top" style="max-height:100px;">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $mod->foto_nama_file;?></h5>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-lg-12" style="text-align:center;">
            <?php echo Html::a('Back', ['create'], ['class' => 'btn btn-default']) ?>
            <!-- <a href="" class="btn btn-default">Back</a> -->
        </div>
    </div>
</div>

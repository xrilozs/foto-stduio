<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AlbumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Produk Album';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="album-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambah Produk Album', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'album_id',
            'album_nama',
            'album_warna',
            [
                'attribute'=>'album_gambar',
                'value'=>'album_gambar',
                'format' => ['image',['width'=>'80','height'=>'100']],                 
            ],
            [
                'attribute' => 'album_harga',
                'format' => 'currency',
            ],
            'album_ket',
        ],
    ]); ?>


</div>

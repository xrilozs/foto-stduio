<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DetailServiceSpkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detail Service Spks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-service-spk-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Detail Service Spk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'serv_id',
            'serv_cust_id',
            'serv_kary_id',
            'serv_studio',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, DetailServiceSpk $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'serv_id' => $model->serv_id]);
                 }
            ],
        ],
    ]); ?>


</div>

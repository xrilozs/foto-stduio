<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DetailServiceSpk */

$this->title = 'Create Detail Service Spk';
$this->params['breadcrumbs'][] = ['label' => 'Detail Service Spks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-service-spk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

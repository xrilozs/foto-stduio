<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DetailServiceSpk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detail-service-spk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'serv_cust_id')->textInput() ?>

    <?= $form->field($model, 'serv_kary_id')->textInput() ?>

    <?= $form->field($model, 'serv_studio')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

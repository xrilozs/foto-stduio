<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DetailServiceSpk */

$this->title = 'Update Detail Service Spk: ' . $model->serv_id;
$this->params['breadcrumbs'][] = ['label' => 'Detail Service Spks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->serv_id, 'url' => ['view', 'serv_id' => $model->serv_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="detail-service-spk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DetailServiceSpk */

$this->title = $model->serv_id;
$this->params['breadcrumbs'][] = ['label' => 'Detail Service Spks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="detail-service-spk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'serv_id' => $model->serv_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'serv_id' => $model->serv_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'serv_id',
            'serv_cust_id',
            'serv_kary_id',
            'serv_studio',
        ],
    ]) ?>

</div>

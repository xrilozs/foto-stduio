<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaketKategori */

$this->title = $model->kategori_id;
$this->params['breadcrumbs'][] = ['label' => 'Paket Kategoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="paket-kategori-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'kategori_id' => $model->kategori_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'kategori_id' => $model->kategori_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kategori_id',
            'kategori_nama',
        ],
    ]) ?>

</div>

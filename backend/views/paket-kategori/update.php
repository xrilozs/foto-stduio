<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaketKategori */

$this->title = 'Update Paket Kategori: ' . $model->kategori_id;
$this->params['breadcrumbs'][] = ['label' => 'Paket Kategoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kategori_id, 'url' => ['view', 'kategori_id' => $model->kategori_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paket-kategori-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaketKategori */

$this->title = 'Form Paket Kategori';
$this->params['breadcrumbs'][] = ['label' => 'Paket Kategoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-kategori-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

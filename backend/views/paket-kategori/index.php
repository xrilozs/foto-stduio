<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PaketKategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategori Paket';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-kategori-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambah Kategori Paket', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'kategori_id',
            'kategori_nama',
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, PaketKategori $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'kategori_id' => $model->kategori_id]);
            //      }
            // ],
        ],
    ]); ?>


</div>

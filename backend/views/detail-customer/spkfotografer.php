<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\depdrop\DepDrop; 
use kartik\select2\Select2;
use common\models\Karyawan;
use common\models\DetailCustomer;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailTema */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
        	<div class="title_left">
				<h3>Form SPK FOTOGRAFER</h3>
			</div>
            <div class="x_title">
                <h2><small>Silahkan Input Paket Foto</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="customer-form">
                <?php $form = ActiveForm::begin([
					'id' => 'customer-form',
                    // 'type' => ActiveForm::TYPE_HORIZONTAL,
                    // 'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
                ]); ?>

			 
				<!-- // $data = DetailCustomer::find()
				// 	->select('*')
				// 	->from('detail_customer as detcus')
				// 	->join('JOIN', 'customer as cust', 'detcus.detcus_cust_id = cust.cust_id')
				// 	->join('JOIN', 'order_detail_paket_foto as odpf', 'odpf.odpf_detcus_id = detcus.detcus_id')
				// 	->join('JOIN', 'order_detail_paket_upgrade AS odpu', 'odpu.odpu_detcus_id = detcus.detcus_id')
				// 	->join('JOIN', 'paket_foto as pf', 'pf.paket_id = odpf.odpf_paket_id')
				// 	->join('JOIN', 'paket_upgrade as pu', 'pu.upg_id = odpu.odpu_upg_id')
				// 	->join('JOIN', 'transaksi as trans', 'trans.trans_detcus_id = detcus.detcus_id')
				// 	->asArray();


				// $form->field($modelODTema, 'detcus_cust_id')->widget(AutoComplete::className(), [
				// 	'clientOptions' => [
				// 		'source' => $data,
				// 		'minLength'=>'3', 
				// 		'autoFill'=>true,
				// 		'select' => new JsExpression("function( event, ui ) {
				// 		$('#detcus_id').val(ui.item.detcus_id);//#detcus_id is the id of hiddenInput.
				// 		$('#cust_no_invoice').val(ui.item.cust_no_invoice);
				// 		}"
				// 	)],
				// 	]); -->
			



                <form>
				  <div class="form-row">
				    <div class="form-group col-md-6">
				      <label for="noinvoice">No Invoice</label>
				      <input type="text" class="form-control" id="cust_no_invoice" placeholder="No Invoice" autocomplete="off">
				    </div>
				    <div class="form-group col-md-6">
				      <label for="tgldaftar">Tanggal</label>
				      <input type="text" class="form-control" id="tgldaftar" placeholder="Nama Customer" readonly="readonly">
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="namacustomer">Nama Customer</label>
				    <input type="text" class="form-control" id="namacustomer" placeholder="Nama Customer" readonly="readonly">
				  </div>
				  <div class="form-group">
				    <label for="paketfoto">Paket Foto</label>
				    <input type="text" class="form-control" id="paketfoto" placeholder="Nama Paket" readonly="readonly">
				  </div>

				    <div class="form-group col-md-6">
				      <label for="jumlahTema">Jumlah Tema</label>
				      <input type="text" class="form-control" id="jumlahtema" readonly="readonly">
				    </div>
				    <div class="form-group col-md-6">
				      <label for="jumlahPose">Jumlah Pose</label>
				      <input type="text" class="form-control" id="jumlahpose" readonly="readonly">
				    </div>

				
				  <!-- <button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#addContactFormModel">
					  <span  class="hand-cursor-pointer quick-add-contact" title="Add Contact">
						  <i class="fa fa-plus-circle" aria-hidden="true"></i>Add Contact Via Model</span>
				  </button> -->

				  <?= Html::a('PILIH POTO TEMA', ['pototema'], ['class' => 'btn btn-primary']) ?>
				  <?= Html::button('PILIH POTO TEMA', ['value' => Url::to(['detail-customer/pototema']), 'title' => 'PILIH POTO TEMA', 'class' => 'showModalButton btn btn-success']); ?>

				  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm" >Small modal</button>
				  
				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Keterangan untuk Tim Foto</label>
				    <textarea class="form-control" id="exampleFormControlTextarea1"></textarea>
				  </div>

				  <div class="form-row">
				    <div class="form-group col-md-3">
				      <label for="inputCity">Studio</label>
				      <input type="text" class="form-control" id="studio">
				    </div>
				    <div class="form-group col-md-3">
				      <label for="inputState">Fotografer</label>
				      <select id="inputState" class="form-control">
				        <option selected>Choose...</option>
				        <option>...</option>
				      </select>
				    </div>
				    <div class="form-group col-md-3">
				      <label for="inputZip">Asisten</label>
				      <input type="text" class="form-control" id="asisten">
				    </div>
				    <div class="form-group col-md-3">
				      <label for="inputZip">CS</label>
				      <input type="text" class="form-control" id="cs">
				    </div>
				  </div>
				  <div class="form-group">
				    <div class="form-check">
				      <input class="form-check-input" type="checkbox" id="gridCheck">
				      <label class="form-check-label" for="gridCheck">
				        Check me out
				      </label>
				    </div>
				  </div>
				  <button type="submit" class="btn btn-primary">Sign in</button>
				</form>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>

<?php
// yii\bootstrap\Modal::begin([
//     'headerOptions' => ['id' => 'modalHeader'],
//     'id' => 'modal',
//     'size' => 'modal-lg',
//     'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
// ]);
// echo "<div id='modalContent'></div>";
// yii\bootstrap\Modal::end();
?>

<!-- <script>
	$(function(){
      $(document).on('click', '.showModalButton', function(){
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                    .load($(this).attr('value'));
            //dynamiclly set the header for the modal
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        } else {
            //if modal isn't open; open it and load content
            $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('value'));
             //dynamiclly set the header for the modal
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        }
    });
});
</script> -->

<?php
Modal::begin([
	// 'header'=>'', 
	'id'=>'modal',
	'size'=>'modal-lg',
	'clientOptions' => ['backdrop' => 'dinamis', 'keyboard' => FALSE],
	// 'footer' => ''
]);
echo "<div id='modalContent'></div>";
Modal::end();

$project = (new \yii\db\Query())
        ->select(['cust.cust_no_invoice', 'cust.cust_nama', 'detcus.detcus_tanggal', 'pf.paket_nama', 'pf.paket_harga', 'odpf.odpf_qty', 'pu.upg_nama', 'pu.upg_harga', 'odpu.odpu_qty', 'pf.paket_jumlah_tema', 'pf.paket_pose', 'trans.trans_status', 'trans.trans_pay'])
        ->from('detail_customer as detcus')
        ->join('JOIN', 'customer as cust', 'detcus.detcus_cust_id = cust.cust_id')
        ->join('JOIN', 'order_detail_paket_foto as odpf', 'odpf.odpf_detcus_id = detcus.detcus_id')
        ->join('JOIN', 'order_detail_paket_upgrade AS odpu', 'odpu.odpu_detcus_id = detcus.detcus_id')
        ->join('JOIN', 'paket_foto as pf', 'pf.paket_id = odpf.odpf_paket_id')
        ->join('JOIN', 'paket_upgrade as pu', 'pu.upg_id = odpu.odpu_upg_id')
        ->join('JOIN', 'transaksi as trans', 'trans.trans_detcus_id = detcus.detcus_id');
        //->where(['detcus.detcus_id' => $detcus_id]);

?>

<?php

$this->registerJsFile(
    Yii::$app->request->baseUrl .'/js/detail-customer/juispk.js');
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\editable\Editable;
use common\models\Transaksi;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DetailCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report Pembayaran Paket Foto Studio';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-customer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    yii\bootstrap\Modal::begin(['id' =>'modal']);
    yii\bootstrap\Modal::end();
    ?>

   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'detcus_tanggal',
                'headerOptions' => ['class' => 'text-center']
            ],
            [
                'label' => 'Nama Customer',
                'headerOptions' => ['class' => 'text-center'],
                'value' => 'customer.cust_nama',

            ],
            [
                'label' => 'No. Invoice',
                'headerOptions' => ['class' => 'text-center'],
                'value' => 'customer.cust_no_invoice',
            ],
            [
                'label' => 'Nama Paket',
                'headerOptions' => ['class' => 'text-center'],
                'value' => 'orderDetailPaketFoto.paketFoto.paket_nama',
            ],
            [
                'label' => 'Qty',
                'headerOptions' => ['class' => 'text-center'],
                'value' => 'orderDetailPaketFoto.odpf_qty',
            ],
            [
                'attribute' => 'detcus_pay',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'currency',
                //'contentOptions'=>['style'=>'max-width: 100px;vertical-align:middle'],
            ],
            [
                'label' => 'Total Pembayaran',
                'attribute' => 'detcus_pay',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'currency',
            ],
            [
                'class' => 'kartik\grid\EditableColumn', 
                'header' => 'Status Pembayaran',
                'headerOptions' => ['class' => 'text-center'],
                'attribute' => 'detcus_status',
                'filter' => ["Down Payment" => "DP", "Lunas" => "Lunas"],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'headerOptions' => ['class' => 'text-center'],
                'template' => '{leadView} {leadSPK} {leadNota}',
                'buttons' => [
                   'leadNota' => function ($url, $model) {
                    return  Html::a(Yii::t('app', '<i class="fa fa-folder-open"></i>'), 
                    ['detail-customer/preview', 'detcus_id' => $model->detcus_id], 
                    ['class' => 'btn btn-primary pull-center', 'style'=>'margin-center: 5px;', 'id' => 'popupModal', 'title' => 'Nota']);      
                    },
                   'leadSPK' => function ($url, $model) {
                    $url = Url::to(['detail-customer/cetakspk', 'detcus_id' => $model->detcus_id]);
                   return Html::a('<button class="btn btn-primary pull-center" style="margin-center: 5px;"><i class="fa fa-cloud-download"></i> </button>', $url, ['title' => 'SPK']);
                    },
                    'leadView' => function ($url, $model) {
                        $url = Url::to(['detail-customer/view', 'detcus_id' => $model->detcus_id]);
                    return Html::a('<button class="btn btn-primary pull-center" style="margin-center: 5px;"><i class="fa fa-eye-slash"></i> </button>', $url, ['title' => 'Lihat']);
                    },
                   
                ]
            ]
        ],
    ]); ?>

</div>

<?php

$this->registerJs("$(function() {
    $('#popupModal').click(function(e) {
      e.preventDefault();
      $('#modal').modal('show').find('.modal-content')
      .load($(this).attr('href'));
    });
 });");
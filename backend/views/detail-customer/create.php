<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DetailCustomer */

$this->title = '(C1)Form Paket Foto';
$this->params['breadcrumbs'][] = ['label' => 'Detail Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-customer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsOrderDetailPaketFoto' => $modelsOrderDetailPaketFoto,
        'modelsOrderDetailPaketUpgrade' => $modelsOrderDetailPaketUpgrade,
    ]) ?>

</div>
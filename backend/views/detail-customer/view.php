<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */

$this->title = "Preview ".$model->customer->cust_nama;
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="customer-view">
    <h3><?= Html::encode($this->title) ?></h3>
</div>

<div class="table-responsive">
    <h1 align="center">BABY MILANO</h1>
    <h2 align="center">Jl. Sekip No. 18/32, Medan</h2>
    <hr style="border-top: dotted 1px;" />

    
    <table class="table table-bordered table-hover mx-auto w-auto">
     <tr>
     	<td><h4>No. Invoice</h4></td>
        <td><?php echo $rowView['cust_no_invoice']; ?> </td>
        <td><h4>Tanggal</h4></td>
        <td><?php echo $rowView['detcus_tanggal'];?></td>
     </tr>
     <tr>
     	<td><h4>Nama Customer</h4></td>
        <td colspan="3"><?php echo $rowView['cust_nama'];?></td>
     </tr>
     <tr>
     	<td colspan="4"></td>
     </tr>
     <tr>
     	<td align="center"><h4>Paket Foto</h4></td>
        <td align="center" colspan="2"><h4>Qty</h4></td>
        <td align="center"><h4>Harga</h4></td>
     </tr>
     <tr>
     	  <td align="center"><?php echo $rowView['paket_nama'];?></td>
        <td colspan="2" align="center"><?php echo $rowView['odpf_qty'];?></td>
        <td align="right">Rp <?php echo $rowView['odpf_qty'] * $rowView['paket_harga']?></td>
     </tr>
     <tr>
        <td colspan="3" align="right"><h4>Down Payment / Payment</h4></td>
        <td align="right"><h4>(Rp <?php echo $rowView['detcus_pay'];?>)</h4></td>
     </tr>
     <tr>
        <td colspan="3" align="right"><h4>Sisa Pelunasan</h4></td>
        <td align="right"><?php echo ($rowView['odpf_qty'] * $rowView['paket_harga']) - $rowView['detcus_pay'] ;?></td>
     </tr>
     <tr>
     	<td colspan="4" align="right"></td>
     </tr>
     <tr>
     	<td colspan="4" align="left"><h4>&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Paket Upgrade</h4></td>
     </tr>
     <tr>
        <td align="center"><?php // echo $rowView['upg_nama'];?></td>
        <td colspan="2" align="center"></td>
        <td align="right"><?php // echo $rowView['odpu_qty'] * $rowView['upg_harga'];?></td>
     </tr>
     <tr>
        <td colspan="3" align="right"><h4>Total Upgrade</h4></td>
        <td align="right"><?php // echo $rowView['odpu_qty'] * $rowView['upg_harga'];?></td>
     </tr>
     <tr>
        <td colspan="3" align="right"><h4>Grand Total</h4></td>
        <td align="right"><h4><?php echo ($rowView['odpf_qty'] * $rowView['paket_harga']);?></h4></td>
     </tr>
    </table>
    <p align="center">HARAP LAKUKAN PEMBAYARAN DI KASIR UNTUK <br /> MENDAPATKAN NOMOR ANTRIAN FOTO STUDIO</p>
    <hr style="border-top: dotted 1px;" />

</div>

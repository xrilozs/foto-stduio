<?php
use kartik\form\ActiveForm;
use kartik\builder\TabularForm;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use kartik\grid\GridView;


$dataProvider = new ArrayDataProvider([
    'allModels'=>[
        ['id'=>1, 'name'=>'Book Number 1', 'publish_date'=>'25-Dec-2014'],
        ['id'=>2, 'name'=>'Book Number 2', 'publish_date'=>'02-Jan-2014'],
        ['id'=>3, 'name'=>'Book Number 3', 'publish_date'=>'11-May-2014'],
        ['id'=>4, 'name'=>'Book Number 4', 'publish_date'=>'16-Apr-2014'],
        ['id'=>5, 'name'=>'Book Number 5', 'publish_date'=>'16-Apr-2014']
    ]
]);
echo Html::beginForm();
echo TabularForm::widget([
    // your data provider
    'dataProvider'=>$dataProvider,
 
    // formName is mandatory for non active forms
    // you can get all attributes in your controller 
    // using $_POST['kvTabForm']
    'formName'=>'kvTabForm',
    
    // set defaults for rendering your attributes
    'attributeDefaults'=>[
        'type'=>TabularForm::INPUT_TEXT,
    ],
    
    // configure attributes to display
    'attributes'=>[
        'id'=>['label'=>'book_id', 'type'=>TabularForm::INPUT_HIDDEN_STATIC, 'columnOptions' => ['vAlign' => GridView::ALIGN_MIDDLE]],
        'name'=>['label'=>'Book Name'],
        'publish_date'=>['label'=>'Published On', 'type'=>TabularForm::INPUT_STATIC]
    ],
    
    // configure other gridview settings
    'gridSettings'=>[
        'condensed' => true,
        'panel'=>[
            'heading'=>'<i class="fas fa-book"></i> Manage Books',
            'before' => false,
            'type'=>GridView::TYPE_PRIMARY,
            'before'=>false,
            'footer'=>false,
            'after'=>Html::button('<i class="fas fa-plus"></i> Add New', ['type'=>'button', 'class'=>'btn btn-success kv-batch-create']) . ' ' . 
                    Html::button('<i class="fas fa-times"></i> Delete', ['type'=>'button', 'class'=>'btn btn-danger kv-batch-delete']) . ' ' .
                    Html::button('<i class="fas fa-save"></i> Save', ['type'=>'button', 'class'=>'btn btn-primary kv-batch-save'])
        ]
    ]
]);
echo Html::endForm();

?>
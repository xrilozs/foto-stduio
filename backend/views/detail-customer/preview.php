<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

yii\web\YiiAsset::register($this);
?>
<div class="detail-customer-view">
    <h2 align="center">BABY MILANO</h2>
    <h4 align="center">Jl. Sekip No. 18/32, Medan</h4>
    <hr style="border-top: dotted 1px;" />

    
    <table class="table table-bordered">
     <tr>
     	<td>No. Invoice</td>
        <td><?php echo $rowView['cust_no_invoice'];?></td>
        <td>Tanggal</td>
        <td>22 Maret 2022</td>
     </tr>
     <tr>
     	<td>Client</td>
        <td>Lamvit Theresia</td>
        <td>CS</td>
        <td>Anita</td>
     </tr>
     <tr>
     	<td colspan="4"></td>
     </tr>
     <tr>
     	<td align="center">Paket Foto</td>
        <td align="center" colspan="2">Qty</td>
        <td>Harga</td>
     </tr>
     <tr>
     	<td>Baby 1 Tema</td>
        <td colspan="2" align="center">1</td>
        <td>299.000</td>
     </tr>
     <tr>
        <td colspan="3" align="right">Down Payment</td>
        <td>(100.000)</td>
     </tr>
     <tr>
        <td colspan="3" align="right">Sisa Pelunasan</td>
        <td>199.000</td>
     </tr>
     <tr>
     	<td colspan="4" align="right"></td>
     </tr>
     <tr>
     	<td colspan="4" align="left">Paket Upgrade</td>
     </tr>
     <tr>
     	<td>Tambah Pose Foto</td>
        <td colspan="2" align="center">1</td>
        <td>150.000</td>
     </tr>
     <tr>
     	<td>Up Paket Baby 1 Tema ke 2 Tema</td>
        <td colspan="2">1</td>
        <td>50.000</td>
     </tr>
     <tr>
        <td colspan="3" align="right">Total Upgrade</td>
        <td>200.000</td>
     </tr>
     <tr>
        <td colspan="3" align="right">Grand Total</td>
        <td>399.000</td>
     </tr>
    </table>
    <p align="center">HARAP LAKUKAN PEMBAYARAN DI KASIR UNTUK <br /> MENDAPATKAN NOMOR ANTRIAN FOTO STUDIO</p>
    <hr style="border-top: dotted 1px;" />

</div>

<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use kartik\depdrop\Depdrop;
use common\models\PaketFoto;
use common\models\PaketUpgrade;
use yii\web\View;
use yii\web\JqueryAsset;

?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Input Jadwal Pemesanan</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="customer-form" data-parsley-validate class="form-horizontal form-label-left">
                <?php $form = ActiveForm::begin([
                    'id' => 'dynamic-form', 
                    'type' => ActiveForm::TYPE_HORIZONTAL,
                    'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
                   
                ]); ?>
                
                <div class="form-group col-sm-6">
                    <?= $form->field($model, 'detcus_cust_id')->textInput() ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= $form->field($model, 'detcus_tanggal')->widget(DatePicker::classname(), [
                        'name' => 'detcus_tanggal',
                        'type' => DatePicker::TYPE_INPUT,
                        'value' => 'pes_tgl_foto',
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                        ]
                    ]); ?>

                </div>
                <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-items', // required: css class selector
                        'widgetItem' => '.item', // required: css class
                        'limit' => 4, // the maximum times, an element can be cloned (default 999)
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-item', // css class
                        'deleteButton' => '.remove-item', // css class
                        'model' => $modelsOrderDetailPaketUpgrade[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'odpu_upg_id',
                            'odpu_qty',
                        ],
                    ]); 
                ?>
                <table class = "table">
                    <thead>
                        <tr>
                            <th class="text-center">Paket Upgrade</th>
                            <th class="text-center">Qty</th>
                            <th class="text-center">Harga</th>
                            <th class="text-center"><button></button></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($modelsOrderDetailPaketUpgrade  as $i => $modelOrderDetailPaketUpgrade): ?>
                        <tr>
                            <td class="text-center">
                                <?php
                                    // necessary for update action.
                                    if (! $modelOrderDetailPaketUpgrade->isNewRecord) {
                                        echo Html::activeHiddenInput($modelOrderDetailPaketUpgrade, "[{$i}]odpu_id");
                                    }
                                ?>
                                <?= $form->field($modelOrderDetailPaketUpgrade, "[{$i}]odpu_upg_id")->widget(Select2::classname(), [
                                    'options' => [
                                        'class' => 'odpu_upg_id',
                                        'placeholder' => 'Select a state ...'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'data' => ArrayHelper::toArray(PaketUpgrade::find()->all(), [
                                            PaketUpgrade::class => [
                                                'id' => function (PaketUpgrade $paketupgrade) {
                                                    return $paketupgrade->upg_id;
                                                },
                                                'text' => function (PaketUpgrade $paketupgrade) {
                                                    return $paketupgrade->upg_nama;
                                                },
                                                'upg_nama',
                                                'upg_harga',
                                            ],
                                            ]), 
                                        ],
                                    ]); 
                                ?> 
                            </td>
                            <td class="text-center">
                                <?= $form->field($modelOrderDetailPaketUpgrade, "[{$i}]odpu_qty")->textInput(['maxlength' => true]) ?>
                            </td>
                            <td class="text-center">
                                <?= $form->field($modelOrderDetailPaketUpgrade, "[{$i}]odpu_harga")->textInput(['disabled' => true, 'maxlength' => true, 'type' => 'number']) ?>
                            </td>
                            <td class="text-center"><button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>/td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colspan="2" class="text-center"> Sub Total</td>
                            <td colspan="2"><input type="number" id="subtotal" class="form-control" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center"> Down Payment</td>
                            <td colspan="2">
                                <?= $form->field($modelTrans, 'trans_pay')->label(false)->textInput(['class'=>'form-control']) ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center">Sisa Pelunasan</td>
                            <td colspan="2"><input type="number" class="form-control" readonly="readonly"></td>
                        </tr>
                    </tbody>
                </table>
                <?php DynamicFormWidget::end(); ?>        

                <div class="panel-heading panel-default"><h4><i class="glyphicon glyphicon-envelope"></i> Paket Foto</h4></div>
                <div class="panel-body">
                     <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-items', // required: css class selector
                        'widgetItem' => '.item', // required: css class
                        'limit' => 4, // the maximum times, an element can be cloned (default 999)
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-item', // css class
                        'deleteButton' => '.remove-item', // css class
                        'model' => $modelsOrderDetailPaketFoto[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'odpf_paket_id',
                            'odpf_qty',
                        ],
                    ]); ?>

                    <div class="container-items"><!-- widgetContainer -->
                    <?php foreach ($modelsOrderDetailPaketFoto  as $i => $modelOrderDetailPaketFoto): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Paket Foto</h3>
                                <div class="pull-right">
                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                    // necessary for update action.
                                    if (! $modelOrderDetailPaketFoto->isNewRecord) {
                                        echo Html::activeHiddenInput($modelOrderDetailPaketFoto, "[{$i}]odpf_id");
                                    }
                                ?>
                                <div class="col-sm-4">
                                    <?= $form->field($modelOrderDetailPaketFoto, "[{$i}]odpf_paket_id")->widget(Select2::classname(), [
                                    'options' => [
                                        'class' => 'odpf_paket_id',
                                        'placeholder' => 'Pilih Nama Paket'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'data' => ArrayHelper::toArray(PaketFoto::find()->all(), [
                                            PaketFoto::class => [
                                                'id' => function (PaketFoto $paketfoto) {
                                                    return $paketfoto->paket_id;
                                                },
                                                'text' => function (PaketFoto $paketfoto) {
                                                    return $paketfoto->paket_nama;
                                                },
                                                'paket_nama',
                                                'paket_harga',
                                            ],
                                            ]), 
                                        ],
                                    ]); 
                                    ?>                                     
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($modelOrderDetailPaketFoto, "[{$i}]odpf_qty")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($modelOrderDetailPaketFoto, "[{$i}]odpf_harga")->textInput(['disabled' => true, 'maxlength' => true, 'type' => 'number']) ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                </div>

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 text-right">Sub Total</label>
                    <div class="col-md-9 col-sm-9">
                        <input type="subtotal" class="form-control" readonly="readonly">
                    </div>
                </div>

                <div id="status_pembayaran" class="form-group col-sm-12">
                    <?= $form->field($modelTrans, 'trans_status')->dropDownList(['Lunas' => 'Lunas', 'DP' => 'Down Payment'],['prompt' =>'Pilih Status Pembayaran']) ?>
                </div>

                <div class="form-group col-sm-12">
                    <?= $form->field($modelTrans, 'trans_pay')->textInput() ?>
                </div>

                <div id="sisa_pelunasan" class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 text-right">Sisa Pembayaran</label>
                    <div class="col-md-9 col-sm-9">
                        <input type="total" class="form-control" readonly="readonly">
                    </div>
                </div>

                <div id="grand_total" class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 text-right">Grand Total</label>
                    <div class="col-md-9 col-sm-9">
                        <input type="total" class="form-control" readonly="readonly">
                    </div>
                </div>
                
                <?= $form->field($model, 'detcus_ket')->textArea(['maxlength' => true]) ?>

                <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Paket Upgrade</h4></div>
                <div class="panel-body">
                     <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                        'widgetBody' => '.container-items', // required: css class selector
                        'widgetItem' => '.item', // required: css class
                        'limit' => 4, // the maximum times, an element can be cloned (default 999)
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-item', // css class
                        'deleteButton' => '.remove-item', // css class
                        'model' => $modelsOrderDetailPaketUpgrade[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'odpu_upg_id',
                            'odpu_qty',
                        ],
                    ]); ?>

                    <div class="container-items"><!-- widgetContainer -->
                    <?php foreach ($modelsOrderDetailPaketUpgrade  as $i => $modelOrderDetailPaketUpgrade): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Paket Upgrade</h3>
                                <div class="pull-right">
                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                    // necessary for update action.
                                    if (! $modelOrderDetailPaketUpgrade->isNewRecord) {
                                        echo Html::activeHiddenInput($modelOrderDetailPaketUpgrade, "[{$i}]odpu_id");
                                    }
                                ?>
                                <div class="col-sm-4">
                                    <?= $form->field($modelOrderDetailPaketUpgrade, "[{$i}]odpu_upg_id")->widget(Select2::classname(), [
                                    'options' => [
                                        'class' => 'odpu_upg_id',
                                        'placeholder' => 'Select a state ...'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'data' => ArrayHelper::toArray(PaketUpgrade::find()->all(), [
                                            PaketUpgrade::class => [
                                                'id' => function (PaketUpgrade $paketupgrade) {
                                                    return $paketupgrade->upg_id;
                                                },
                                                'text' => function (PaketUpgrade $paketupgrade) {
                                                    return $paketupgrade->upg_nama;
                                                },
                                                'upg_nama',
                                                'upg_harga',
                                            ],
                                            ]), 
                                        ],
                                    ]); 
                                    ?>                                     
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($modelOrderDetailPaketUpgrade, "[{$i}]odpu_qty")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($modelOrderDetailPaketUpgrade, "[{$i}]odpu_harga")->textInput(['disabled' => true, 'maxlength' => true, 'type' => 'number']) ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('
        function initSelect2DropStyle(a,b,c){
            initS2Loading(a,b,c);
        }
        function initSelect2Loading(a,b){
            initS2Loading(a,b);
        }
    ',
    yii\web\View::POS_HEAD
);

$this->registerJsFile(
    Yii::$app->request->baseUrl .'/js/detail-customer/_form.js',
    ['depends' => [JqueryAsset::class], View::POS_READY]
);


$script = <<<JS
 $(function () {
    $(document).ready(function() {
        $("#status_pembayaran").change(function () {
            if ( $(event.target).val() === 'DP')
            {
                $("#sisa_pelunasan").show();
                $("#grand_total").hide();
            }
            else{
                $("#grand_total").show();
                $("#sisa_pelunasan").hide();
            }    
        });
    });
}); 
JS;
$this->registerJs($script);

$scripthitung = <<<EOD

    var bayar = function(){
        var qty = $(.odpf_qty).val();
        var harga = $(.odpf_harga).val();
        var subtotal = 0;

        subtotal = parseInt(qty) * parseInt(harga);

        $(".subtotal").val(total);

    };
    $(".qty").on("change", function() {
        subtotal();
        })

EOD;
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DetailCustomerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detail-customer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'detcus_id') ?>

    <?= $form->field($model, 'detcus_cust_id') ?>

    <?= $form->field($model, 'detcus_paket_id') ?>

    <?= $form->field($model, 'detcus_frame_id') ?>

    <?= $form->field($model, 'detcus_album_id') ?>

    <?php // echo $form->field($model, 'detcus_foto_id') ?>

    <?php // echo $form->field($model, 'detcus_harga') ?>

    <?php // echo $form->field($model, 'detcus_ket') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>

</head>
<body>
<h3 style="text-align:center">BABY MILANO</h1>
<p align="center" class="keterangan">Jl. Sekip No. 18/32, Medan</p>
<hr style="border-top: dotted 1px;" />
<table style="width:100%" cellpadding="5" cellspacing="5" class="table table-bordered">
	<tr>
    	<td>No. Invoice</td>
        <td><?php echo $rowTema['cust_no_invoice'];?></td>
        <td>Tanggal</td> 
        <td><?php echo $rowTema['detcus_tanggal'];?></td>
    </tr>
    <tr>
    	<td>Client</td>
        <td colspan="3"><?php echo $rowTema['cust_nama']; ?></td>
    </tr>
    <tr>
    	<td>Paket Foto</td>
        <td colspan="3"><?php echo $rowTema['paket_nama'];?></td>
    </tr>
    <tr>
        <td colspan="2" align="center">Jumlah Tema</td>
        <td colspan="2" align="center">Jumlah Pose</td>
    </tr>
    <tr>
    	<td colspan="2" align="center"><?php echo $rowTema['paket_jumlah_tema'];?></td>
        <td colspan="2" align="center"><?php echo $rowTema['paket_pose'];?></td>
    </tr>
    <tr>
    	<td colspan="4">Pilihan Tema</td>
    </tr>
    <tr>
    	<td align="center"><img src="<?=Yii::$app->request->baseUrl?>" width="100" height="80"><br/>Baby Story 1</td>
        <td align="center"><img src="<?=Yii::$app->request->baseUrl?>/tema_gambarStory 2226.jpg" width="100" height="80"><br>Baby Story 2</td>
        <td align="center"><img src="<?=Yii::$app->request->baseUrl?>/tema_gambarStory32684.jpg" width="100" height="80"><br>Baby Story 3</td>
        <td align="center"><img src="<?=Yii::$app->request->baseUrl?>/paket_fotoBaby Story3399.jpg" width="100" height="80"><br>Baby Story 4</td>
    </tr>
    <tr>
    	<td colspan="4">Keterangan untuk Tim Foto : <br/>
        <?php echo $rowTema['odtema_ket'];?></td>
    </tr>
    <tr>
    	<td align="center">Ruang Studio</td>
        <td align="center">Fotografer</td>
        <td align="center">Asisten</td>
        <td align="center">CS</td>
    </tr>
    <tr>
    	<td align="center"><?php echo $rowTema['serv_studio']?></td>
        <td align="center">
            <?php 
            // if (is_null($rowTema['serv_fotografer_id'])) 
            // { 
            //     $session_data = '';
            // } 
            // else 
            // { 
            //     $session_data = $rowTema['kary_nama'];  
            // } 
            //     return $rowTema['kary_nama'];
            echo $rowTema['kary_nama']
            ?>
        </td>
        <td align="center"><?php echo $rowTema['kary_nama'];?></td>
        <td align="center"><?php echo $rowTema['kary_nama'];?></td>
    </tr>
</table>
<p class="keterangan">HARAP MENUNGGU GILIRAN ANDA,<br/> TUNJUKKAN SPK FOTO KEPADA FOTOGRAFER DI STUDIO</p>
<hr style="border-top: dotted 1px;" />
</body>
</html> 
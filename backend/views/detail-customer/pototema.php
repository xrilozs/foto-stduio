<?php

use yii\helpers\Html;
?>
<style>
    ul {
  list-style-type: none;
}

li {
  display: inline-block;
}

input[type="checkbox"][id^="cb"] {
  display: none;
}

label {
  border: 1px solid #fff;
  padding: 10px;
  display: block;
  position: relative;
  margin: 10px;
  cursor: pointer;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

label::before {
  background-color: white;
  color: white;
  content: " ";
  display: block;
  border-radius: 50%;
  border: 1px solid grey;
  position: absolute;
  top: -5px;
  left: -5px;
  width: 25px;
  height: 25px;
  text-align: center;
  line-height: 28px;
  transition-duration: 0.4s;
  transform: scale(0);
}

label img {
  height: 100px;
  width: 100px;
  transition-duration: 0.2s;
  transform-origin: 50% 50%;
}

:checked+label {
  border-color: #ddd;
}

:checked+label::before {
  content: "✓";
  background-color: grey;
  transform: scale(1);
}

:checked+label img {
  transform: scale(0.9);
  box-shadow: 0 0 5px #333;
  z-index: -1;
}
</style>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>Media Gallery <small> Tema Foto</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
                </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul> 
        <div class="clearfix"></div>
        </div>
        <div class="x_content"></div>

        <div class="row">
            <?php foreach($medias as $media) { ?>
            <div class="col-md-55">
                <div class="thumnail">
                    <div class="image view view-first">
                        <ul>
                            <li>
                            <input type="checkbox" id="cb">
                            <label for="cb"><img src="<?php echo Yii::getAlias('@web').'/'.$media->tem_gambar;?>" alt="image"/></label>
                            </li>
                        </ul>
                    </div>
                    <div class="caption" align = "center";>
                        <?php echo $media->tem_nama;?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col text-center">
          <button type="button" class="btn btn-primary btn-lg">Simpan</button>
          <button type="button" class="btn btn-secondary btn-lg">Reset</button>
        </div>
        
    </div>
    </div>
</div>

<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\select2\Select2;
use common\models\Album;
use common\models\JenisFrame;
use common\models\PaketKategori;
use common\models\CetakFoto;
use yii\web\View;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
use wbraganca\dynamicform\DynamicFormWidget;


/* @var $this yii\web\View */
/* @var $model common\models\PaketFoto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Input Paket Foto</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="customer-form">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype'=>'multipart/form-data'],
                    'id' => 'dynamic-form', 
                   
                ]); ?>

                <div class="row">
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_kategori_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(PaketKategori::find()->all(), 'kategori_id', 'kategori_nama'),
                            'language' => 'en',
                            'options' => [
                                'disabled' => false,
                                'placeholder' => 'Pilih Kategori Paket',
                                'id' => 'paket_kategori_id'
                            ],
                            'pluginOptions' => ['allowClear' => true, ]
                            ])
                        ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_nama')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
			    
                <div class="row">
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_album_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Album::find()->all(), 'album_id', 'album_warna', 'album_nama'),
                            'language' => 'en',
                            'options' => [
                                'disabled' => false,
                                'placeholder' => 'Pilih Jenis Album',
                                'id' => 'paket_album_id'
                            ],
                            'pluginOptions' => ['allowClear' => true, ]
                            ])
                        ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_album_qty')->textInput(['placeholder' => 'Masukkan Qty Album']) ?>
                    </div>
                </div>

                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    'limit' => 4, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelsPaketFrameDetail[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'fd_paket_id',
                        'fd_frame_id',
                        'fd_qty'
                    ],
                ]); ?>
                <table class="table">
                    <thead class="thead-dark">
                        <tr class="text-center">
                            <th class="text-center">Nama Frame</th>
                            <th class="text-center">Qty</th>
                            <th class="text-center">
                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="container-items">
                        <?php foreach ($modelsPaketFrameDetail as $i => $modelPaketFrameDetail) : ?>
                        <tr class="item">
                            <td class="text-center">
                            <?php 
                                if (! $modelPaketFrameDetail->isNewRecord) {
                                    echo Html::activeHiddenInput($modelPaketFrameDetail, "[{$i}fd_id]");
                                }
                            ?>
                            <?= $form->field($modelPaketFrameDetail, "[{$i}]fd_frame_id")->label(false)->widget(Select2::classname(), [
                                    'options' => [
                                        'class' => 'fd_frame_id',
                                        'placeholder' => 'Pilih Jenis Frame'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'data' => ArrayHelper::toArray(JenisFrame::find()->all(), [
                                            JenisFrame::class => [
                                                'id' => function (JenisFrame $jenisFrame) {
                                                    return $jenisFrame->frame_id;
                                                },
                                                'text' => function (JenisFrame $jenisFrame) {
                                                    return $jenisFrame->frame_nama_produk;
                                                },
                                            ],
                                            ]), 
                                        ],
                                    ]); 
                                ?>
                            </td>
                            <td>
                            <?= $form->field($modelPaketFrameDetail, "[{$i}]fd_qty", ['addClass' => 'form-control fd_qty'])->label(false)->textInput(['maxlength' => true, 'type' => 'number', 'autocomplete' => 'off']) ?>
                            </td>
                            <td class="text-center">
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php DynamicFormWidget::end(); ?>

                <div class="row">
                    <div class="form-group col-md-6">
                    <?= $form->field($model, 'paket_cetak_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(CetakFoto::find()->all(), 'cetak_id', 'cetak_nama'),
                            'language' => 'en',
                            'options' => [
                                'disabled' => false,
                                'placeholder' => 'Pilih Ukuran Cetak Foto',
                                'id' => 'paket_cetak_id'
                            ],
                            'pluginOptions' => ['allowClear' => true, ]
                            ])
                        ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_cetak_qty')->textInput(['placeholder' => 'Masukkan Qty Ukuran Cetak']) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_jumlah_foto_utama')->textInput(['placeholder' => 'Masukkan Jumlah Foto Utama']) ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_jumlah_cetak')->textInput(['placeholder' => 'Masukkan Jumlah Cetak Foto Biasa']) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_jumlah_tema')->textInput(['placeholder' => 'Masukkan Jumlah Tema']) ?>
                    </div>
                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_pose')->textInput(['placeholder' => 'Masukkan Jumlah Pose']) ?>
                    </div>
                </div>

			    <?= $form->field($model, 'paket_harga')->textInput(['placeholder' => 'Masukkan Harga Paket']) ?>

			    <?= $form->field($model, 'paket_ket')->textArea(['maxlength' => true]) ?>

			    <div class="form-group">
			        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
			    </div>

			    <?php ActiveForm::end(); ?>

			</div>
        </div>
    </div>
</div>

<?php
$this->registerJs('
        function initSelect2DropStyle(a,b,c){
            initS2Loading(a,b,c);
        }
        function initSelect2Loading(a,b){
            initS2Loading(a,b);
        }
    ',
    yii\web\View::POS_HEAD
);

$this->registerJsFile(
    Yii::$app->request->baseUrl .'/js/paket-foto/_form.js',
    ['depends' => [JqueryAsset::class], View::POS_READY]
);

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaketFoto */

$this->title = 'Update Paket Foto: ' . $model->paket_id;
$this->params['breadcrumbs'][] = ['label' => 'Paket Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->paket_id, 'url' => ['view', 'paket_id' => $model->paket_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paket-foto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

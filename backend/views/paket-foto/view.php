<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaketFoto */

$this->title = "Paket ".$model->paket_nama;
$this->params['breadcrumbs'][] = ['label' => 'Paket Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="paket-foto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Ubah', ['update', 'paket_id' => $model->paket_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Hapus', ['delete', 'paket_id' => $model->paket_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [                                                  // the owner name of the model
                'label' => 'Nama Kategori Paket',
                'value' => $model->paketKategori->kategori_nama,
            ],
            'paket_nama',
            // [
            //     'attribute'=>'paket_gambar',
            //     'value'=>$model->paket_gambar,
            //     'format' => ['image',['width'=>'280px','height'=>'200px']],                 
            // ],
            'paket_jumlah_tema',
            'paket_pose',
            'paket_jumlah_foto_utama',
            'paket_jumlah_cetak',
            [                                                  // the owner name of the model
                'label' => 'Nama Album',
                'value' => $model->album->album_nama,            
                // 'contentOptions' => ['class' => 'bg-dark'],     // HTML attributes to customize value tag
                // 'captionOptions' => ['tooltip' => 'Tooltip'],  // HTML attributes to customize label tag
            ],
            'paket_album_qty',
            // [                                                  // the owner name of the model
            //     'label' => 'Size Cetak',
            //     'value' => $model->cetakFoto->cetak_nama,
            // ],
            'paket_cetak_qty',
            'paket_jumlah_foto_utama',
            'paket_jumlah_cetak',
            'paket_harga',
            'paket_ket',
        ],
    ]) ?>

</div>

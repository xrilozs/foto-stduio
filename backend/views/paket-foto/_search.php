<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaketFotoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paket-foto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'paket_id') ?>

    <?= $form->field($model, 'paket_nama') ?>

    <?= $form->field($model, 'paket_gambar') ?>

    <?= $form->field($model, 'paket_harga') ?>

    <?= $form->field($model, 'paket_ket') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

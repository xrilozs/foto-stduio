<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaketFoto */

$this->title = 'Form Paket Foto Baru';
$this->params['breadcrumbs'][] = ['label' => 'Paket Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-foto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsPaketFrameDetail' => $modelsPaketFrameDetail,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\i18n\Formatter;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PaketFotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Paket Foto';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-foto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Paket Foto Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [ 
                'label' => 'Nama Kategori Paket',
                'attribute' => 'paket_kategori_id',
                'value' => 'paketKategori.kategori_nama',
            ],
            'paket_nama',
            [                                                  
                'label' => 'Nama Album',
                'attribute'=>'paket_album_id',
                'value' => 'album.album_nama',
            ],
            'paket_album_qty',
            [                                                 
                'label' => 'Nama Frame',
                'attribute' => 'fd_frame_id',
                'value' => 'paketFrameDetail.jenisFrame.frame_nama_produk',
            ],
            [                                                 
                'label' => 'Qty',
                'attribute' => 'fd_frame_id',
                'value' => 'paketFrameDetail.fd_qty',
            ],
            [                                                  
                'label' => 'Size Cetak',
                'attribute' => 'paket_cetak_id',
                'value' => 'cetakFoto.cetak_nama',
            ],
            'paket_cetak_qty',
            'paket_jumlah_tema',
            'paket_pose',
            'paket_jumlah_foto_utama',
            'paket_jumlah_cetak',
            'paket_jumlah_foto_utama',
            'paket_jumlah_cetak',
            [
                'attribute' => 'paket_harga',
                'format' => 'currency',
            ],
            'paket_ket',
        ],
    ]); ?>


</div>

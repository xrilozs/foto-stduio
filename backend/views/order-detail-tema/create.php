<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailTema */

$this->title = 'FORM SPK FOTOGRAFER';
$this->params['breadcrumbs'][] = ['label' => 'Order Detail Temas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-detail-tema-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'customers' => $customers,
        'model' => $model,
        'temaFotos' => $temaFotos,
    ]) ?>

</div>

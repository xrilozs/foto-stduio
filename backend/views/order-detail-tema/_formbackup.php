<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use common\models\Customer;
use common\models\Karyawan;
use common\models\TemaFoto;
use common\models\DetailCustomer;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\checkbox\CheckboxX;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailTema */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Input Paket Foto</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="customer-form">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'target' =>'_blank'
                    ],
                ]); ?>  

                <div class="form-group">
                    <?php
                     $customer = ArrayHelper::map(Customer::find()->all(), 'cust_id', function($model, $default){
                        return $model["cust_no_invoice"];
                    });
                 
                    ?>

                    <?= $form->field($model, 'odtema_detcus_id')->widget(Select2::classname(), [
                        //'data' =>  DetailCustomer::retrieveInvoiceLunas(),
                        'data' => $customer,
                        'language' => 'en',
                        'options' => [
                            'value' => (!$model->isNewRecord ? $selected : ''), 
                            'placeholder' => 'Input No Invoice',
                            'id' => 'cust_id'
                        ],
                        'pluginOptions' => ['allowClear' => true, ]
                        ])
                    ?>
                </div>
                
                <?= $form->field($model, 'namaCustomer')->textInput(['id' => 'namacustomer','readonly'=> true])?>

                <?= $form->field($model, 'tanggalDaftar')->textInput(['id' => 'tgldaftar', 'readonly'=> true, 'placeholder' => 'Tanggal Foto']) ?>

                <?= $form->field($model, 'namaPaketFoto')->textInput(['id' => 'namapaketfoto', 'readonly'=> true, 'placeholder' => 'Nama Paket' ]) ?>

                <div class="form-group col-md-6">
                    <?= $form->field($model, 'jumlahTema')->textInput(['id' => 'jumlahtema', 'readonly'=> true, 'placeholder' => 'Jumlah Tema' ]) ?>
                </div>

                <div class="form-group col-md-6">
                    <?= $form->field($model, 'jumlahPose')->textInput(['id' => 'jumlahpose', 'readonly'=> true, 'placeholder' => 'Jumlah Pose' ]) ?>
                </div>

                <?= $form->field($model, 'odtema_ket')->textArea()->label('Keterangan ') ?>
                
                <?= $form->field($modelDetailSpk, 'formCheckListIDs[]')->checkboxList(TemaFoto::mapIdToNama(Html::img('@web').'/'.$model->tem_gambar)) ;?>

                <div class="form-row">
                    <div class="form-group col-md-3">
                      <?= $form->field($modelDetailSpk, 'serv_studio')->textInput()?>
                    </div>

                    <div class="form-group col-md-3">
                    <?= $form->field($modelDetailSpk, 'serv_fotografer_id')->label('Fotografer')->widget(Select2::classname(), [
                        'data' => \common\models\Karyawan::map() ,
                        'options' => ['placeholder' => 'Pilih Nama Fotografer', 'id'=>'fotografer'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]);
                    ?>
                    </div>

                    <div class="form-group col-md-3">
                        <?= $form->field($modelDetailSpk, 'serv_asisten_id')->label('Asisten')->widget(Select2::classname(), [
                            'data' => \common\models\Karyawan::map(),
                            'options' => ['placeholder' => 'Pilih Nama Asisten', 'id'=>'asisten'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]);?>
                    </div>

                    <div class="form-group col-md-3"> 
                        <?= $form->field($modelDetailSpk, 'serv_cs_id')->label('CS')->widget(Select2::classname(), [
                            'data' =>  \common\models\Karyawan::map(),
                            'options' => ['placeholder' => 'Pilih Nama CS', 'id'=>'cs'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]);?>
                    </div>
                </div>

                <div class="col-md-6 col-md-offset-6">
                    <div class="form-group">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-lg']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<<JS
$('#cust_id').change(function(){
    var cust_id = $(this).val();
    $.get('index.php?r=customer/get-customer', { cust_id : cust_id }, function(data) {

    console.log(data);   

    $('#namacustomer').attr('value', data.customer.cust_nama);
    $('#tgldaftar').attr('value', data.detailCustomer.detcus_tanggal);
    $('#namapaketfoto').attr('value', data.paketFoto.paket_nama);
    $('#jumlahtema').attr('value', data.paketFoto.paket_jumlah_tema);
    $('#jumlahpose').attr('value', data.paketFoto.paket_pose);
    $('#namatema').attr('value', data.temaFoto.tem_nama);
    $('#gambartema').attr('value', data.temaFoto.tem_gambar);
    })
});

JS;
$this->registerJs($script);
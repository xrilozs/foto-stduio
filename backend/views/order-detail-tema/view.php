<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailTema */

$this->title = $model->odtema_detcus_id;
$this->params['breadcrumbs'][] = ['label' => 'Order Detail Temas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-detail-tema-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'odtema_id',
            'odtema_detcus_id',
            'odtema_tem_id',
            // 'serv_studio',
            // 'serv_photografer_id',
            // 'serv_asisten_id',
            // 'serv_cs_id'
        ],
    ]) 
    ?>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderDetailTemaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'FORM SPK FOTOGRAFER';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-detail-tema-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Order Detail Tema', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'odtema_id',
            'odtema_detcus_id',
            'odtema_tem_id',
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action, OrderDetailTema $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'odtema_id' => $model->odtema_id]);
            //      }
            // ],
        ],
    ]); ?>


</div>

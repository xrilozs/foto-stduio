<?php

use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrderDetailTema */

$this->title = 'CHOOSE FORM SPK FOTOGRAFER';
$this->params['breadcrumbs'][] = ['label' => 'Order Detail Temas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-detail-tema-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="clearfix"></div>
	<div class="row">
	    <div class="col-md-12 col-sm-12 ">
	        <div class="x_panel">
	            <div class="x_title">
	                
					<h2>Lakukan Pembayaran Terlebih Dahulu</h2>
	                <div class="clearfix"></div>
	            </div>
	            <div class="customer-form">
	                <?php $form = ActiveForm::begin(); ?>  

	                <div class="form-group">
	                    <?= $form->field($model, 'cust_id')->widget(Select2::classname(), [
	                        'data' => ArrayHelper::map($customers, 'cust_id', 'cust_no_invoice'),
	                        'language' => 'en',
	                        'options' => [
	                            'placeholder' => 'Choose no invoice',
	                            'id' => 'cust_id'
	                        ],
	                        'pluginOptions' => ['allowClear' => true, ]
	                        ])
	                    ?>
	                </div>
	            </div>

	            <div class="col-md-6 col-md-offset-6">
	                <div class="form-group">
	                    <?= Html::submitButton('Choose', ['class' => 'btn btn-success btn-lg']) ?>
	                </div>
	            </div>
	            <?php ActiveForm::end(); ?>
	        </div>
	    </div>
	</div>
	</div>


</div>

<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use common\models\Karyawan;
use common\models\TemaFoto;
use common\models\DetailCustomer;
use common\models\Customer;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\checkbox\CheckboxX;
use fedemotta\datatables\DataTables;
use yii\web\JqueryAsset;
use yii\web\View;

/** @var \yii\web\View $this */
/* @var $model common\models\OrderDetailTema */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Input Paket Foto</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="customer-form">
                <?php $form = ActiveForm::begin(); ?>  

                <div class="form-group">
                    <?= $form->field($model, 'cust_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($customers, 'cust_id', 'cust_no_invoice'),
                        'language' => 'en',
                        'options' => [
                            'disabled' => true,
                            'placeholder' => 'Choose no invoice',
                            'id' => 'cust_id'
                        ],
                        'pluginOptions' => ['allowClear' => true, ]
                        ])
                    ?>
                </div>
                
                <?= $form->field($model, 'cust_nama')->label('Nama Customer')->textInput(['readonly'=> true])?>

                <?= $form->field($model, 'detcus_tanggal')->label('Tanggal Daftar')->textInput(['readonly'=> true, date('d-M-Y')]) ?>

                <?= $form->field($model, 'paket_nama')->label('Nama Paket')->textInput(['readonly'=> true]) ?>

                <div class="row">

                <div class="form-group col-md-6">
                    <?= $form->field($model, 'jumlah_tema')->textInput(['readonly'=> true]) ?>
                    </div>

                    <div class="form-group col-md-6">
                        <?= $form->field($model, 'paket_pose')->textInput(['readonly'=> true]) ?>
                    </div>
                </div>

                <?= $form->field($model, 'ket')->textArea()->label('Keterangan ') ?>
                
                <?= $form->field($model, 'tem_id[]')->checkboxList(
                    ArrayHelper::map($temaFotos, 'tem_id', 'tem_gambar'),
                    [
                        'item' => function ($index, $label, $name, $checked, $value) {
                            return '
                                <div class="checkbox-inline tem_id_div">
                                    <label class="tem_id_label" for="tem_id_'.$index.'">
                                        <input class="tem_id_input_checkbox" id="tem_id_'.$index.'" name="'.$name.'" type="checkbox" value="'.$value.'" />
                                        <img class="tem_id_img" height="200" src="'.Yii::getAlias('@web').'/'.$label.'" width="200" />
                                    </label>
                                </div>
                            ';
                        },
                    ]
                ) ;?>

                <div class="row">
                    <div class="form-group col-md-3">
                      <?= $form->field($model, 'studio')->textInput()?>
                  </div>

                  <div class="form-group col-md-3">
                   
                    <?= $form->field($model, 'fotografer_id')->label('Fotografer')->widget(Select2::classname(), [
                        'data' => \common\models\Karyawan::map() ,
                        'options' => ['placeholder' => 'Pilih Nama Fotografer', 'id'=>'fotografer'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]);
                    ?>
                </div>

                <div class="form-group col-md-3">
                    <?= $form->field($model, 'asisten_id')->label('Asisten')->widget(Select2::classname(), [
                        'data' => \common\models\Karyawan::map(),
                        'options' => ['placeholder' => 'Pilih Nama Asisten', 'id'=>'asisten'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]);?>
                </div>

                <div class="form-group col-md-3"> 
                    <?= $form->field($model, 'cs_id')->label('CS')->widget(Select2::classname(), [
                        'data' =>  \common\models\Karyawan::map(),
                        'options' => ['placeholder' => 'Pilih Nama CS', 'id'=>'cs'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]);?>
                </div>
            </div>

            <div class="col-md-6 col-md-offset-6">
                <div class="form-group">
                    <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-lg']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
$this->registerCssFile("@web/css/order-detail-tema/_form.css");
$this->registerJsFile(
    Yii::$app->request->baseUrl .'/js/order-detail-tema/_form.js',
    ['depends' => [JqueryAsset::class], View::POS_READY]
);

<?php

use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
?>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>Media Gallery <small> Tema Foto</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
                </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul> 
        <div class="clearfix"></div>
        </div>
        <div class="x_content"></div>

        <div class="row">
            <?php foreach($medias as $media) { 
            ?>
            <div class="col-md-55">
                <div class="thumnail">
                    <div class="mb-2 has-success">
                      <label class="cbx-label" for="kv-adv-119">
                      <?= CheckboxX::widget([
                          'name' => 'kv-adv-119',
                          'initInputType' => CheckboxX::INPUT_CHECKBOX,
                          //'options'=>['id'=>'kv-adv-119'],
                          // 'pluginOptions' => [
                          //     'enclosedLabel' => true
                          // ]
                      ]);?> 
                      <div class="image view">
                        <img src="<?php echo Yii::getAlias('@web').'/'.$media->tem_gambar;?>" alt="image"/>
                      </div>
                      
                      <div class="caption" text-align="center">
                          <?php echo $media->tem_nama;?>>
                      </div>
                      </label>
                  </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col text-center">
          <button type="button" class="btn btn-primary btn-lg">Simpan</button>
          <button type="button" class="btn btn-secondary btn-lg">Reset</button>
        </div>
        
    </div>
    </div>
</div>

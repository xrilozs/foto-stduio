<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CetakFotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Produk Cetak Foto';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cetak-foto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambah Produk Cetak Foto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'cetak_nama',
            'cetak_size',
            [
                'attribute' => 'cetak_harga',
                'format' => 'currency',
            ],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CetakFoto */

$this->title = 'Update Cetak Foto: ' . $model->cetak_id;
$this->params['breadcrumbs'][] = ['label' => 'Cetak Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cetak_id, 'url' => ['view', 'cetak_id' => $model->cetak_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cetak-foto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

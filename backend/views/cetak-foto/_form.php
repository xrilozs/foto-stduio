<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CetakFoto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cetak-foto-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="form-group col-md-6">
            <?= $form->field($model, 'cetak_nama')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="form-group col-md-6">
            <?= $form->field($model, 'cetak_size')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <?= $form->field($model, 'cetak_harga') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CetakFoto */

$this->title = $model->cetak_nama;
$this->params['breadcrumbs'][] = ['label' => 'Cetak Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cetak-foto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cetak_id' => $model->cetak_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cetak_id' => $model->cetak_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'cetak_id',
            'cetak_nama',
            'cetak_size',
            'cetak_harga',
        ],
    ]) ?>

</div>

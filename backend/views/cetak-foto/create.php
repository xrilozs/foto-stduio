<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CetakFoto */

$this->title = 'Form Tambah Produk Cetak Foto';
$this->params['breadcrumbs'][] = ['label' => 'Cetak Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cetak-foto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

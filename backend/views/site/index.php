<?php

/** @var yii\web\View $this */

$this->title = 'MILANO ART STUDIO';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">WELCOME, </h1>

        <p class="lead">MILANO ART STUDIO</p>
    </div>

    <div class="body-content">
        <div class="row">
        <div class="col-md-12" style="display: inline-block;">
              <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 ">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                  <div class="count">20</div>
                  <h3>Pengunjung Hari Ini</h3>
                  <p>Lorem ipsum psdea itgum rixt.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 ">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-comments-o"></i></div>
                  <div class="count">6</div>
                  <h3>Pemesanan Hari Ini</h3>
                  <p>Lorem ipsum psdea itgum rixt.</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 ">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                  <div class="count">83</div>
                  <h3>Pengunjung Minggu Ini</h3>
                  <p>Lorem ipsum psdea itgum rixt.</p>
                </div>
              </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
        <div class="x_panel">
            <div class="x_title">
            <h2>Pengunjung Hari Ini <small>Sessions</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item One Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Two Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Two Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Two Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Three Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            </div>
        </div>
        </div>

        <div class="col-md-4">
        <div class="x_panel">
            <div class="x_title">
            <h2>Foto Diambil Minggu Ini<small>Sessions</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item One Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Two Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Two Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Two Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Three Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            </div>
        </div>
        </div>

        <div class="col-md-4">
        <div class="x_panel">
            <div class="x_title">
            <h2>Home Service Minggu Ini<small>Sessions</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                    </div>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item One Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Two Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Two Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Two Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            <article class="media event">
                <a class="pull-left date">
                <p class="month">April</p>
                <p class="day">23</p>
                </a>
                <div class="media-body">
                <a class="title" href="#">Item Three Title</a>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </article>
            </div>
        </div>
        </div>
    </div>
</div>

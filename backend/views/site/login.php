<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var \common\models\LoginForm $model */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use backend\assets\LoginAsset;

LoginAsset::register($this);
?>

<?php $this->beginPage();?>

<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>" >
<?php $this->beginBody(); ?>
<div class="container body">
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
    <div class="animate form login_form">
        <section class="login_content">
            <?php $form = ActiveForm::begin(['id' => 'login-form',]);?>
            
            <form>
            <h1><i class="fa fa-camera"></i> MILANO ART STUDIO <i class="fa fa-camera"></i></h1>
            <p>SISTEM INFORMASI MANAJEMEN OPERASIONAL</p>
                <h1>Login Form</h1>
                    <div>
                        <?= $form->field($model, 'username')->label(false)->textInput(['autofocus' => true, 'placeholder' => 'Username']) ?>
                    </div>
                    <div>
                        <?= $form->field($model, 'password')->label(false)->passwordInput(['placeholder' => 'Password']) ?>
                    </div>
                    <div>
                        <?= Html::submitButton('Login', ['class' => 'btn btn-default submit', 'name' => 'Log In']) ?>
                    </div>

                <div class="clearfix"></div>

                <div class="separator">
                <p class="change_link">New to site?
                    <a href="#signup" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                    <p>©2016. PT. MOMBEE PRIMA INDONESIA</p>
                </div>
                </div>
            </form>
            <?php ActiveForm::end(); ?>
        </section>
    </div>

    <div id="register" class="animate form registration_form">
        <section class="login_content">
        <form>
            <h1>Create Account</h1>
            <div>
            <input type="text" class="form-control" placeholder="Username" required="" />
            </div>
            <div>
            <input type="email" class="form-control" placeholder="Email" required="" />
            </div>
            <div>
            <input type="password" class="form-control" placeholder="Password" required="" />
            </div>
            <div>
            <a class="btn btn-default submit" href="index.html">Submit</a>
            </div>

            <div class="clearfix"></div>

            <div class="separator">
            <p class="change_link">Already a member ?
                <a href="#signin" class="to_register"> Log in </a>
            </p>

            <div class="clearfix"></div>
            <br />

            <div>
                <h1><i class="fa fa-paw"></i> MILANO ART STUDIO</h1>
                <p>©2016. PT. MOMBEE PRIMA INDONESIA</p>
            </div>
            </div>
        </form>
        </section>
    </div>
    </div>
</div>
</body>
</html>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaketFrameDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paket-frame-detail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fd_paket_id')->textInput() ?>

    <?= $form->field($model, 'fd_frame_id')->textInput() ?>

    <?= $form->field($model, 'fd_qty')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaketFrameDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paket-frame-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fd_id') ?>

    <?= $form->field($model, 'fd_paket_id') ?>

    <?= $form->field($model, 'fd_frame_id') ?>

    <?= $form->field($model, 'fd_qty') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaketFrameDetail */

$this->title = 'Update Paket Frame Detail: ' . $model->fd_id;
$this->params['breadcrumbs'][] = ['label' => 'Paket Frame Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fd_id, 'url' => ['view', 'fd_id' => $model->fd_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paket-frame-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

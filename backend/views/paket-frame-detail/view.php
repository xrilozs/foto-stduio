<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaketFrameDetail */

$this->title = $model->fd_id;
$this->params['breadcrumbs'][] = ['label' => 'Paket Frame Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="paket-frame-detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'fd_id' => $model->fd_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'fd_id' => $model->fd_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fd_id',
            'fd_paket_id',
            'fd_frame_id',
            'fd_qty',
        ],
    ]) ?>

</div>

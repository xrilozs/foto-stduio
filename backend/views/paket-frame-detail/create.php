<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaketFrameDetail */

$this->title = 'Create Paket Frame Detail';
$this->params['breadcrumbs'][] = ['label' => 'Paket Frame Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-frame-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

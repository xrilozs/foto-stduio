<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TemaFotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tema Foto';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tema-foto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tema Foto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= DataTables::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                
                'label' => 'Nama Paket',
                'value'=>'paketFoto.paket_nama',
            ],
            'tem_nama',
            [
                'attribute'=>'tem_gambar',
                'value'=>'tem_gambar',
                'format' => ['image',['width'=>'250','height'=>'200']],                 
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

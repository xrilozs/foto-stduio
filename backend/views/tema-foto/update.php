<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TemaFoto */

$this->title = 'Update Tema Foto: ' . $model->tem_id;
$this->params['breadcrumbs'][] = ['label' => 'Tema Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tem_id, 'url' => ['view', 'tem_id' => $model->tem_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tema-foto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

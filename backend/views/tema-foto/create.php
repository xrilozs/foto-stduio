<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TemaFoto */

$this->title = 'Tambah Tema Foto';
$this->params['breadcrumbs'][] = ['label' => 'Tema Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tema-foto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

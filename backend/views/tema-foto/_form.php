<?php

use common\models\PaketKategori;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\TemaFoto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Input Paket Foto</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="dropdown-item" href="#">Settings 1</a>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="customer-form">
                    <?php $form = ActiveForm::begin([
                    	'options'=>['enctype'=>'multipart/form-data']
                    ]); ?>

                    <?= $form->field($model, 'tem_kategori_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(PaketKategori::find()->all(),'kategori_id','kategori_nama'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Pilih Kategori Paket Foto'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>

				    <?= $form->field($model, 'tem_nama')->textInput() ?>

				    <?= $form->field($model, 'tem_gambar')->fileInput(); ?>

				    <div class="form-group">
				        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
				    </div>

				    <?php ActiveForm::end(); ?>

				</div>
            </div>
        </div>
    </div>
</div>

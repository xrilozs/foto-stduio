<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TemaFoto */

$this->title = $model->tem_id;
$this->params['breadcrumbs'][] = ['label' => 'Tema Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tema-foto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'tem_id' => $model->tem_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'tem_id' => $model->tem_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tem_paket_id',
            'tem_nama',
            [
                'attribute'=>'tem_gambar',
                'value'=>$model->tem_gambar,
                'format' => ['image',['width'=>'150','height'=>'150']],                 
            ],
        ],
    ]) ?>

</div>

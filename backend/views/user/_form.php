<?php

use common\models\Karyawan;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2><small>Silahkan Input Jadwal Pemesanan</small></h2>
                <ul class="nav navbar-right panel_toolbox"></ul>
                <div class="clearfix"></div>
            </div>
            <div class="user-form" data-parsley-validate class="form-horizontal form-label-left">
                <?php $form = ActiveForm::begin(); ?>

                <div class="form-group">
                    <?php 
                        $karyawan = ArrayHelper::map(Karyawan::find()->all(), 'kary_id', function($model, $default){
                            return $model["kary_nama"];
                        });
                    ?>
                    <?= $form->field($model, 'id_karyawan')->widget(Select2::classname(), [
                        'data' => $karyawan,
                        'language' => 'en',
                        'options' => [
                            'value' => (!$model->isNewRecord ? $selected : ''), 
                            'placeholder' => 'Pilih Nama Karyawan',
                            'id' => 'idkaryawan'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ]
                    ]); 
                    ?>
                </div>

                <div class="form-group">
                    <?= $form->field($model, 'username')->textInput() ?>
                </div>

                <div class="form-group">
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>

                <div class="form-group">
                    <?= $form->field($model, 'status')->textInput() ?>
                </div>

                <div class="form-group">
                    <?= $form->field($model, 'role')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaketUpgrade */

$this->title = 'Update Paket Upgrade: ' . $model->upg_id;
$this->params['breadcrumbs'][] = ['label' => 'Paket Upgrades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->upg_id, 'url' => ['view', 'upg_id' => $model->upg_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paket-upgrade-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

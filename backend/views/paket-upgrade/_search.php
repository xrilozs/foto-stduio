<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaketUpgradeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paket-upgrade-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'upg_id') ?>

    <?= $form->field($model, 'upg_nama') ?>

    <?= $form->field($model, 'upg_gambar') ?>

    <?= $form->field($model, 'upg_harga') ?>

    <?= $form->field($model, 'upg_keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

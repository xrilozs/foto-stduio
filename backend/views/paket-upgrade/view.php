<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PaketUpgrade */

$this->title = $model->upg_id;
$this->params['breadcrumbs'][] = ['label' => 'Paket Upgrades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="paket-upgrade-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'upg_id' => $model->upg_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'upg_id' => $model->upg_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'upg_id',
            'upg_nama',
            [
                'attribute'=>'upg_gambar',
                'value'=>$model->upg_gambar,
                'format' => ['image',['width'=>'150','height'=>'150']],                 
            ],
            'upg_harga',
            'upg_keterangan',
        ],
    ]) ?>

</div>

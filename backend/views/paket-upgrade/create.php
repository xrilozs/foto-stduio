<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PaketUpgrade */

$this->title = 'Form Paket Upgrade Foto';
$this->params['breadcrumbs'][] = ['label' => 'Paket Upgrades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-upgrade-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

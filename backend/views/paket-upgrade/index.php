<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PaketUpgradeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List Paket Upgrade';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-upgrade-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Paket Upgrade', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'upg_id',
            'upg_nama',
            [
                'attribute'=>'upg_gambar',
                'value'=>'upg_gambar',
                'format' => ['image',['width'=>'250','height'=>'200']],                 
            ],
            [
                'attribute' => 'upg_harga',
                'format' => 'currency',
            ],
            'upg_keterangan',
        ],
    ]); ?>


</div>

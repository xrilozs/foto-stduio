<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UkuranCetak */

$this->title = $model->ukuran_id;
$this->params['breadcrumbs'][] = ['label' => 'Ukuran Cetaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ukuran-cetak-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'ukuran_id' => $model->ukuran_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'ukuran_id' => $model->ukuran_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ukuran_id',
            'ukuran_nama',
            'ukuran_size',
            'ukuran_harga',
        ],
    ]) ?>

</div>

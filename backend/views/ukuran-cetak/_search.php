<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UkuranCetakSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ukuran-cetak-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ukuran_id') ?>

    <?= $form->field($model, 'ukuran_nama') ?>

    <?= $form->field($model, 'ukuran_size') ?>

    <?= $form->field($model, 'ukuran_harga') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

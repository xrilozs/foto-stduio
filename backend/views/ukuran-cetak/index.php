<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UkuranCetakSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ukuran Cetaks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ukuran-cetak-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ukuran Cetak', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ukuran_id',
            'ukuran_nama',
            'ukuran_size',
            'ukuran_harga',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, UkuranCetak $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'ukuran_id' => $model->ukuran_id]);
                 }
            ],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UkuranCetak */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ukuran-cetak-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ukuran_nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ukuran_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ukuran_harga')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UkuranCetak */

$this->title = 'Create Ukuran Cetak';
$this->params['breadcrumbs'][] = ['label' => 'Ukuran Cetaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ukuran-cetak-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

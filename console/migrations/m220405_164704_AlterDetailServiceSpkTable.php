<?php

use yii\db\Migration;

/**
 * Class m220405_164704_AlterDetailServiceSpkTable
 */
class m220405_164704_AlterDetailServiceSpkTable extends Migration
{


    public function init(){
        $this->db = 'db';
        parent::init();
    }

    private $table = "{{detail_service_spk}}";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, 'serv_fotografer_id', $this->integer());
        $this->addColumn($this->table, 'serv_asisten_id', $this->integer());
        $this->addColumn($this->table, 'serv_cs_id', $this->integer());

        $this->createIndex('idx_fotografer',$this->table, 'serv_fotografer_id');
        $this->createIndex('idx_asisten',$this->table, 'serv_asisten_id');
        $this->createIndex('idx_cs',$this->table, 'serv_cs_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropIndex('idx_fotografer',$this->table);
        $this->dropIndex('idx_asisten',$this->table);
        $this->dropIndex('idx_cs',$this->table);

        $this->dropColumn($this->table, 'serv_fotografer_id');
        $this->dropColumn($this->table, 'serv_asisten_id');
        $this->dropColumn($this->table, 'serv_cs_id');

        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220405_164704_AlterDetailServiceSpkTable cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m220405_173029_AlterDetailServiceSpkRelation
 */
class m220405_173029_AlterDetailServiceSpkRelation extends Migration
{


    private $table = "{{detail_service_spk}}";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_fotografer',
            $this->table, 
            'serv_fotografer_id', 
            'karyawan', 
            'kary_id', 
            'RESTRICT', 
            'CASCADE'
        );

        $this->addForeignKey('fk_asisten',$this->table, 'serv_asisten_id', 'karyawan', 
            'kary_id', 
            'RESTRICT', 
            'CASCADE');
        $this->addForeignKey('fk_cs',$this->table, 'serv_cs_id', 'karyawan', 
            'kary_id', 
            'RESTRICT', 
            'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropForeignKey(
            'fk_fotografer',
            $this->table
        );

        $this->dropForeignKey('fk_asisten',$this->table);
        $this->dropForeignKey('fk_cs',$this->table);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220405_173029_AlterDetailServiceSpkRelation cannot be reverted.\n";

        return false;
    }
    */
}

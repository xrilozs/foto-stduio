<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JenisFrameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenis Frames';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-frame-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Jenis Frame', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'frame_id',
            'frame_nama',
            'frame_ukuran',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, JenisFrame $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'frame_id' => $model->frame_id]);
                 }
            ],
        ],
    ]); ?>


</div>

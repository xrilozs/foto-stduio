<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\JenisFrame */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-frame-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'frame_nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'frame_ukuran')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

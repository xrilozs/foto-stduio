<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JenisFrame */

$this->title = 'Create Jenis Frame';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Frames', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-frame-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

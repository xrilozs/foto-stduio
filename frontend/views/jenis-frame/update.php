<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JenisFrame */

$this->title = 'Update Jenis Frame: ' . $model->frame_id;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Frames', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->frame_id, 'url' => ['view', 'frame_id' => $model->frame_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenis-frame-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

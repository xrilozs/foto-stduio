<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\cmenu\ContextMenu;

?>


<h1>Selamat Datang, <br/> di Milano Art Studio</h1>
<h2>Silahkan Pilih Foto Terbaik Kamu di Momen Ini!</h2>
<?php
$wizard_config = [
	'id' => 'stepwizard',
	'steps' => [
		1 => [
            'title' => 'Form Pilih Foto',
			'icon' => 'glyphicon glyphicon-cloud-download',
			'content' => '<h3>Pilih Foto</h3>Lakukan Pemilihan Foto Terlebih Dahulu',
            'url' => ['/foto/step1'],
		],
		2 => [
			'title' => 'Rekap',
			'icon' => 'glyphicon glyphicon-cloud-upload',
			'content' => '<h3>Rekap</h3>Pastikan fotonya sudah sesuai harapan Kamu',
		],
		3 => [
			'title' => 'Finalisasi',
			'icon' => 'glyphicon glyphicon-transfer',
			'content' => '<h3>Finalisasi</h3>Foto akan di proses</h3>',
		],
	],
	'complete_content' => '<h2>Terima Kasih Sudah Mengabadikan Momen Kamu di Milano Art Studio </h2>', 
];
?>

<?= \drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>

<?php foreach ($model as $mod) { ?>
  <div class="col-xs-4 col-sm-3 col-md-2 nopad text-center">
        <?php
        $items = [
            ['label'=>'Foto Utama   0/1', 'url'=>'#'],
            '<div class="dropdown-divider"></div>',
            ['label'=>'Cetak   0/3', 'url'=>'#'],
            '<div class="dropdown-divider"></div>',
            ['label'=>'Tambah Foto   0/1', 'url'=>'#'],
        ];            
        // Context menu usage on a div container
        ContextMenu::begin(['items'=>$items, 'options'=>['tag'=>'div']]);?>
        <!-- echo '<div class="well card card-body bg-light kv-context"><h3>Klik Kanan</h3></div>'; -->
        <img class="img-responsive well card card-body bg-light kv-context" src="<?php echo Yii::getAlias('@web/foto_customer').'/'.$mod->foto_image;?>" alt="image"/>
        <?php
        ContextMenu::end();
        ?>
  </div>
<?php 
}
?>


<?php 

$script = <<< 'JS'
function (e, element, target) {
    e.preventDefault();
    if (e.target.tagName == 'SPAN') {
        e.preventDefault();
        this.closemenu();
        return false;
    }
    return true;
}
JS;
?>
<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\cmenu\ContextMenu;
$this->title = 'Print Bukti Pilih Foto';
?>
<div class="row" style="height:100%">
    <div class="col-sm-12 d-flex justify-content-center align-items-center">
        <div class="row">
            <div class="col-sm-12">
                <h2>Terima Kasih Sudah Mengabadikan Momen Kamu di Studio Foto </h2>
            </div>
            <div class="col-sm-12">
                <span style="font-size:16px;">Silahkan print bukti pemilihan foto: 
                    <a href="index.php?r=foto/print&hasil_foto_id=<?=$hasil_foto_id;?>">
                        <i class="fas fa-print"></i> Print
                    </a>
                </span>
            </div>
        </div>
    </div>
</div>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fotos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="foto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Foto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'foto_id',
            'foto_detcus_id',
            'foto_nama_file',
            'foto_image',
            'foto_ket',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Foto $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'foto_id' => $model->foto_id]);
                 }
            ],
        ],
    ]); ?>


</div>

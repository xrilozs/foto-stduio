<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Foto */

$this->title = $model->foto_id;
$this->params['breadcrumbs'][] = ['label' => 'Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="foto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'foto_id' => $model->foto_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'foto_id' => $model->foto_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'foto_id',
            'foto_detcus_id',
            'foto_nama_file',
            'foto_image',
            'foto_ket',
        ],
    ]) ?>

</div>

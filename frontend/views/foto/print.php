<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\cmenu\ContextMenu;
$this->title = 'Print';
?>
<div class="row">
    <div class="col-lg-12" style="text-align:center;">
        <h1>MILANO ART STUDIO</h1>
        <span>Jl. Sekip No. 18/32, Medan</span>
    </div>
</div>
<div class="row" style="margin-top:30px;">
    <div class="col-sm-6" >
        <form>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-3 col-form-label">No. Invoice</label>
                <div class="col-sm-9">
                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?=$customer->cust_no_invoice;?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-3 col-form-label">Nama Kustomer</label>
                <div class="col-sm-9">
                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?=$customer->cust_nama;?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-3 col-form-label">Paket Foto</label>
                <div class="col-sm-9">
                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?=$paket_foto->paket_nama;?>">
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-6">
        <form>
            <div class="form-group row">
                <label for="tanggal_foto" class="col-sm-3 col-form-label">Tanggal Foto</label>
                <div class="col-sm-9">
                <input type="text" readonly class="form-control-plaintext" id="tanggal_foto" value="<?=$detail_customer->detcus_tanggal;?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="deadline_date" class="col-sm-3 col-form-label">Tanggal Deadline</label>
                <div class="col-sm-9">
                <input type="text" readonly class="form-control-plaintext" id="deadline_date" >
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row" style="margin-top:30px;">
    <div class="col-sm-12">
        <hr style="border:solid 1px;">
        <span>Foto yang dipilih</span>
    </div>
</div>
<div class="row" style="margin-top:30px;">
<?php foreach ($list_foto as $mod) { ?>
  <div class="col-xs-4 col-sm-3 col-md-2 nopad text-center " >
    <a href="<?php echo '/babymilano/backend/web/uploads/' . $mod->foto_image;?>" data-fancybox="gallery">
      <img class="img-responsive well card card-body kv-context context-menu" src="<?php echo  '/babymilano/backend/web/uploads/' . $mod->foto_image;?>" alt="image" style="width:100px;"/>
    </a>
  </div>
<?php } ?>
</div>
<div class="row" style="margin-top:30px;">
    <div class="col-sm-12">
        <hr style="border:solid 1px;">
    </div>
</div>
<div class="row" style="margin-top:50px;">
    <div class="col-lg-12" style="text-align:center;">
        <h4>TERIMA KASIH SUDAH MENGABADIKAN MOMEN PENTING ANDA</h4>
        <h4> BERSAMA KAMI</h4>
    </div>
</div>

<?php
$script = <<< 'JS'
    let tanggal_foto = $('#tanggal_foto').val()
    $('#deadline_date').val(getDeadlineDate(new Date()))
    $('#tanggal_foto').val(formatDate(new Date(tanggal_foto)))

    function getDeadlineDate(date, limit=18, weekendDayBefore=null){
      let start = date
      let start_temp = new Date(start.getTime());
      let finish_timestamp = start_temp.setDate(start_temp.getDate() + limit);
      let finish = new Date(finish_timestamp)
      let dayMilliseconds = 1000 * 60 * 60 * 24;
      let weekendDays = 0;
      while (start <= finish) {
        let day = start.getDay()
        if (day == 0) {
          weekendDays++;
        }
        start = new Date(+start + dayMilliseconds);
      }
      if(weekendDayBefore){
        if(weekendDayBefore == weekendDays){
          console.log("FINISH: ", formatDate(finish))
          return formatDate(finish)
        }else{
          console.log("start: ", formatDate(start))
          console.log("limit: ", limit+weekendDays)
          console.log("weekend day: ", weekendDays)
          return getDeadlineDate(new Date(), limit+weekendDays, weekendDays)
        }
      }else{
        
        console.log("start: ", formatDate(start))
        console.log("limit: ", limit+weekendDays)
        console.log("weekend day: ", weekendDays)
        if(weekendDays > 0){
          return getDeadlineDate(new Date(), limit+weekendDays, weekendDays)          
        }else{
          return formatDate(finish)
        }
      }
    }

    function formatDate(dateObj){
      let today = dateObj;
      let dd = String(today.getDate()).padStart(2, '0');
      let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = today.getFullYear();

      today = dd + '/' + mm + '/' + yyyy;
      return today
    }
    window.print();
JS;
  $this->registerJs($script);
?>
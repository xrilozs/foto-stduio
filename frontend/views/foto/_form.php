<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Foto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="foto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'foto_detcus_id')->textInput() ?>

    <?= $form->field($model, 'foto_nama_file')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto_ket')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

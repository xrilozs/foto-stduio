<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HasilFotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hasil Fotos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hasil-foto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Hasil Foto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'hf_id',
            'hf_detcus_id',
            'hf_foto_utama',
            'hf_keterangan_utama:ntext',
            'hf_foto_cetak',
            //'hf_keterangan_cetak:ntext',
            //'hf_foto_tambahan',
            //'hf_keterangan_tambahan:ntext',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, HasilFoto $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'hf_id' => $model->hf_id]);
                 }
            ],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HasilFoto */

$this->title = 'Create Hasil Foto';
$this->params['breadcrumbs'][] = ['label' => 'Hasil Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hasil-foto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

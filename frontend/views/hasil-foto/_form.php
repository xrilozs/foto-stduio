<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HasilFoto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hasil-foto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hf_detcus_id')->textInput() ?>

    <?= $form->field($model, 'hf_foto_utama')->textInput() ?>

    <?= $form->field($model, 'hf_keterangan_utama')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'hf_foto_cetak')->textInput() ?>

    <?= $form->field($model, 'hf_keterangan_cetak')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'hf_foto_tambahan')->textInput() ?>

    <?= $form->field($model, 'hf_keterangan_tambahan')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HasilFotoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hasil-foto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'hf_id') ?>

    <?= $form->field($model, 'hf_detcus_id') ?>

    <?= $form->field($model, 'hf_foto_utama') ?>

    <?= $form->field($model, 'hf_keterangan_utama') ?>

    <?= $form->field($model, 'hf_foto_cetak') ?>

    <?php // echo $form->field($model, 'hf_keterangan_cetak') ?>

    <?php // echo $form->field($model, 'hf_foto_tambahan') ?>

    <?php // echo $form->field($model, 'hf_keterangan_tambahan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

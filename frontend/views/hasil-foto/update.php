<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HasilFoto */

$this->title = 'Update Hasil Foto: ' . $model->hf_id;
$this->params['breadcrumbs'][] = ['label' => 'Hasil Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->hf_id, 'url' => ['view', 'hf_id' => $model->hf_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hasil-foto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\HasilFoto */

$this->title = $model->hf_id;
$this->params['breadcrumbs'][] = ['label' => 'Hasil Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="hasil-foto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'hf_id' => $model->hf_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'hf_id' => $model->hf_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'hf_id',
            'hf_detcus_id',
            'hf_foto_utama',
            'hf_keterangan_utama:ntext',
            'hf_foto_cetak',
            'hf_keterangan_cetak:ntext',
            'hf_foto_tambahan',
            'hf_keterangan_tambahan:ntext',
        ],
    ]) ?>

</div>

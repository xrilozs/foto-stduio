<?php

/** @var yii\web\View $this */

$this->title = 'MILANO ART STUDIO';
$js = <<< JS
    $('#invoice_form').submit(function(e){
        e.preventDefault()
        let invoice = $('#invoice_field').val()
        console.log(invoice)
        $('#err_msg').html('')
        $('#invoice_submit').prop("disabled", true);
        $.get('index.php?r=foto/get-invoice', { invoice : invoice }, function(data) {
            console.log("DATA:", data)
            if(data.status == 'failed'){
                console.log("ERROR")
                let err_msg = data.message
                $('#err_msg').html('<div class="alert alert-danger" role="alert">'+err_msg+'</div>')
                $('#invoice_submit').prop("disabled", false);
            }else{
                var customer_detail = data.data
                console.log("DETAIL:", customer_detail)
                if(customer_detail.detcus_status_pilih_foto == 'SUDAH PILIH FOTO'){
                    $('#err_msg').html('<div class="alert alert-danger" role="alert">Anda telah memilih Foto!</div>')
                    $('#invoice_submit').prop("disabled", false);
                }else{
                    let link = "index.php?r=foto%2Fstepper&foto_detcus_id="+customer_detail.detcus_id.toString()
                    window.location.href = link
                }
            }
            // $('#namacustomer').attr('value', data.customer.cust_nama);
        })
    })
JS;
$this->registerJs($js);
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">WELCOME!</h1>

        <p class="lead">MILANO ART STUDIO</p>
    </div>

    <div class="body-content">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <h2 align="center">SILAHKAN MASUKKAN NO. INVOICE</h2>
                <p align="center">Untuk memilih hasil foto terbaik Anda</p>
                <br/><br/><br/>
                <div id="err_msg"></div>
                <form id="invoice_form">
                    <div class="form-group">
                        <p align="center">Ketik No. Invoice di bawah ini!</p>
                        <div class="center col-md-12">
                        <input type="text" id="invoice_field" style = "text-align:center;" class="form-control" placeholder="No. Invoice">
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" id="invoice_submit" class="btn btn-primary btn-lg">Masuk</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

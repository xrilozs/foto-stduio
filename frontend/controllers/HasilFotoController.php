<?php

namespace frontend\controllers;

use common\models\HasilFoto;
use common\models\HasilFotoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HasilFotoController implements the CRUD actions for HasilFoto model.
 */
class HasilFotoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all HasilFoto models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new HasilFotoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HasilFoto model.
     * @param int $hf_id Hf ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($hf_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($hf_id),
        ]);
    }

    /**
     * Creates a new HasilFoto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new HasilFoto();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'hf_id' => $model->hf_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing HasilFoto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $hf_id Hf ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($hf_id)
    {
        $model = $this->findModel($hf_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'hf_id' => $model->hf_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing HasilFoto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $hf_id Hf ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($hf_id)
    {
        $this->findModel($hf_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HasilFoto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $hf_id Hf ID
     * @return HasilFoto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($hf_id)
    {
        if (($model = HasilFoto::findOne(['hf_id' => $hf_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace frontend\controllers;

use Yii;
use common\models\Customer;
use common\models\DetailCustomer;
use common\models\Foto;
use common\models\FotoSearch;
use common\models\OrderDetailPaketFoto;
use common\models\PaketFoto;
use common\models\PaketFrameDetail;
use common\models\JenisFrame;
use common\models\Album;
use common\models\HasilFoto;
use common\models\HasilFotoItem;
use common\models\DetailServiceSpk;
use common\models\Karyawan;
use common\models\PaketUpgrade;
use common\models\CetakFoto;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FotoController implements the CRUD actions for Foto model.
 */
class FotoController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Foto models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new FotoSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetInvoice($invoice){
        $customer = Customer::find()->where(['cust_no_invoice' => $invoice])->one();
        if(!$customer){
            $data = array("status" => "failed", "message" => "Nomor Invoice tidak valid");
            return $this->asJson($data);
        }
        $customer_detail = DetailCustomer::find()->where(['detcus_cust_id' => $customer->cust_id])->one();
        if(!$customer_detail){
            $data = array("status" => "failed", "message" => "Nomor Invoice tidak valid");
            return $this->asJson($data);
        }
        $foto = Foto::find()->where(['foto_detcus_id' => $customer_detail->detcus_id])->all();
        if(!$foto || sizeof($foto) < 1){
            $data = array("status" => "failed", "message" => "Foto belum diinput oleh admin");
            return $this->asJson($data);
        }
        return $this->asJson(array("status" => "success", "data" => $customer_detail));
    }

    /**
     * Displays a single Foto model.
     * @param int $foto_id Foto ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($foto_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($foto_id),
        ]);
    }

    /**
     * Creates a new Foto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Foto();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'foto_id' => $model->foto_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Foto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $foto_id Foto ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($foto_id)
    {
        $model = $this->findModel($foto_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'foto_id' => $model->foto_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Foto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $foto_id Foto ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($foto_id)
    {
        $this->findModel($foto_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Foto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $foto_id Foto ID
     * @return Foto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($foto_id)
    {
        if (($model = Foto::findOne(['foto_id' => $foto_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionStepper($foto_detcus_id = null)
    {
        if($foto_detcus_id){
            $modelFoto = Foto::find()->where(['foto_detcus_id' => $foto_detcus_id])->all();
        }else{
            $modelFoto = Foto::find()->all();
        }
        return $this->render('stepper', [
            'modelFoto' => $modelFoto,
        ]);
    }

    public function actionStep1($foto_detcus_id = '')
    {
        return $this->render('step1', [
            'modelFoto' => $modelFoto,
        ]);
    }

    public function actionStep2()
    {
        return $this->render('step2', [
            'modelFoto' => $modelFoto,
        ]);
    }

    public function actionStepComplete($hasil_foto_id)
    {
        return $this->render('stepComplete', [
            'hasil_foto_id' => $hasil_foto_id,
        ]);
    }

    public function actionPrint($hasil_foto_id)
    {
        $hasil_foto = HasilFoto::find()->where(['hf_id' => $hasil_foto_id])->one();
        $hasil_foto_item = HasilFotoItem::find()->where(['hasil_foto_id' => $hasil_foto_id])->all();
        
        $detail_customer = DetailCustomer::findOne($hasil_foto->hf_detcus_id);
        $customer = Customer::findOne($detail_customer->detcus_cust_id);

        $foto = Foto::find()->where(['foto_detcus_id' => $hasil_foto->hf_detcus_id])->all();

        $order_detail_paket_foto = OrderDetailPaketFoto::find()->where(['odpf_detcus_id' => $hasil_foto->hf_detcus_id])->one();

        $paket_foto = PaketFoto::find()->where(['paket_id' => $order_detail_paket_foto->odpf_paket_id])->one();

        $list_foto = [];
        foreach($foto as $item){
            foreach($hasil_foto_item as $hasil_item){
                if($hasil_item->foto_id == $item->foto_id){
                    array_push($list_foto, $item);
                }
            }
        }

        return $this->render('print', [
            'customer' => $customer,
            'detail_customer' => $detail_customer,
            'paket_foto' => $paket_foto,
            'list_foto' => $list_foto
        ]);
    }

    public function actionGetDetailFoto($detcus_id)
    {
        $data['detail_customer'] = DetailCustomer::findOne($detcus_id);
        $data['customer'] = Customer::findOne($data['detail_customer']->detcus_cust_id);
        $data['detail_service_spk'] = DetailServiceSpk::find()->where(['serv_detcus_id' => $detcus_id])->one();
        $data['karyawan'] = Karyawan::find()->where(['kary_id' => $data['detail_service_spk']->serv_cs_id])->one();
        $data['paket_upgrade'] = PaketUpgrade::find()->where(['upg_nama' => 'Foto Tambahan'])->one();
        $data['foto'] = Foto::find()->where(['foto_detcus_id' => $detcus_id])->all();

        $data['order_detail_paket_foto'] = OrderDetailPaketFoto::find()->where(['odpf_detcus_id' => $detcus_id])->one();
        $data['paket_foto'] = PaketFoto::find()->where(['paket_id' => $data['order_detail_paket_foto']->odpf_paket_id])->one();

        $paket_frame_detail = PaketFrameDetail::find()->where(['fd_paket_id' => $data['order_detail_paket_foto']->odpf_paket_id])->all();
        $data['jenis_frame'] = array();
        foreach($paket_frame_detail as $item){
            $jenis_frame = JenisFrame::find()->where(['frame_id' => $item->fd_frame_id])->one();
            array_push($data['jenis_frame'], $jenis_frame);
        }
        $data['album'] = Album::find()->where(['album_id' => $data['paket_foto']->paket_album_id])->all();
        $data['cetak_foto'] = CetakFoto::find()->where(['cetak_id' => $data['paket_foto']->paket_cetak_id])->one();

        return $this->asJson($data);
    }

    public function actionSaveHasilFoto()
    {
        $request = Yii::$app->request;

        $req = $request->post();
        $detcus_id = $req['detcus_id'];
        $foto = $req['foto'];
        $deadline_date = $req['deadline_date'];
        $signature = $req['signature'];
        $t=time();
        $url = "uploads/SIGN-$t-$detcus_id.png";

        list($type, $signature) = explode(';', $signature);
        list(, $signature)      = explode(',', $signature);
        $signature = base64_decode($signature);

        file_put_contents(Yii::getAlias("@backend") . "/web/$url", $signature);

        $detail_customer = DetailCustomer::find()->where(['detcus_id' => $detcus_id])->one();
        $detail_customer->detcus_status_pilih_foto = 'SUDAH PILIH FOTO';
        $detail_customer->detcus_tgl_deadline = $deadline_date;
        $detail_customer->detcus_ttd_customer = $url;
        if($detail_customer->save()){
        }else{
            return $this->asJson(array('status' => 'failed', 'message' => 'gagal mengubah data detail customer'));
        }

        $hasil_foto = new HasilFoto();
        $hasil_foto->hf_detcus_id = $detcus_id;
        if($hasil_foto->save()){
        }else{
            return $this->asJson(array('status' => 'failed', 'message' => 'gagal membuat data hasil foto'));
        }


        foreach($foto as $item){
            $hasil_foto_item = new HasilFotoItem();
            $hasil_foto_item->hasil_foto_id = $hasil_foto->hf_id;
            $hasil_foto_item->type = $item['type'];
            $hasil_foto_item->foto_id = $item['foto_id'];
            $hasil_foto_item->keterangan = $item['keterangan'];
            $hasil_foto_item->jenis_frame_id = $item['jenis_frame_id'];
            $hasil_foto_item->cetak_foto_id = $item['cetak_foto_id'];
            $hasil_foto_item->save();
        }

        return $this->asJson(array('status' => 'success', 'data' => $hasil_foto->hf_id));
    }
}

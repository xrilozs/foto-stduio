<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_frame".
 *
 * @property int $frame_id
 * @property string $frame_nama
 * @property string $frame_ukuran
 */
class JenisFrame extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_frame';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['frame_nama', 'frame_ukuran'], 'required'],
            [['frame_nama', 'frame_ukuran'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'frame_id' => 'Frame ID',
            'frame_nama' => 'Frame Nama',
            'frame_ukuran' => 'Frame Ukuran',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hasil_foto".
 *
 * @property int $hf_id
 * @property int $hf_detcus_id
 * @property int $hf_foto_utama
 * @property string $hf_keterangan_utama
 * @property int $hf_foto_cetak
 * @property string $hf_keterangan_cetak
 * @property int $hf_foto_tambahan
 * @property string $hf_keterangan_tambahan
 */
class HasilFoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hasil_foto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hf_detcus_id', 'hf_foto_utama', 'hf_keterangan_utama', 'hf_foto_cetak', 'hf_keterangan_cetak', 'hf_foto_tambahan', 'hf_keterangan_tambahan'], 'required'],
            [['hf_detcus_id', 'hf_foto_utama', 'hf_foto_cetak', 'hf_foto_tambahan'], 'integer'],
            [['hf_keterangan_utama', 'hf_keterangan_cetak', 'hf_keterangan_tambahan'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hf_id' => 'Hf ID',
            'hf_detcus_id' => 'Hf Detcus ID',
            'hf_foto_utama' => 'Hf Foto Utama',
            'hf_keterangan_utama' => 'Hf Keterangan Utama',
            'hf_foto_cetak' => 'Hf Foto Cetak',
            'hf_keterangan_cetak' => 'Hf Keterangan Cetak',
            'hf_foto_tambahan' => 'Hf Foto Tambahan',
            'hf_keterangan_tambahan' => 'Hf Keterangan Tambahan',
        ];
    }
}

-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Jun 2022 pada 11.37
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `milanold`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `album`
--

CREATE TABLE `album` (
  `album_id` int(11) NOT NULL,
  `album_nama` varchar(30) NOT NULL,
  `album_warna` varchar(30) NOT NULL,
  `album_gambar` varchar(50) NOT NULL,
  `album_ket` varchar(50) NOT NULL,
  `album_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `album`
--

INSERT INTO `album` (`album_id`, `album_nama`, `album_warna`, `album_gambar`, `album_ket`, `album_harga`) VALUES
(1, 'Polaroid', 'Cream', 'uploads/album_foto/Polaroid1853.jpg', 'tes', 70000),
(2, 'Polaroid', 'White', 'uploads/album_foto/Polaroid269.jpg', 'tes', 70000),
(3, 'Polaroid', 'Black', 'uploads/album_foto/Polaroid3545.jpg', 'tes', 70000),
(4, 'Scrapbook', 'Cream', 'uploads/album_foto/Scrapbook694.jpg', 'tes', 95000),
(5, 'Scrapbook', 'Black', 'uploads/album_foto/Scrapbook406.jpg', 'tes', 95000),
(6, 'Scrapbook', 'White', 'uploads/album_foto/Scrapbook367.jpg', 'tes', 95000),
(7, 'Scrapbook', 'Pink', 'uploads/album_foto/Scrapbook3690.jpg', 'tes', 95000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '2', 1651050171);

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, 1651050171, 1651050171),
('album/create', 2, 'Create Produk Album', NULL, NULL, 1651899045, 1651899045),
('album/index', 2, 'List Index Produk Album', NULL, NULL, 1651899026, 1651899026),
('album/view', 2, 'View Produk Album', NULL, NULL, 1651899059, 1651899059),
('cetak-foto/create', 2, 'Create Cetak Foto', NULL, NULL, 1651899218, 1651899218),
('cetak-foto/index', 2, 'List Index Cetak Foto', NULL, NULL, 1651899199, 1651899238),
('customer service', 1, NULL, NULL, NULL, 1651897798, 1651897798),
('customer/create', 2, 'Create Customer', NULL, NULL, 1651898144, 1651898144),
('customer/index', 2, 'View Customer', NULL, NULL, 1651898178, 1651898178),
('detail-customer/create', 2, 'Create (C1) Form Paket Foto', NULL, NULL, 1651898453, 1651898453),
('detail-customer/index', 2, 'List Index Paket Foto Studio', NULL, NULL, 1651899598, 1651899598),
('detail-customer/pototema', 2, 'View Galeri Tema Foto', NULL, NULL, 1651899524, 1651899524),
('detail-customer/view', 2, 'View (C1) Form Paket Foto', NULL, NULL, 1651898486, 1651898486),
('foto/create', 2, 'Create Form Foto', NULL, NULL, 1651898707, 1651898707),
('foto/view', 2, 'View Foto', NULL, NULL, 1651898730, 1651898730),
('jenis-frame/create', 2, 'Create Jenis Frame', NULL, NULL, 1651898972, 1651898972),
('jenis-frame/index', 2, 'List Index Jenis Frame', NULL, NULL, 1651898961, 1651898961),
('jenis-frame/view', 2, 'View Jenis Frame', NULL, NULL, 1651898986, 1651898986),
('karyawan/create', 2, 'Create Karyawan', NULL, NULL, 1651899557, 1651899557),
('karyawan/index', 2, 'List Index Karyawan', NULL, NULL, 1651899571, 1651899571),
('order-detail-tema/choose', 2, 'Create Choose Form SPK Fotografi', NULL, NULL, 1651898559, 1651898594),
('order-detail-tema/create', 2, 'Create Form SPK Fotografi', NULL, NULL, 1651898648, 1651898648),
('paket-foto/create', 2, 'Create Paket Foto', NULL, NULL, 1651898777, 1651898777),
('paket-foto/index', 2, 'List Index Paket Foto', NULL, NULL, 1651898790, 1651898826),
('paket-foto/view', 2, 'View Paket Foto', NULL, NULL, 1651898845, 1651898845),
('paket-kategori/create', 2, 'Create Paket Kategori', NULL, NULL, 1651050171, 1651050171),
('paket-kategori/delete', 2, 'Delete Paket Kategori', NULL, NULL, 1651050171, 1651050171),
('paket-kategori/index', 2, 'Index Paket Kategori', NULL, NULL, 1651050171, 1651050171),
('paket-kategori/view', 2, 'View Paket Kategori', NULL, NULL, 1651050171, 1651050171),
('paket-upgrade/create', 2, 'Create Paket Upgrade', NULL, NULL, 1651898882, 1651898882),
('paket-upgrade/index', 2, 'List Index Paket Upgrade', NULL, NULL, 1651898921, 1651898921),
('paket-upgrade/view', 2, 'View Paket Upgrade', NULL, NULL, 1651898906, 1651898906),
('receptionist', 1, NULL, NULL, NULL, 1651897829, 1651897829),
('site/index', 2, 'Beranda', NULL, NULL, 1651899630, 1651899630),
('superadmin', 1, NULL, NULL, NULL, 1651050171, 1651050171),
('supervisor', 1, NULL, NULL, NULL, 1651897845, 1651897845),
('tema-foto/create', 2, 'Create Tema Foto', NULL, NULL, 1651899298, 1651899298),
('tema-foto/index', 2, 'List Index Tema Foto', NULL, NULL, 1651899315, 1651899315),
('tema-foto/view', 2, 'View Tema Foto', NULL, NULL, 1651899331, 1651899331);

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'paket-kategori/create'),
('admin', 'paket-kategori/index'),
('admin', 'paket-kategori/view'),
('superadmin', 'paket-kategori/create'),
('superadmin', 'paket-kategori/delete'),
('superadmin', 'paket-kategori/index'),
('superadmin', 'paket-kategori/view');

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cetak_foto`
--

CREATE TABLE `cetak_foto` (
  `cetak_id` int(11) NOT NULL,
  `cetak_nama` varchar(30) NOT NULL,
  `cetak_size` varchar(30) NOT NULL,
  `cetak_harga` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cetak_foto`
--

INSERT INTO `cetak_foto` (`cetak_id`, `cetak_nama`, `cetak_size`, `cetak_harga`) VALUES
(1, '2R', '2.2\" x 3.5', 50000),
(2, '3R', '3.5 X 6', 50000),
(3, 'S8R', '8\" x 10\"', 75000),
(4, '10 R ', '10\" x 12\"', 85000),
(5, '6 inch', '6 inch', 50000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `cust_id` int(11) NOT NULL,
  `cust_no_invoice` varchar(10) NOT NULL,
  `cust_nama` varchar(50) NOT NULL,
  `cust_telp` varchar(100) NOT NULL,
  `cust_email` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`cust_id`, `cust_no_invoice`, `cust_nama`, `cust_telp`, `cust_email`) VALUES
(1, '#2022', 'lamvita', '09834783321', 'vita@gmail.com'),
(2, '#20231', 'joko widodo', '098234723468', 'emai@email.com'),
(3, '#20232', 'Najwa Shihab', '098234723467', 'email@email.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_customer`
--

CREATE TABLE `detail_customer` (
  `detcus_id` int(11) NOT NULL,
  `detcus_cust_id` int(11) NOT NULL,
  `detcus_tanggal` date NOT NULL,
  `detcus_pay` int(11) NOT NULL,
  `detcus_status` enum('DP','Lunas') NOT NULL,
  `detcus_status_pilih_foto` enum('BELUM PILIH FOTO','SUDAH PILIH FOTO') NOT NULL,
  `detcus_ttd_customer` varchar(200) NOT NULL,
  `detcus_tgl_deadline` date DEFAULT NULL,
  `lokasi_folder_start` varchar(200) DEFAULT NULL,
  `lokasi_folder_finish` varchar(200) DEFAULT NULL,
  `total_bayar_tambahan` int(20) DEFAULT NULL,
  `status_bayar_tambahan` enum('Belum Lunas','Lunas') DEFAULT NULL,
  `status_validasi_spk` enum('Belum di Verifikasi','Disetujui','Ditolak') DEFAULT NULL,
  `tanggal_verifikasi` date DEFAULT NULL,
  `tanggal_bayar_tambahan` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `detail_customer`
--

INSERT INTO `detail_customer` (`detcus_id`, `detcus_cust_id`, `detcus_tanggal`, `detcus_pay`, `detcus_status`, `detcus_status_pilih_foto`, `detcus_ttd_customer`, `detcus_tgl_deadline`, `lokasi_folder_start`, `lokasi_folder_finish`, `total_bayar_tambahan`, `status_bayar_tambahan`, `status_validasi_spk`, `tanggal_verifikasi`, `tanggal_bayar_tambahan`) VALUES
(1, 1, '2022-04-26', 1500000, 'Lunas', 'SUDAH PILIH FOTO', 'uploads/SIGN-1655717411-1.png', '2022-06-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, '2022-05-06', 500000, 'DP', 'BELUM PILIH FOTO', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_service_spk`
--

CREATE TABLE `detail_service_spk` (
  `serv_id` int(11) NOT NULL,
  `serv_detcus_id` int(11) NOT NULL,
  `serv_studio` int(11) NOT NULL,
  `serv_fotografer_id` int(11) DEFAULT NULL,
  `serv_asisten_id` int(11) DEFAULT NULL,
  `serv_cs_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `detail_service_spk`
--

INSERT INTO `detail_service_spk` (`serv_id`, `serv_detcus_id`, `serv_studio`, `serv_fotografer_id`, `serv_asisten_id`, `serv_cs_id`) VALUES
(1, 1, 1, 3, 5, 7),
(2, 1, 1, 2, 7, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto`
--

CREATE TABLE `foto` (
  `foto_id` int(11) NOT NULL,
  `foto_detcus_id` int(11) NOT NULL,
  `foto_nama_file` varchar(400) NOT NULL,
  `foto_image` varchar(400) NOT NULL,
  `foto_ket` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `foto`
--

INSERT INTO `foto` (`foto_id`, `foto_detcus_id`, `foto_nama_file`, `foto_image`, `foto_ket`) VALUES
(1, 1, 'IMG_4684.jpg', 'IMG_4684.jpg', ''),
(2, 1, 'IMG_4685.jpg', 'IMG_4685.jpg', ''),
(3, 1, 'IMG_4686.jpg', 'IMG_4686.jpg', ''),
(4, 1, 'IMG_4687.jpg', 'IMG_4687.jpg', ''),
(5, 1, 'IMG_4688.jpg', 'IMG_4688.jpg', ''),
(6, 1, 'IMG_4689.jpg', 'IMG_4689.jpg', ''),
(20, 1, 'IMG_4704.jpg', 'IMG_4704.jpg', ''),
(21, 1, '2.jpg', '2.jpg', ''),
(22, 1, '10.jpg', '10.jpg', ''),
(23, 1, '11.jpg', '11.jpg', ''),
(24, 1, '12.jpg', '12.jpg', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_foto`
--

CREATE TABLE `hasil_foto` (
  `hf_id` int(11) NOT NULL,
  `hf_detcus_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil_foto`
--

INSERT INTO `hasil_foto` (`hf_id`, `hf_detcus_id`) VALUES
(0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_foto_item`
--

CREATE TABLE `hasil_foto_item` (
  `id` int(11) NOT NULL,
  `hasil_foto_id` int(11) NOT NULL,
  `type` enum('MAIN','PRINT','ADD') NOT NULL,
  `foto_id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `jenis_frame_id` int(11) DEFAULT NULL,
  `cetak_foto_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_frame`
--

CREATE TABLE `jenis_frame` (
  `frame_id` int(11) NOT NULL,
  `frame_nama_produk` varchar(50) NOT NULL,
  `frame_ukuran` varchar(30) NOT NULL,
  `frame_warna` varchar(30) NOT NULL,
  `frame_gambar` varchar(200) NOT NULL,
  `frame_harga` int(11) NOT NULL,
  `frame_ket` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `jenis_frame`
--

INSERT INTO `jenis_frame` (`frame_id`, `frame_nama_produk`, `frame_ukuran`, `frame_warna`, `frame_gambar`, `frame_harga`, `frame_ket`) VALUES
(1, 'Frameblok', '18 inch (30×45 cm)', 'Coklat', 'uploads/frame_foto/Frameblok787.jpg', 50000, 'ters'),
(2, 'Frameblok', '10 inch (20×25 cm)', 'White', 'uploads/frame_foto/Frameblok395.jpg', 30000, 'tesss'),
(3, 'Frameblok', '(20×30 cm)', 'Navy', 'uploads/frame_foto/Frameblok230.jpg', 65000, 'tessss'),
(4, 'Kanvas', '30×40 cm', 'Putih', 'uploads/frame_foto/Kanvas146.jpg', 100000, 'blabla'),
(5, 'Kanvas', '40cm x 40cm', 'Cream', 'uploads/frame_foto/Kanvas2286.jpg', 120000, 'lalalala'),
(6, 'Kanvas', '45cm x 45cm', 'Pink', 'uploads/frame_foto/Kanvas2747.jpg', 150000, 'tes'),
(7, 'Frame 3D Plat', '25cm x 25cm', 'Merah', 'uploads/frame_foto/Frame 3D Plat1000.jpg', 50000, 'tes'),
(8, 'Frame 3D Plat', '40cm x 40cm', 'Dark', 'uploads/frame_foto/Frame 3D Plat213.jpg', 75000, 'tes'),
(9, 'Frame 3D Plat', '50cm x 50cm', 'Black', 'uploads/frame_foto/Frame 3D Plat3055.jpg', 100000, 'tes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `kary_id` int(11) NOT NULL,
  `kary_nama` varchar(50) NOT NULL,
  `kary_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`kary_id`, `kary_nama`, `kary_status`) VALUES
(1, 'Lamvita Theresia', 'Software Developer'),
(2, 'Desi', 'Admin HR'),
(3, 'Puspita Sari', 'Admin '),
(4, 'Anita', 'Customer Service'),
(5, 'Dewi', 'Customer Service'),
(6, 'Fira', 'Customer Service'),
(7, 'Liza', 'Receptionist'),
(8, 'Agnes', 'Receptionist'),
(9, 'Tika', 'Asisten'),
(10, 'Sari', 'Asisten'),
(11, 'Sally', 'Asisten'),
(12, 'Fajar', 'Fotografer'),
(13, 'Isnan', 'Fotografer'),
(14, 'Iwan', 'Fotografer'),
(15, 'Halim', 'Fotografer'),
(16, 'Auditor', 'Auditor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1646209336),
('m130524_201442_init', 1646209338),
('m140506_102106_rbac_init', 1651045778),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1651045778),
('m180523_151638_rbac_updates_indexes_without_prefix', 1651045778),
('m190124_110200_add_verification_token_column_to_user_table', 1646209339),
('m200409_110543_rbac_update_mssql_trigger', 1651045778),
('m220405_164704_AlterDetailServiceSpkTable', 1649179815),
('m220405_173029_AlterDetailServiceSpkRelation', 1649180253),
('m220427_081619_init_rbac', 1651050171);

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_detail_paket_foto`
--

CREATE TABLE `order_detail_paket_foto` (
  `odpf_id` int(11) NOT NULL,
  `odpf_detcus_id` int(11) NOT NULL,
  `odpf_paket_id` int(11) NOT NULL,
  `odpf_qty` int(11) NOT NULL,
  `odpf_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `order_detail_paket_foto`
--

INSERT INTO `order_detail_paket_foto` (`odpf_id`, `odpf_detcus_id`, `odpf_paket_id`, `odpf_qty`, `odpf_harga`) VALUES
(1, 1, 1, 1, 0),
(2, 2, 130, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_detail_paket_upgrade`
--

CREATE TABLE `order_detail_paket_upgrade` (
  `odpu_id` int(11) NOT NULL,
  `odpu_detcus_id` int(11) NOT NULL,
  `odpu_upg_id` int(11) NOT NULL,
  `odpu_qty` int(11) NOT NULL,
  `odpu_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `order_detail_paket_upgrade`
--

INSERT INTO `order_detail_paket_upgrade` (`odpu_id`, `odpu_detcus_id`, `odpu_upg_id`, `odpu_qty`, `odpu_harga`) VALUES
(1, 2, 2, 2, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_detail_tema`
--

CREATE TABLE `order_detail_tema` (
  `odtema_id` int(11) NOT NULL,
  `odtema_detcus_id` int(11) NOT NULL,
  `odtema_tem_id` int(11) NOT NULL,
  `odtema_ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `order_detail_tema`
--

INSERT INTO `order_detail_tema` (`odtema_id`, `odtema_detcus_id`, `odtema_tem_id`, `odtema_ket`) VALUES
(1, 1, 14, 'wajib pakai toga'),
(2, 1, 18, 'wajib pakai toga');

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket_foto`
--

CREATE TABLE `paket_foto` (
  `paket_id` int(11) NOT NULL,
  `paket_kategori_id` int(11) NOT NULL,
  `paket_nama` varchar(30) NOT NULL,
  `paket_gambar` varchar(200) DEFAULT NULL,
  `paket_jumlah_tema` int(11) NOT NULL,
  `paket_pose` int(11) NOT NULL,
  `paket_album_id` int(11) DEFAULT NULL,
  `paket_cetak_id` int(11) DEFAULT NULL,
  `paket_album_qty` int(11) DEFAULT NULL,
  `paket_cetak_qty` int(11) DEFAULT NULL,
  `paket_jumlah_foto_utama` int(11) NOT NULL,
  `paket_jumlah_cetak` int(11) NOT NULL,
  `paket_harga` varchar(20) NOT NULL,
  `paket_ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `paket_foto`
--

INSERT INTO `paket_foto` (`paket_id`, `paket_kategori_id`, `paket_nama`, `paket_gambar`, `paket_jumlah_tema`, `paket_pose`, `paket_album_id`, `paket_cetak_id`, `paket_album_qty`, `paket_cetak_qty`, `paket_jumlah_foto_utama`, `paket_jumlah_cetak`, `paket_harga`, `paket_ket`) VALUES
(1, 4, 'Family Sale', '', 2, 11, 4, 1, 1, 8, 3, 8, '1500000', 'hahahah'),
(3, 1, 'Weekdays Free Momen', '', 2, 11, 7, 1, 1, 8, 3, 8, '1500000', 'hahahah'),
(8, 1, 'Family Sale for Ramadhan', '', 2, 11, 2, 1, 1, 8, 3, 8, '1500000', 'hemmmmmmmm'),
(130, 3, 'Baby Story Christmas Day', '', 2, 11, 4, 3, 1, 8, 3, 8, '1000000', 'te4s'),
(155, 4, 'Wisuda Bareng-bareng', NULL, 2, 7, 3, NULL, 1, NULL, 2, 5, '1000000', 'tes'),
(156, 4, 'Wisuda Bareng Keluarga', NULL, 3, 13, 3, 2, 1, 10, 3, 10, '1500000', 'tesss');

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket_frame_detail`
--

CREATE TABLE `paket_frame_detail` (
  `fd_id` int(11) NOT NULL,
  `fd_paket_id` int(11) NOT NULL,
  `fd_frame_id` int(11) NOT NULL,
  `fd_qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `paket_frame_detail`
--

INSERT INTO `paket_frame_detail` (`fd_id`, `fd_paket_id`, `fd_frame_id`, `fd_qty`) VALUES
(1, 1, 6, 1),
(2, 1, 5, 2),
(3, 3, 9, 2),
(4, 3, 3, 1),
(5, 8, 7, 1),
(12, 155, 3, 2),
(13, 155, 1, 1),
(14, 156, 4, 1),
(15, 156, 6, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket_kategori`
--

CREATE TABLE `paket_kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `paket_kategori`
--

INSERT INTO `paket_kategori` (`kategori_id`, `kategori_nama`) VALUES
(1, 'Keluarga'),
(2, 'Maternity'),
(3, 'Baby Born'),
(4, 'Wisuda'),
(5, 'Prewedding'),
(6, 'Nikahan Batak Toba');

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket_upgrade`
--

CREATE TABLE `paket_upgrade` (
  `upg_id` int(11) NOT NULL,
  `upg_nama` varchar(100) NOT NULL,
  `upg_gambar` varchar(200) NOT NULL,
  `upg_harga` int(11) NOT NULL,
  `upg_keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `paket_upgrade`
--

INSERT INTO `paket_upgrade` (`upg_id`, `upg_nama`, `upg_gambar`, `upg_harga`, `upg_keterangan`) VALUES
(1, 'Make Up Artist', 'upg_gambarMake Up Artist3356.jpg', 300000, 'blskjdfsdjfkl'),
(2, 'Kostum', 'upg_gambarKostum2667.jpg', 50000, 'gdtuty fghrruu awetrwe '),
(3, 'Foto Tambahan', 'upg_gambarFoto Tambahan2422.jpg', 50000, 'Kosong');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemesanan`
--

CREATE TABLE `pemesanan` (
  `pes_id` int(11) NOT NULL,
  `pes_cust_id` int(11) NOT NULL,
  `pes_tgl_foto` date NOT NULL,
  `pes_jam_foto` time NOT NULL,
  `pes_tgl_pilih_foto` date NOT NULL,
  `pes_tgl_review` date NOT NULL,
  `pes_tgl_deadline` date NOT NULL,
  `pes_status_pembayaran` tinyint(1) NOT NULL COMMENT '0:Lunas1:DP'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pemesanan`
--

INSERT INTO `pemesanan` (`pes_id`, `pes_cust_id`, `pes_tgl_foto`, `pes_jam_foto`, `pes_tgl_pilih_foto`, `pes_tgl_review`, `pes_tgl_deadline`, `pes_status_pembayaran`) VALUES
(1, 1, '0000-00-00', '08:00:00', '0000-00-00', '0000-00-00', '0000-00-00', 1),
(2, 2, '0000-00-00', '08:00:00', '0000-00-00', '0000-00-00', '0000-00-00', 0),
(4, 1, '0000-00-00', '08:00:00', '0000-00-00', '0000-00-00', '0000-00-00', 1),
(5, 1, '2022-03-21', '09:00:00', '0000-00-00', '0000-00-00', '0000-00-00', 1),
(6, 1, '2022-03-21', '10:00:00', '2022-03-21', '2022-03-28', '2022-03-28', 1),
(7, 20220415, '2022-04-30', '12:00:00', '2022-04-30', '2022-04-30', '2022-04-30', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tema_foto`
--

CREATE TABLE `tema_foto` (
  `tem_id` int(11) NOT NULL,
  `tem_kategori_id` int(11) NOT NULL,
  `tem_nama` varchar(50) NOT NULL,
  `tem_gambar` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tema_foto`
--

INSERT INTO `tema_foto` (`tem_id`, `tem_kategori_id`, `tem_nama`, `tem_gambar`) VALUES
(2, 1, 'Family1', 'uploads/tema_gambar/Family1326.jpg'),
(3, 1, 'Family2', 'uploads/tema_gambar/Family2358.jpg'),
(4, 1, 'Family3', 'uploads/tema_gambar/Family32032.jpg'),
(5, 1, 'Family4', 'uploads/tema_gambar/Family4676.jpg'),
(6, 1, 'Family5', 'uploads/tema_gambar/Family5217.jpg'),
(7, 5, 'PR1', 'uploads/tema_gambar/PR1419.jpg'),
(8, 5, 'PW2', 'uploads/tema_gambar/PW2148.jpg'),
(9, 5, 'PW3', 'uploads/tema_gambar/PW3213.jpg'),
(10, 5, 'PW4', 'uploads/tema_gambar/PW42355.jpg'),
(11, 5, 'PW 5', 'uploads/tema_gambar/PW 51771.jpg'),
(12, 5, 'PW 6', 'uploads/tema_gambar/PW 62384.jpg'),
(13, 4, 'Wisuda1', 'uploads/tema_gambar/Wisuda11120.jpg'),
(14, 4, 'Wisuda2', 'uploads/tema_gambar/Wisuda2670.jpg'),
(15, 4, 'Wisuda3', 'uploads/tema_gambar/Wisuda32324.png'),
(16, 4, 'Wisuda 4', 'uploads/tema_gambar/Wisuda 43060.jpg'),
(17, 4, 'Wisuda 5', 'uploads/tema_gambar/Wisuda 52938.jpg'),
(18, 4, 'Wisuda 7', 'uploads/tema_gambar/Wisuda 72008.jpg'),
(19, 4, 'Wisuda 8', 'uploads/tema_gambar/Wisuda 81538.jpg'),
(20, 4, 'Wisuda 9', 'uploads/tema_gambar/Wisuda 93754.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`album_id`);

--
-- Indeks untuk tabel `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Indeks untuk tabel `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indeks untuk tabel `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indeks untuk tabel `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indeks untuk tabel `cetak_foto`
--
ALTER TABLE `cetak_foto`
  ADD PRIMARY KEY (`cetak_id`);

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indeks untuk tabel `detail_customer`
--
ALTER TABLE `detail_customer`
  ADD PRIMARY KEY (`detcus_id`);

--
-- Indeks untuk tabel `detail_service_spk`
--
ALTER TABLE `detail_service_spk`
  ADD PRIMARY KEY (`serv_id`),
  ADD KEY `serv_detcus_id` (`serv_detcus_id`),
  ADD KEY `idx_fotografer` (`serv_fotografer_id`),
  ADD KEY `idx_asisten` (`serv_asisten_id`),
  ADD KEY `idx_cs` (`serv_cs_id`);

--
-- Indeks untuk tabel `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`foto_id`),
  ADD KEY `foto_detcus_id` (`foto_detcus_id`);

--
-- Indeks untuk tabel `hasil_foto`
--
ALTER TABLE `hasil_foto`
  ADD PRIMARY KEY (`hf_id`);

--
-- Indeks untuk tabel `hasil_foto_item`
--
ALTER TABLE `hasil_foto_item`
  ADD KEY `id` (`id`),
  ADD KEY `foto_id` (`foto_id`),
  ADD KEY `jenis_frame_id` (`jenis_frame_id`),
  ADD KEY `hasil_foto_id` (`hasil_foto_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

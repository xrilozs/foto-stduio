<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HasilFoto;

/**
 * HasilFotoSearch represents the model behind the search form of `common\models\HasilFoto`.
 */
class HasilFotoSearch extends HasilFoto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hf_id', 'hf_detcus_id', 'hf_foto_utama', 'hf_foto_cetak', 'hf_foto_tambahan'], 'integer'],
            [['hf_keterangan_utama', 'hf_keterangan_cetak', 'hf_keterangan_tambahan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HasilFoto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'hf_id' => $this->hf_id,
            'hf_detcus_id' => $this->hf_detcus_id,
            'hf_foto_utama' => $this->hf_foto_utama,
            'hf_foto_cetak' => $this->hf_foto_cetak,
            'hf_foto_tambahan' => $this->hf_foto_tambahan,
        ]);

        $query->andFilterWhere(['like', 'hf_keterangan_utama', $this->hf_keterangan_utama])
            ->andFilterWhere(['like', 'hf_keterangan_cetak', $this->hf_keterangan_cetak])
            ->andFilterWhere(['like', 'hf_keterangan_tambahan', $this->hf_keterangan_tambahan]);

        return $dataProvider;
    }
}

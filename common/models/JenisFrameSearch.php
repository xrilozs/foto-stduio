<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\JenisFrame;

/**
 * JenisFrameSearch represents the model behind the search form of `common\models\JenisFrame`.
 */
class JenisFrameSearch extends JenisFrame
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['frame_id', 'frame_harga'], 'integer'],
            [['frame_nama_produk', 'frame_gambar', 'frame_ukuran', 'frame_ket'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JenisFrame::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'frame_id' => $this->frame_id,
            'frame_harga' => $this->frame_harga,
        ]);

        $query->andFilterWhere(['like', 'frame_nama_produk', $this->frame_nama_produk]);
            // ->andFilterWhere(['like', 'frame_gambar', $this->frame_gambar])
            // ->andFilterWhere(['like', 'frame_ukuran', $this->frame_ukuran])
            // ->andFilterWhere(['like', 'frame_ket', $this->frame_ket]);

        return $dataProvider;
    }
}

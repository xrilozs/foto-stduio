<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ukuran_cetak".
 *
 * @property int $ukuran_id
 * @property string $ukuran_nama
 * @property string $ukuran_size
 * @property int $ukuran_harga
 */
class UkuranCetak extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ukuran_cetak';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ukuran_nama', 'ukuran_size', 'ukuran_harga'], 'required'],
            [['ukuran_harga'], 'integer'],
            [['ukuran_nama'], 'string', 'max' => 50],
            [['ukuran_size'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ukuran_id' => 'Ukuran ID',
            'ukuran_nama' => 'Ukuran Nama',
            'ukuran_size' => 'Ukuran Size',
            'ukuran_harga' => 'Ukuran Harga',
        ];
    }

    /**
     * {@inheritdoc}
     * @return UkuranCetakQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UkuranCetakQuery(get_called_class());
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hasil_foto".
 *
 * @property int $hf_id
 * @property int $hf_detcus_id
 * @property int $hf_foto_utama
 * @property string $hf_keterangan_utama
 * @property int $hf_foto_cetak
 * @property string $hf_keterangan_cetak
 * @property int $hf_foto_tambahan
 * @property string $hf_keterangan_tambahan
 */
class HasilFotoItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hasil_foto_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [];
    }
}

<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pemesanan;

/**
 * PemesananSearch represents the model behind the search form of `common\models\Pemesanan`.
 */
class PemesananSearch extends Pemesanan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pes_id', 'pes_cust_id', 'pes_status_pembayaran'], 'integer'],
            [['pes_tgl_foto', 'pes_jam_foto', 'pes_tgl_pilih_foto', 'pes_tgl_review', 'pes_tgl_deadline'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pemesanan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pes_id' => $this->pes_id,
            'pes_cust_id' => $this->pes_cust_id,
            'pes_tgl_foto' => $this->pes_tgl_foto,
            'pes_jam_foto' => $this->pes_jam_foto,
            'pes_tgl_pilih_foto' => $this->pes_tgl_pilih_foto,
            'pes_tgl_review' => $this->pes_tgl_review,
            'pes_tgl_deadline' => $this->pes_tgl_deadline,
            'pes_status_pembayaran' => $this->pes_status_pembayaran,
        ]);

        return $dataProvider;
    }
}

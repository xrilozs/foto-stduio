<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderDetailTema;

/**
 * OrderDetailTemaSearch represents the model behind the search form of `common\models\OrderDetailTema`.
 */
class OrderDetailTemaSearch extends OrderDetailTema
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['odtema_id', 'odtema_detcus_id', 'odtema_tem_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderDetailTema::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'odtema_id' => $this->odtema_id,
            'odtema_detcus_id' => $this->odtema_detcus_id,
            'odtema_tem_id' => $this->odtema_tem_id,
        ]);

        return $dataProvider;
    }
}

<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderDetailPaketUpgrade;

/**
 * OrderDetailPaketUpgradeSearch represents the model behind the search form of `common\models\OrderDetailPaketUpgrade`.
 */
class OrderDetailPaketUpgradeSearch extends OrderDetailPaketUpgrade
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['odpu_id', 'odpu_detcus_id', 'odpu_upg_id', 'odpu_qty'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderDetailPaketUpgrade::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'odpu_id' => $this->odpu_id,
            'odpu_detcus_id' => $this->odpu_detcus_id,
            'odpu_upg_id' => $this->odpu_upg_id,
            'odpu_qty' => $this->odpu_qty,
        ]);

        return $dataProvider;
    }
}

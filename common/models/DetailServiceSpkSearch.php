<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DetailServiceSpk;

/**
 * DetailServiceSpkSearch represents the model behind the search form of `common\models\DetailServiceSpk`.
 */
class DetailServiceSpkSearch extends DetailServiceSpk
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['serv_id', 'serv_cust_id', 'serv_kary_id', 'serv_studio'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetailServiceSpk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'serv_id' => $this->serv_id,
            'serv_cust_id' => $this->serv_cust_id,
            'serv_kary_id' => $this->serv_kary_id,
            'serv_studio' => $this->serv_studio,
        ]);

        return $dataProvider;
    }
}

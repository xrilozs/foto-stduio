<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderDetailPaketFoto;

/**
 * OrderDetailPaketFotoSearch represents the model behind the search form of `common\models\OrderDetailPaketFoto`.
 */
class OrderDetailPaketFotoSearch extends OrderDetailPaketFoto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['odpf_id', 'odpf_detcus_id', 'odpf_paket_id', 'odpf_qty'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderDetailPaketFoto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'odpf_id' => $this->odpf_id,
            'odpf_detcus_id' => $this->odpf_detcus_id,
            'odpf_paket_id' => $this->odpf_paket_id,
            'odpf_qty' => $this->odpf_qty,
        ]);

        return $dataProvider;
    }
}

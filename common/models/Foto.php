<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "foto".
 *
 * @property int $foto_id
 * @property int $foto_detcus_id
 * @property string $foto_nama_file
 * @property string $foto_image
 * @property string $foto_ket
 */
class Foto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['foto_detcus_id'], 'integer'],
            [['foto_nama_file'], 'file', 'maxFiles' => 400],
            [['foto_image'], 'string', 'max' => 400],
            [['foto_ket'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'foto_id' => 'Foto ID',
            'foto_detcus_id' => 'No. Invoice',
            'foto_nama_file' => 'Foto Nama File',
            'foto_image' => 'Upload Image',
            'foto_ket' => 'Keterangan Foto',
        ];
    }

    public function getDetailCustomer()
    {
        return $this->hasMany(DetailCustomer::className(), ['detcus_id' => 'foto_detcus_id']);
    }
}

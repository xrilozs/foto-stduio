<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "album".
 *
 * @property int $album_id
 * @property string $album_nama
 * @property string $album_warna
 * @property string $album_gambar
 * @property string $album_ket
 * @property int $album_harga
 */
class Album extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'album';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['album_nama', 'album_warna', 'album_gambar', 'album_ket', 'album_harga'], 'required'],
            [['album_harga'], 'integer'],
            [['album_nama', 'album_warna'], 'string', 'max' => 30],
            [['album_gambar', 'album_ket'], 'string', 'max' => 50],
            [['album_gambar'], 'file', 'extensions'=>'jpg, png, gif, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'album_id' => 'Album ID',
            'album_nama' => 'Nama Album',
            'album_warna' => 'Warna Album',
            'album_gambar' => 'Gambar Album',
            'album_ket' => 'Keterangan',
            'album_harga' => 'Harga',
        ];
    }
}

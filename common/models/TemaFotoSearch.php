<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TemaFoto;

/**
 * TemaFotoSearch represents the model behind the search form of `common\models\TemaFoto`.
 */
class TemaFotoSearch extends TemaFoto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tem_id'], 'integer'],
            [['tem_nama', 'tem_gambar'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TemaFoto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tem_id' => $this->tem_id,
        ]);

        $query->andFilterWhere(['like', 'tem_nama', $this->tem_nama])
            ->andFilterWhere(['like', 'tem_gambar', $this->tem_gambar]);

        return $dataProvider;
    }
}

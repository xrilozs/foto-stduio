<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jenis_frame".
 *
 * @property int $frame_id
 * @property string $frame_nama_produk
 * @property string $frame_gambar
 * @property string $frame_ukuran
 * @property int $frame_harga
 * @property string $frame_ket
 */
class JenisFrame extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_frame';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['frame_nama_produk', 'frame_gambar', 'frame_ukuran', 'frame_warna', 'frame_harga', 'frame_ket'], 'required'],
            [['frame_harga'], 'integer'],
            [['frame_ukuran', 'frame_warna'], 'string', 'max' => 30],
            [['frame_nama_produk'], 'string', 'max' => 50],
            [['frame_gambar'], 'string', 'max' => 200],
            [['frame_ket'], 'string', 'max' => 50],
            [['frame_gambar'], 'file', 'extensions'=>'jpg, png, gif, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'frame_id' => 'Frame ID',
            'frame_nama_produk' => 'Nama Produk Frame',
            'frame_gambar' => 'Gambar Frame',
            'frame_warna' => 'Warna Frame',
            'frame_ukuran' => 'Ukuran Frame',
            'frame_harga' => 'Harga',
            'frame_ket' => 'Keterangan',
        ];
    }

    public function getPaketFrameDetail()
    {
        return $this->hasOne(PaketFrameDetail::className(), ['fd_frame_id' => 'frame_id']);
    }
}

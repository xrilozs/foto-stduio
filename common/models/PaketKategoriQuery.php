<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PaketKategori]].
 *
 * @see PaketKategori
 */
class PaketKategoriQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PaketKategori[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PaketKategori|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

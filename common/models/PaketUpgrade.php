<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "paket_upgrade".
 *
 * @property int $upg_id
 * @property string $upg_nama
 * @property string $upg_gambar
 * @property int $upg_harga
 * @property string $upg_keterangan
 */
class PaketUpgrade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paket_upgrade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['upg_nama', 'upg_gambar', 'upg_harga', 'upg_keterangan'], 'required'],
            [['upg_harga'], 'integer'],
            [['upg_nama', 'upg_keterangan'], 'string', 'max' => 100],
            [['upg_gambar'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'upg_id' => 'Upg ID',
            'upg_nama' => 'Nama Paket Upgrade',
            'upg_gambar' => 'Gambar',
            'upg_harga' => 'Harga Paket Upgrade',
            'upg_keterangan' => 'Keterangan',
        ];
    }
}

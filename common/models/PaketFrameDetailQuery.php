<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PaketFrameDetail]].
 *
 * @see PaketFrameDetail
 */
class PaketFrameDetailQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PaketFrameDetail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PaketFrameDetail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

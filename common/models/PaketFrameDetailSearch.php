<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaketFrameDetail;

/**
 * PaketFrameDetailSearch represents the model behind the search form of `common\models\PaketFrameDetail`.
 */
class PaketFrameDetailSearch extends PaketFrameDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fd_id', 'fd_paket_id', 'fd_frame_id', 'fd_qty'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaketFrameDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fd_id' => $this->fd_id,
            'fd_paket_id' => $this->fd_paket_id,
            'fd_frame_id' => $this->fd_frame_id,
            'fd_qty' => $this->fd_qty,
        ]);

        return $dataProvider;
    }
}

<?php

namespace common\models;

use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "detail_customer".
 *
 * @property int $detcus_id
 * @property int $detcus_cust_id
 * @property int $detcus_paket_id
 * @property int $detcus_frame_id
 * @property int $detcus_album_id
 * @property int $detcus_foto_id
 * @property int $detcus_harga
 * @property string $detcus_ket
 */
class DetailCustomer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detail_customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['detcus_tanggal', ], 'required'],
            [['detcus_cust_id', 'detcus_pay'], 'integer'],
            [['detcus_tanggal', 'detcus_cust_id', 'detcus_pay', 'detcus_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'detcus_id' => 'Detcus ID',
            'detcus_cust_id' => 'No. Invoice',
            'detcus_pay' => 'Sisa Pembayaran',
            'detcus_status' => 'Status Pembayaran',
            'detcus_tanggal' => 'Tanggal'
        ];
    }
    public function getOrderDetailPaketFoto()
    {
        return $this->hasOne(OrderDetailPaketFoto::className(), ['odpf_detcus_id' => 'detcus_id']);
    }

    public function getOrderDetailPaketUpgrade()
    {
        return $this->hasOne(OrderDetailPaketUpgrade::className(), ['odpu_detcus_id' => 'detcus_id']);
    }

    public function getCustomer()
    {
            return $this->hasOne(Customer::className(), ['cust_id' => 'detcus_cust_id']);
    }

    public function getOrderDetailTema()
    {
        return $this->hasOne(OrderDetailTema::className(), ['odtema_detcus_id' => 'detcus_id']);
    }

    public function getDetailServiceSpk()
    {
        return $this->hasOne(DetailServiceSpk::className(), ['serv_detcus_id' => 'detcus_id']);
    }

    public function getFoto()
    {
        return $this->hasOne(Foto::className(), ['foto_detcus_id' => 'detcus_id']);
    }


    public static function retrieveInvoiceLunas(){

        $data =
            self::find()->select([
            'detcus_id' => 'detcus_id',
            'cust_no_invoice' => 'cust_no_invoice'
        ])->joinWith('customer', false)
            ->where('detcus_status = :status', [':status' => 'Lunas'])
            ->asArray()
            ->all()
        ;

        // $customer = Customer::find()->orderBy('cust_no_invoice')->all();
        // $customerList = ArrayHelper::map($customer, 'cust_id', function($customer) {
        //     $detailCustomer = DetailCustomer::findOne(['detcus_cust_id' =>$customer->cust_id]);
        //     $detailCustomer = (['detcus_status' => 'Lunas']);
        //     return $model["cust_no_invoice"];
        // });
    }

}

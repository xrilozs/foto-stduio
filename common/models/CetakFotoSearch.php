<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CetakFoto;

/**
 * CetakFotoSearch represents the model behind the search form of `common\models\CetakFoto`.
 */
class CetakFotoSearch extends CetakFoto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cetak_id', 'cetak_harga'], 'integer'],
            [['cetak_nama', 'cetak_size'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CetakFoto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cetak_id' => $this->cetak_id,
            'cetak_harga' => $this->cetak_harga,
        ]);

        $query->andFilterWhere(['like', 'cetak_nama', $this->cetak_nama])
            ->andFilterWhere(['like', 'cetak_size', $this->cetak_size]);

        return $dataProvider;
    }
}

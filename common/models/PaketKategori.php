<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "paket_kategori".
 *
 * @property int $kategori_id
 * @property string $kategori_nama
 */
class PaketKategori extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paket_kategori';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kategori_nama'], 'required'],
            [['kategori_nama'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kategori_id' => 'Kategori ID',
            'kategori_nama' => 'Kategori Nama',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PaketKategoriQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaketKategoriQuery(get_called_class());
    }
}

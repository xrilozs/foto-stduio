<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "paket_frame_detail".
 *
 * @property int $fd_id
 * @property int $fd_paket_id
 * @property int $fd_frame_id
 * @property int $fd_qty
 */
class PaketFrameDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paket_frame_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fd_paket_id', 'fd_frame_id', 'fd_qty'], 'required'],
            [['fd_paket_id', 'fd_frame_id', 'fd_qty'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fd_id' => 'Fd ID',
            'fd_paket_id' => 'Nama Paket',
            'fd_frame_id' => 'Nama Frame',
            'fd_qty' => 'Qty',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PaketFrameDetailQuery the active query used by this AR class.
     */
    // public static function find()
    // {
    //     return new PaketFrameDetailQuery(get_called_class());
    // }

    public function getPaketFoto()
    {
        return $this->hasOne(PaketFoto::className(), ['paket_id' => 'fd_paket_id']);
    }

    public function getJenisFrame()
    {
        return $this->hasOne(JenisFrame::className(), ['frame_id' => 'fd_frame_id']);
    }

}

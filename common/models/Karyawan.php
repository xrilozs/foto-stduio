<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "karyawan".
 *
 * @property int $kary_id
 * @property string $kary_nama
 * @property string $kary_status
 */
class Karyawan extends \yii\db\ActiveRecord
{


    public static function map(){
        return  yii\helpers\ArrayHelper::map(\common\models\Karyawan::find()->all(), 'kary_id', 'kary_nama', 'kary_status');
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'karyawan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kary_nama', 'kary_status'], 'required'],
            [['kary_nama'], 'string', 'max' => 50],
            [['kary_status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kary_id' => 'Kary ID',
            'kary_nama' => 'Nama Karyawan',
            'kary_status' => 'Posisi',
        ];
    }
}

<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[UkuranCetak]].
 *
 * @see UkuranCetak
 */
class UkuranCetakQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UkuranCetak[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UkuranCetak|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaketUpgrade;

/**
 * PaketUpgradeSearch represents the model behind the search form of `common\models\PaketUpgrade`.
 */
class PaketUpgradeSearch extends PaketUpgrade
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['upg_id', 'upg_harga'], 'integer'],
            [['upg_nama', 'upg_gambar', 'upg_keterangan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaketUpgrade::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'upg_id' => $this->upg_id,
            'upg_harga' => $this->upg_harga,
        ]);

        $query->andFilterWhere(['like', 'upg_nama', $this->upg_nama])
            ->andFilterWhere(['like', 'upg_gambar', $this->upg_gambar])
            ->andFilterWhere(['like', 'upg_keterangan', $this->upg_keterangan]);

        return $dataProvider;
    }
}

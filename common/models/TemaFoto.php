<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tema_foto".
 *
 * @property int $tem_id
 * @property string $tem_nama
 * @property string $tem_gambar
 */
class TemaFoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tema_foto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tem_nama', 'tem_gambar'], 'required'],
            [['tem_kategori_id'], 'integer'],
            [['tem_nama',], 'string', 'max' => 50],
            [['tem_gambar'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tem_id' => 'Tem ID',
            'tem_kategori_id' => 'Paket Foto',
            'tem_nama' => 'Nama Tema',
            'tem_gambar' => 'Gambar',
        ];
    }

    public function getPaketFoto()
    {
        return $this->hasOne(PaketKategori::className(), ['kategori_id' => 'tem_kategori_id']);
    }


    // public static function mapIdToNama(){
    //     return \yii\helpers\ArrayHelper::map(static::find()->all(), 'tem_id', 'tem_nama');
    // }
}

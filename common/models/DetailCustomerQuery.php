<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[DetailCustomer]].
 *
 * @see DetailCustomer
 */
class DetailCustomerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return DetailCustomer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return DetailCustomer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


    // public function retrieveInvoiceLunas(){

    //     $data =  parent::select([
    //         'detcus_id' => 'detcus_id',
    //         'customer' => 'cust_no_invoice'
    //     ])->joinWith('customer', false)
    //         ->asArray()
    //         ->all()
    //     ;

    //     return ArrayHelper::map($data, 'id', 'customer') 

    // }


}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hasil_foto".
 *
 * @property int $hf_id
 * @property int $hf_detcus_id
 */
class HasilFoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hasil_foto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hf_detcus_id'], 'required'],
            [['hf_detcus_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hf_id' => 'Hf ID',
            'hf_detcus_id' => 'Hf Detcus ID'
        ];
    }
}

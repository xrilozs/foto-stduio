<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PaketFoto;

/**
 * PaketFotoSearch represents the model behind the search form of `common\models\PaketFoto`.
 */
class PaketFotoSearch extends PaketFoto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paket_id'], 'integer'],
            [['paket_nama', 'paket_gambar', 'paket_harga', 'paket_ket'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaketFoto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'paket_id' => $this->paket_id,
        ]);

        $query->andFilterWhere(['like', 'paket_nama', $this->paket_nama]);
            // ->andFilterWhere(['like', 'paket_gambar', $this->paket_gambar])
            // ->andFilterWhere(['like', 'paket_harga', $this->paket_harga])
            // ->andFilterWhere(['like', 'paket_ket', $this->paket_ket]);

        return $dataProvider;
    }
}

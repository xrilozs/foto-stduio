<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "detail_service_spk".
 *
 * @property int $serv_id
 * @property int $serv_detcus_id
 * @property int $serv_kary_id
 * @property int $serv_studio
 * @property int|null $serv_fotografer_id
 * @property int|null $serv_asisten_id
 * @property int|null $serv_cs_id
 *
 * @property Karyawan $servAsisten
 * @property Karyawan $servCs
 * @property DetailCustomer $servDetcus
 * @property Karyawan $servFotografer
 * @property Karyawan $servKary
 */
class DetailServiceSpk extends \yii\db\ActiveRecord
{

    /*
        @var formChekcList array
    */
    public $formCheckListIDs = [];
    // public function checkboxList($items, $formCheckListIDs = [])
    // {
    //     if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_INPUT) {
    //         $this->addErrorClassIfNeeded($formCheckListIDs);
    //     }
    //     $this->addAriaAttributes($formCheckListIDs);
    //     $this->adjustLabelFor($formCheckListIDs);
    //     $this->_skipLabelFor = true;
    //     $this->parts['{input}'] = Html::activeCheckboxList($this->model, $this->attribute, $items, $formCheckListIDs);
    //     return $this;
    // }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detail_service_spk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
             [['serv_detcus_id', 'serv_studio'], 'required'],
            [['serv_detcus_id', 'serv_studio', 'serv_fotografer_id', 'serv_asisten_id', 'serv_cs_id'], 'integer'],
            [['serv_detcus_id'], 'exist', 'skipOnError' => true, 'targetClass' => DetailCustomer::className(), 'targetAttribute' => ['serv_detcus_id' => 'detcus_id']],
            [['serv_asisten_id'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['serv_asisten_id' => 'kary_id']],
            [['serv_cs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['serv_cs_id' => 'kary_id']],
            [['serv_fotografer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['serv_fotografer_id' => 'kary_id']],
             ['formCheckListIDs', 'each', 'rule' => ['integer']],
        ];
    }

 

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'serv_id' => 'Serv ID',
            'serv_detcus_id' => 'Serv Detcus ID',
            'serv_studio' => 'Serv Studio',
            'serv_fotografer_id' => 'Serv Fotografer ID',
            'serv_asisten_id' => 'Serv Asisten ID',
            'serv_cs_id' => 'Serv Cs ID',
            'formCheckListIDs' => 'Tema Foto'
        ];
    }

    /**
     * Gets query for [[ServAsisten]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getServAsisten()
    {
        return $this->hasOne(Karyawan::className(), ['kary_id' => 'serv_asisten_id']);
    }

    /**
     * Gets query for [[ServCs]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getServCs()
    {
        return $this->hasOne(Karyawan::className(), ['kary_id' => 'serv_cs_id']);
    }

    /**
     * Gets query for [[ServDetcus]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getServDetcus()
    {
        return $this->hasOne(DetailCustomer::className(), ['detcus_id' => 'serv_detcus_id']);
    }

    /**
     * Gets query for [[ServFotografer]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getServFotografer()
    {
        return $this->hasOne(Karyawan::className(), ['kary_id' => 'serv_fotografer_id']);
    }

    /**
     * Gets query for [[ServKary]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    // public function getServKary()
    // {
    //     return $this->hasOne(Karyawan::className(), ['kary_id' => 'serv_kary_id']);
    // }

    // /**
    //  * {@inheritdoc}
    //  * @return DetailServiceSpkQuery the active query used by this AR class.
    //  */
    // public static function find()
    // {
    //     return new DetailServiceSpkQuery(get_called_class());
    // }
}

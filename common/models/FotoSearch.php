<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Foto;

/**
 * FotoSearch represents the model behind the search form of `common\models\Foto`.
 */
class FotoSearch extends Foto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['foto_id', 'foto_detcus_id'], 'integer'],
            [['foto_nama_file', 'foto_image', 'foto_ket'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Foto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'foto_id' => $this->foto_id,
            'foto_detcus_id' => $this->foto_detcus_id,
        ]);

        $query->andFilterWhere(['like', 'foto_nama_file', $this->foto_nama_file])
            ->andFilterWhere(['like', 'foto_image', $this->foto_image])
            ->andFilterWhere(['like', 'foto_ket', $this->foto_ket]);

        return $dataProvider;
    }
}

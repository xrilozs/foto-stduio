<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cetak_foto".
 *
 * @property int $cetak_id
 * @property string $cetak_nama
 * @property string $cetak_size
 * @property int $cetak_harga
 */
class CetakFoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cetak_foto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cetak_nama', 'cetak_size', 'cetak_harga'], 'required'],
            [['cetak_harga'], 'integer'],
            [['cetak_nama', 'cetak_size'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cetak_id' => 'Cetak ID',
            'cetak_nama' => 'Cetak Nama',
            'cetak_size' => 'Cetak Size',
            'cetak_harga' => 'Cetak Harga',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CetakFotoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CetakFotoQuery(get_called_class());
    }
}

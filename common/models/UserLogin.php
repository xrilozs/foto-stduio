<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_login".
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $username
 * @property string $password
 * @property string $authKey
 * @property string $accessToken
 * @property int $status
 * @property string $role
 * @property int $id_karyawan
 * @property string $time_create
 * @property string $time_update
 *
 * @property Karyawan $karyawan
 */
class UserLogin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'username', 'password', 'authKey', 'accessToken', 'status', 'role', 'id_karyawan', 'time_create', 'time_update'], 'required'],
            [['status', 'id_karyawan'], 'integer'],
            [['time_create', 'time_update'], 'safe'],
            [['firstName'], 'string', 'max' => 15],
            [['lastName'], 'string', 'max' => 20],
            [['username', 'password'], 'string', 'max' => 30],
            [['authKey'], 'string', 'max' => 50],
            [['accessToken'], 'string', 'max' => 100],
            [['role'], 'string', 'max' => 60],
            [['id_karyawan'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['id_karyawan' => 'kary_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'username' => 'Username',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'status' => 'Status',
            'role' => 'Role',
            'id_karyawan' => 'Id Karyawan',
            'time_create' => 'Time Create',
            'time_update' => 'Time Update',
        ];
    }

    /**
     * Gets query for [[Karyawan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKaryawan()
    {
        return $this->hasOne(Karyawan::className(), ['kary_id' => 'id_karyawan']);
    }
}

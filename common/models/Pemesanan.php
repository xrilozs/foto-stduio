<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pemesanan".
 *
 * @property int $pes_id
 * @property int $pes_cust_id
 * @property string $pes_tgl_foto
 * @property string $pes_jam_foto
 * @property string $pes_tgl_pilih_foto
 * @property string $pes_tgl_review
 * @property string $pes_tgl_deadline
 * @property int $pes_status_pembayaran 0:Lunas
 1:DP
 */
class Pemesanan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pemesanan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pes_cust_id', 'pes_tgl_foto', 'pes_jam_foto', 'pes_tgl_pilih_foto', 'pes_tgl_review', 'pes_tgl_deadline', 'pes_status_pembayaran'], 'required'],
            [['pes_cust_id', 'pes_status_pembayaran'], 'integer'],
            [['pes_tgl_foto', 'pes_jam_foto', 'pes_tgl_pilih_foto', 'pes_tgl_review', 'pes_tgl_deadline'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pes_id' => 'Pes ID',
            'pes_cust_id' => 'No. Invoice',
            'pes_tgl_foto' => 'Tanggal Foto',
            'pes_jam_foto' => 'Jam Foto',
            'pes_tgl_pilih_foto' => 'Tanggal Pilih Foto',
            'pes_tgl_review' => 'Tanggal Review',
            'pes_tgl_deadline' => 'Tanggal Deadline',
            'pes_status_pembayaran' => 'Status Pembayaran',
        ];
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['cust_id' => 'pes_cust_id']);
    }

}

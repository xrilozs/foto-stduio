<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_detail_tema".
 *
 * @property int $odtema_id
 * @property int $odtema_detcus_id
 * @property int $odtema_tem_id
 */
class OrderDetailTema extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_detail_tema';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['odtema_detcus_id', 'odtema_tem_id'], 'required'],
            [['odtema_detcus_id', 'odtema_tem_id'], 'integer'],
            [['odtema_ket'], 'safe']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'odtema_id' => 'Odtema ID',
            'odtema_detcus_id' => 'No. Invoice',
            'odtema_tem_id' => 'Nama Tema',
            'odtema_ket' => 'Keterangan untuk Tim Foto',
        ];
    }

    public function getDetailCustomer()
    {
        return $this->hasOne(DetailCustomer::className(), ['detcus_id' => 'odtema_detcus_id']);
    }
    
    public function getTemaFoto()
    {
        return $this->hasOne(TemaFoto::className(), ['tem_id' => 'odtema_tem_id']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "paket_foto".
 *
 * @property int $paket_id
 * @property int $paket_kategori_id
 * @property string $paket_nama
 * @property string|null $paket_gambar
 * @property int $paket_jumlah_tema
 * @property int $paket_pose
 * @property int|null $paket_album_id
 * @property int|null $paket_cetak_id
 * @property int|null $paket_album_qty
 * @property int|null $paket_cetak_qty
 * @property int $paket_jumlah_foto_utama
 * @property int $paket_jumlah_cetak
 * @property string $paket_harga
 * @property string $paket_ket
 *
 * @property PaketFrameDetail[] $paketFrameDetails
 */
class PaketFoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paket_foto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paket_kategori_id', 'paket_nama', 'paket_jumlah_tema', 'paket_pose', 'paket_jumlah_foto_utama', 'paket_jumlah_cetak', 'paket_harga', 'paket_ket'], 'required'],
            [['paket_kategori_id', 'paket_jumlah_tema', 'paket_pose', 'paket_album_id', 'paket_cetak_id', 'paket_album_qty', 'paket_cetak_qty', 'paket_jumlah_foto_utama', 'paket_jumlah_cetak'], 'integer'],
            [['paket_ket'], 'string'],
            [['paket_nama'], 'string', 'max' => 30],
            [['paket_gambar'], 'string', 'max' => 200],
            [['paket_harga'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'paket_id' => 'Paket ID',
            'paket_kategori_id' => 'Paket Kategori ID',
            'paket_nama' => 'Paket Nama',
            'paket_gambar' => 'Paket Gambar',
            'paket_jumlah_tema' => 'Paket Jumlah Tema',
            'paket_pose' => 'Paket Pose',
            'paket_album_id' => 'Paket Album ID',
            'paket_cetak_id' => 'Paket Cetak ID',
            'paket_album_qty' => 'Paket Album Qty',
            'paket_cetak_qty' => 'Paket Cetak Qty',
            'paket_jumlah_foto_utama' => 'Paket Jumlah Foto Utama',
            'paket_jumlah_cetak' => 'Paket Jumlah Cetak',
            'paket_harga' => 'Paket Harga',
            'paket_ket' => 'Paket Ket',
        ];
    }

    /**
     * Gets query for [[PaketFrameDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemaFoto()
    {
        return $this->hasOne(TemaFoto::className(), ['tem_paket_id' => 'paket_id']);
    }
    public function getOrderDetailPaketFoto()
    {
        return $this->hasOne(OrderDetailPaketFoto::className(), ['odpf_paket_id' => 'paket_id']);
    }
    public function getAlbum()
    {
        return $this->hasOne(Album::className(), ['album_id' => 'paket_album_id']);
    }
    public function getPaketKategori()
    {
        return $this->hasOne(PaketKategori::className(), ['kategori_id' => 'paket_kategori_id']);
    }
    public function getCetakFoto()
    {
        return $this->hasOne(CetakFoto::className(), ['cetak_id' => 'paket_cetak_id']);
    }
    public function getPaketFrameDetail()
    {
        return $this->hasOne(PaketFrameDetail::className(), ['fd_paket_id' => 'paket_id']);
    }
}

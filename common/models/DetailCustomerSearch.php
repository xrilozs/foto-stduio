<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DetailCustomer;

/**
 * DetailCustomerSearch represents the model behind the search form of `common\models\DetailCustomer`.
 */
class DetailCustomerSearch extends DetailCustomer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['detcus_id', 'detcus_cust_id'], 'integer'],
            // [['detcus_tanggal', 'detcus_cust_id', 'detcus_pay', 'detcus_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetailCustomer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'detcus_id' => $this->detcus_id,
            'detcus_cust_id' => $this->detcus_cust_id,
            
        ]);

        //$query->andFilterWhere(['like', 'detcus_ket', $this->detcus_ket]);

        return $dataProvider;
    }

}
